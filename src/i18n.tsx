

import common_en from "./translations/en/common.json"
import common_es from "./translations/es/common.json"

import login_en from "./translations/en/login.json"
import login_es from "./translations/es/login.json"

import dashboard_en from './translations/en/dashboard.json'
import dashboard_es from './translations/es/dashboard.json'

import seguimientooc_en from './translations/en/seguimientooc.json'
import seguimientooc_es from './translations/es/seguimientooc.json'

import inventarios_en from './translations/en/inventarios.json'
import inventarios_es from './translations/es/inventarios.json'

import embarques_en from './translations/en/embarques.json'
import embarques_es from './translations/es/embarques.json'

import estadocuenta_en from './translations/en/estadocuenta.json'
import estadocuenta_es from './translations/es/estadocuenta.json'

import facturas_en from './translations/en/facturas_cert.json'
import facturas_es from './translations/es/facturas_cert.json'

import conozcanos_en from './translations/en/conozcanos.json'
import conozcanos_es from './translations/es/conozcanos.json'

import avisoprivacidad_en from './translations/en/avisoprivacidad.json'
import avisoprivacidad_es from './translations/es/avisoprivacidad.json'

import terminos_en from './translations/en/terminos.json'
import terminos_es from './translations/es/terminos.json'

import contactos_en from './translations/en/contactos.json'
import contactos_es from './translations/es/contactos.json'

export default {
  es:{
    common: common_es,
    login: login_es,
    dashboard: dashboard_es,
    seguimientooc: seguimientooc_es,
    inventarios: inventarios_es,
    embarques: embarques_es,
    estadocuenta: estadocuenta_es,
    facturas_cert: facturas_es,
    conozcanos: conozcanos_es,
    avisoprivacidad: avisoprivacidad_es,
    terminos: terminos_es,
    contactos: contactos_es
  },
  en:{
    common: common_en,
    login: login_en,
    dashboard: dashboard_en,
    seguimientooc: seguimientooc_en,
    inventarios: inventarios_en,
    embarques: embarques_en,
    estadocuenta: estadocuenta_en,
    facturas_cert: facturas_en,
    conozcanos: conozcanos_en,
    avisoprivacidad: avisoprivacidad_en,
    terminos: terminos_en,
    contactos: contactos_en
  }
}