﻿import * as React from "react";
import * as _ from "lodash";
import Tabla from "../common/Tabla";
import numeral from "numeral";
import { bindActionCreators } from "redux";
import { Button, Card, CardContent } from "@material-ui/core";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as EstadoCuentaDetalleStore from "../store/EstadoCuentaDetalleStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Loading from "../common/Loading";
import moment from "moment";
import { translate } from "react-i18next";

type EstadoCuentaDetalleProps =
  EstadoCuentaDetalleStore.EstadoCuentaDetalleStoreState &
    typeof EstadoCuentaDetalleStore.actionCreators &
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;

interface EstadoCuentaDetalleState {
  periodo: string;
  cliente: string;
  vendedor: any;
  catalogoClientes: any;
  imgExcel: string;
}
class EstadoCuentaDetalle extends React.Component<
  EstadoCuentaDetalleProps,
  EstadoCuentaDetalleState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      periodo: "",
      cliente: "",
      vendedor: "",
      catalogoClientes: [],
      imgExcel: "i-excel-2.png",
    };
  }
  componentDidMount() {
    //var title: any = document.getElementById('title-page');
    //title.innerHTML = "Historial de estado de cuenta por período";
    this.props.setVista("SeguimientoOCDetalle", false);
    const state = (this.props.location as any).state;
    this.setState({
      periodo: state.periodo,
      cliente: state.cliente,
      vendedor: state.vendedor,
      catalogoClientes: state.catalogoClientes,
    });
    this.props.requestInformacionInicialEstadoCuentaDeta(
      state.cliente,
      state.vendedor,
      state.periodo
    );
  }

  public render() {
    const { t } = this.props;
    const columns = [
      {
        Header: t('tbl_num_doc'),
        accessor: "DocNum",
        Cell: (row: any) => {
          return (
            <span
              style={{
                cursor: "pointer",
                textDecoration: row.original.Tipo == "F" ? "underline" : "none",
              }}
              onClick={() => {
                if (row.original.Tipo == "F") {
                  this.props.requestDowloadArchivoFacturaEdoCuenta(
                    row.original.DocNum,
                    "PDF"
                  );
                }
              }}
            >
              {row.value}
            </span>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: "Zip",
        accessor: "DocNum",
        Cell: (row: any) => {
          return (
            <span
              style={{
                cursor: "pointer",
                textDecoration: row.original.Tipo == "F" ? "underline" : "none",
              }}
              onClick={() => {
                if (row.original.Tipo == "F") {
                  this.props.requestDowloadArchivoFacturaEdoCuenta(
                    row.original.DocNum,
                    "zip"
                  );
                }
              }}
            >
              {row.value}
            </span>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('OC'),
        accessor: "NumAtCard",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_clase_doc'),
        accessor: "Tipo",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_fecha'),
        accessor: "DocDate",
        Cell: (row: any) => {
          return moment(row.value).format("YY-MM-DD");
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_fecha_vencimiento'),
        accessor: "DocDueDate",
        Cell: (row: any) => {
          return moment(row.value).format("YY-MM-DD");
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 150,
      },

      {
        Header: t('tbl_dias_atraso'),
        accessor: "Vencido",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_periodo'),
        accessor: "Debit0USD",
        Cell: (row: any) => {
          var periodo = "";
          var color = "#FE2E2E";
          var colorTexto = "#FFFFFF";
          if (row.original.Debit0USD != 0) {
            periodo = "Por vencer";
            color = "#00B050";
          } else if (row.original.Debit15USD != 0) {
            periodo = "De 1 a 15";
            color = "#FFFF00";
            colorTexto = "#555454";
          } else if (row.original.Debit30USD != 0) {
            periodo = "De 15 a 30";
            color = "#FFC000";
          } else if (row.original.Debit60USD != 0) {
            periodo = "De 30 a 60";
            color = "#FF6600";
          } else if (row.original.Debit90USD != 0) {
            periodo = "De 60 a 90";
            color = "#A50021";
          } else {
            periodo = "Mas de 91";
            color = "#FF0000";
          }
          return (
            <span
              style={{ backgroundColor: color, color: colorTexto }}
              className="label label-table label-secondary"
            >
              {t(periodo)}
            </span>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_moneda'),
        accessor: "ISOCurrCod",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_monto'),
        Cell: (row: any) => {
          var colorTexto = "";
          var valor =
            row.original.ISOCurrCod == "MXN"
              ? row.original.DocTotal
              : row.original.DocTotalFC;
              if (row.original.Tipo == "N"){
                valor = -valor;
                colorTexto = "#FF0000";                
              }
          return (
            <div style={{ textAlign: "right",color: colorTexto }}>
              {numeral(valor).format("0,0.00")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 150,
      },
      {
        Header: t('tbl_abono'),
        Cell: (row: any) => {
          var colorTexto = "";
          var valor =
            row.original.ISOCurrCod == "MXN"
              ? row.original.PaidToDate
              : row.original.PaidFC;
              if (row.original.Tipo == "N"){
                valor = -valor;
                colorTexto = "#FF0000";                
              }              
          return (
            <div style={{ textAlign: "right",color: colorTexto }}>
              {numeral(valor).format("0,0.00")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_saldo'),
        Cell: (row: any) => {
          var total =
            row.original.ISOCurrCod == "MXN"
              ? row.original.DocTotal
              : row.original.DocTotalFC;
          var abono =
            row.original.ISOCurrCod == "MXN"
              ? row.original.PaidToDate
              : row.original.PaidFC;
          var result = total - abono;
          let colorTexto = '';
          if (row.original.Tipo == "N"){
            result = -result;          
            colorTexto = "#FF0000";
          }
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" ,color: colorTexto}}>
              {numeral(result).format("0,0.00")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
    ];
    if (!this.state.cliente) {
      columns.unshift({
        Header: t('tbl_nom_cliente'),
        accessor: "_Alias",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 200,
      });
      columns.unshift({
        Header: t('tbl_cliente'),
        accessor: "CardCode",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      });
    }
    var cliente = _.find(this.state.catalogoClientes, (x: any) => {
      return x.clave == this.state.cliente;
    });
    return this.props.isLoadingEdoCtaDetalle ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="detallepage">
        {cliente ? (
          <div style={{ fontSize: 13 }}>
            <div className="row">
              {/*<div className="col-xs-6 col-md-6">*/}
              {/*    <table className="table table-bordered">*/}
              {/*        <tbody>*/}
              {/*            <tr style={{ textAlign: "center" }}>*/}
              {/*                        <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div  > Linea de crédito </div> </td><td style={{ backgroundColor: "#555454", color: "#fff" }}>*/}
              {/*                            {numeral(this.props.registros.LineaCredito).format("0,0.00") }</td>*/}
              {/*            </tr>*/}
              {/*        </tbody>*/}
              {/*       </table>                              */}
              {/*</div>*/}
              <div className="col-xs-6 col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#0e7ec3",
                          color: "#fff",
                          width: "65%",
                        }}
                      >
                        <div> {t('lbl_saldo')} </div>{" "}
                      </td>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#555454",
                          color: "#fff",
                          textAlign: "right",
                        }}
                      >
                        $
                        {numeral(
                          this.props.registrosEdoCtaDetalle.Saldo
                        ).format("0,0.00")}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-xs-6 col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#0e7ec3",
                          color: "#fff",
                          width: "65%",
                        }}
                      >
                        <div> {t('lbl_dias_promedio')} </div>{" "}
                      </td>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#555454",
                          color: "#fff",
                          textAlign: "right",
                        }}
                      >
                        {numeral(
                          this.props.registrosEdoCtaDetalle.DiasPromedioPago
                        ).format("0,0")}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              {/*<div className="col-xs-6 col-md-6">*/}
              {/*    <table className="table table-bordered">*/}
              {/*        <tbody>*/}
              {/*            <tr style={{ textAlign: "center" }}>*/}
              {/*                        <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div > Disponible </div> </td><td style={{ backgroundColor: "#555454", color: "#fff" }}> {*/}
              {/*                            numeral(this.props.registros.Disponible).format("0,0.00")}</td>*/}
              {/*            </tr>*/}
              {/*        </tbody>*/}
              {/*    </table>*/}
              {/*</div>*/}
              <div className="col-xs-6 col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#0e7ec3",
                          color: "#fff",
                          width: "65%",
                        }}
                      >
                        <div> {t('lbl_saldo_vencido')} </div>{" "}
                      </td>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#555454",
                          color: "#fff",
                          textAlign: "right",
                        }}
                      >
                        $
                        {numeral(
                          this.props.registrosEdoCtaDetalle.SaldoVencido
                        ).format("0,0.00")}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-xs-6 col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#0e7ec3",
                          color: "#fff",
                          width: "65%",
                        }}
                      >
                        <div> {t('lbl_dias_plazo')} </div>{" "}
                      </td>
                      <td
                        style={{
                          marginRight: 5,
                          borderRadius: 5,
                          backgroundColor: "#555454",
                          color: "#fff",
                          textAlign: "right",
                        }}
                      >
                        {numeral(
                          this.props.registrosEdoCtaDetalle.DiasPlazo
                        ).format("0,0")}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>{" "}
          </div>
        ) : null}
        <div className="row">
          <div className="col col-lg-10 col-md-10 col-sm-12 col-12">
            <div className="row" style={{ paddingTop: "10px" }}>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_estatus')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {this.state.periodo ? this.state.periodo : t('lbl_todos')}
                  </span>
                </div>
              </div>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_cliente')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {cliente
                      ? cliente.clave + " - " + cliente.descripcion
                      : t('lbl_todos')}
                  </span>{" "}
                </div>
              </div>
            </div>
          </div>
          <div className="col col-md-2 col-lg-2 col-sm-12 col-12">
            <div style={{ display: "flex", float: "right" }}>
              <div>
                <div
                onMouseOver={() => {
                    var imagen = this.state.imgExcel;
                    this.setState({
                      imgExcel: String(imagen).replace("2", "1"),
                    });
                  }}
                  onMouseOut={() => {
                    var imagen = this.state.imgExcel;
                    this.setState({
                      imgExcel: String(imagen).replace("1", "2"),
                    });
                  }}
                  onClick={() => {
                    this.props.requestDowloadExcelEdoCuentaDetalle(
                      this.state.cliente,
                      this.state.vendedor,
                      this.state.periodo
                    );
                  }}
                  className="circulo-iconos boton-inactivo"
                >
                  <img
                    src={`../../images/icons/${this.state.imgExcel}`}
                    style={{ width: "33px", margin: "auto" }}
                  />
                </div>
              </div>
              <div>
                <div
                  onClick={() => {
                    window.history.go(-1);
                  }}
                  className="circulo-iconos boton-inactivo"
                >
                  <img
                    src={`../../images/icons/i-return.png`}
                    style={{ width: "30px", margin: "auto" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-12">
            {
              <Tabla
                registros={
                  this.props.registrosEdoCtaDetalle.EdoCtaLista
                    ? this.props.registrosEdoCtaDetalle.EdoCtaLista
                    : []
                }
                columnas={columns}
                filterable={true}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.EstadoCuentaDetalleStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...EstadoCuentaDetalleStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('estadocuenta')(EstadoCuentaDetalle as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
