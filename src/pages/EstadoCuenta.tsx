﻿import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as EstadoCuentaStore from "../store/EstadoCuentaStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import { AnyAction, bindActionCreators } from "redux";
import GraficaGral from "../common/GraficaGeneral";
import DetallesGraficaEstados from "../common/DetallesGraficaEstados";
/**
 * Components
 * */
import { CardBody, CardHeader, Row, Col, CardTitle } from "reactstrap";
import Loading from "../common/Loading";
import { translate } from "react-i18next";

type EstadoCuentaProps = EstadoCuentaStore.EstadoCuentaStoreState &
  typeof EstadoCuentaStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;
interface EstadoCuentaState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  ValorTxt: any;
  imgExcel: string;
  imgUSD: string;
  imgMXN: string;
  btnUSD: string;
  btnMXN: string;
}

class EstadoCuenta extends React.Component<
  EstadoCuentaProps,
  EstadoCuentaState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      cliente: "",
      vendedor: "",
      nombreCliente: "",
      open: true,
      ValorTxt: "USD",
      imgExcel: "i-excel-2.png",
      imgUSD: "dollar-2.png",
      imgMXN: "pesos-1.png",
      btnUSD: "boton-activo",
      btnMXN: "boton-inactivo",
    };
  }
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("EstadoCuenta", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.requestInformacionInicialEdoCuenta(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
  }

  public render() {
    const { t } = this.props;
    var registrosDetalles: any = [];
    /*var primerRegistro = _.find(this.props.registrosEdoCuenta, (item: any) => {
      return item;
    });*/
    //var valor = this.props.primerRegistro ? this.props.primerRegistro.Valor2 : 0;

    var colores: any = [];
    var valorTxt = this.props.valorTxt;
    var registrosFilter = this.props.registrosEdoCuenta
      ? _.filter(this.props.registrosEdoCuenta, (item: any) => {
          return (
            item.ValorTxt.substr(item.ValorTxt.length - 3) ===
            (this.props.valor == 0 ? this.state.ValorTxt : valorTxt)
          );
        })
      : [];
    var total = 0;
    var dataset: any = [];
    var data = registrosFilter.map((item: any) => {
      total = total + item.Valor;
      var color = "";
      if (item.Grupo == "Por Vencer") color = "#00B95D";
      else if (item.Grupo == "De 1 a 15") color = "#D4B01A";
      else if (item.Grupo == "De 15 a 30") color = "#E93314";
      else if (item.Grupo == "De 30 a 60") color = "#E93314";
      else if (item.Grupo == "De 60 a 90") color = "#E93314";
      else color = "#E93314";
      dataset.push([item.Grupo, item.Valor, color]);
      colores.push(color);
      registrosDetalles.push({
        Grupo: t(item.Grupo),
        Valor: numeral(item.Valor).format("0,00"),
        ValorTxt : item.ValorTxt
      });
      return item.Valor;
    });
    const labels = registrosFilter.map((item: any) => {
      return t(item.Grupo);
    });
    const props = this.props;
    var state = this.state;
    var height = window.screen.height - 400;
    return this.props.isloadingEdoCuenta ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="containerpage">
        <div
          style={{ display: "flex", alignItems: "center", margin: 0 }}
          className="row"
        >
         <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-7 col-md-7 col-sm-12 col-12">
            <div className="row">
              {this.props.registrosEdoCuenta.length > 0 ? (
                <GraficaGral
                  label={t('title_estado_cuenta')}
                  labels={labels}
                  data={data}
                  backgroundColor={colores}
                  onclick={(items: any) => {
                    if (items[0]) {
                      var index = items[0]._index;
                      const merca = registrosFilter[index];
                      props.history.push({
                        pathname: `/EstadoCuentaDetalle`,
                        state: {
                          periodo: merca ? merca.ValorTxt : "",
                          vendedor: props.selectedVendedor,
                          cliente: props.selectedCliente,
                          catalogoClientes: props.clientes,
                        },
                      });
                    }
                  }}
                />
              ) : null}
            </div>
          </div>
          <div className="col col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
          <div className="col col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div className="row">
              <div className="col"></div>
              <div className="col">
                <div style={{ display: "flex" }}>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("1", "2"),
                      });
                    }}
                    onClick={() => {
                      this.props.requestDowloadExcelEdoCuenta(
                        this.props.selectedCliente,
                        this.props.selectedVendedor
                      );
                    }}
                    className="circulo-iconos boton-inactivo"
                  >
                    <img
                      src={`../../images/icons/${this.state.imgExcel}`}
                      style={{ width: "33px", margin: "auto" }}
                    />
                  </div>
                  {this.props.valor == 1 || this.props.valor == 0 ? (
                    <div
                      onMouseOver={() => {
                        var imagen = this.state.imgMXN;
                        this.setState({
                          imgMXN: String(imagen).replace("1", "2"),
                        });
                      }}
                      onMouseOut={() => {
                        this.setState({
                          imgMXN:
                            this.state.btnMXN == "boton-activo"
                              ? "pesos-2.png"
                              : "pesos-1.png",
                        });
                      }}
                      onClick={() => {
                        this.setState({
                          ValorTxt: "MXN",
                          btnMXN: "boton-activo",
                          btnUSD: "boton-inactivo",
                          imgMXN: "pesos-2.png",
                          imgUSD: "dollar-1.png",
                        });
                        this.props.changeValor("MXN");
                      }}
                      className={`circulo-iconos ${ valorTxt == "MXN"  ? "boton-activo" : this.state.btnMXN}`}
                    >
                      <img
                        src={`../../images/icons/${valorTxt == "MXN"  ? "pesos-2.png" : this.state.imgMXN}`}
                        style={{ width: "33px", margin: "auto" }}
                      />
                    </div>
                  ) : null}
                  {this.props.valor == 2 || this.props.valor == 0 ? (
                    <div
                      onMouseOver={() => {
                        var imagen = this.state.imgUSD;
                        this.setState({
                          imgUSD: String(imagen).replace("1", "2"),
                        });
                      }}
                      onMouseOut={() => {
                        this.setState({
                          imgUSD:
                            this.state.btnUSD == "boton-activo"
                              ? "dollar-2.png"
                              : "dollar-1.png",
                        });
                      }}
                      onClick={() => {
                        this.setState({
                          ValorTxt: "USD",
                          btnMXN: "boton-inactivo",
                          btnUSD: "boton-activo",
                          imgMXN: "pesos-1.png",
                          imgUSD: "dollar-2.png",
                        });
                       this.props.changeValor("USD");
                      }}
                      className={`circulo-iconos ${valorTxt == "USD"  ? "boton-activo" : this.state.btnUSD}`}
                    >
                      <img
                        src={`../../images/icons/${valorTxt == "USD"  ? "dollar-2.png" : this.state.imgUSD}`}
                        style={{ width: "30px", margin: "auto" }}
                      />
                    </div>
                  ) : null}
                </div>
              </div>

              <div className="col"></div>
            </div>
            <div className="row">
              <DetallesGraficaEstados
                labelTotal={"Total " + valorTxt }
                total = {numeral(total).format("0,0")}
                onClickTotal={() => {
                  this.props.history.push({
                    pathname: `/EstadoCuentaDetalle`,
                    state: {
                      periodo: "",
                      vendedor: this.props.selectedVendedor,
                      cliente: this.props.selectedCliente,
                      catalogoClientes: this.props.clientes,
                    },
                  });
                }}
                onClickDetalle={(item: any) => {
                  props.history.push({
                    pathname: `/EstadoCuentaDetalle`,
                    state: {
                      vendedor: this.props.selectedVendedor,
                      cliente: this.props.selectedCliente,
                      catalogoClientes: this.props.clientes,
                      periodo: item,
                    },
                  });
                }}
                registros={registrosDetalles}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.EstadoCuentaStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...EstadoCuentaStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('estadocuenta')(EstadoCuenta as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
