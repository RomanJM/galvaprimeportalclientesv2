﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import { AnyAction, bindActionCreators } from 'redux';

import * as ContactosStore from '../store/ContactosStore';
import * as LogInStore from '../store/LogInStore';
import * as NavbarTopStore from '../store/NavbarTopStore';
import { translate } from 'react-i18next';

type ContactosProps =
    ContactosStore.ContactosStoreState &
    typeof ContactosStore.actionCreators &
    LogInStore.LogInStoreState &
    typeof LogInStore.actionCreators &
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;

 class Contactos extends React.Component<ContactosProps, {}>{

    componentDidMount() {
        this.props.setVista("Contactos", false);
        var rol = this.props.rolusuario;
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = this.props.Usuario_id;
            this.props.requestInformacionInicialEmail(cliente);
            this.props.requestInformacionInicialTelefono(cliente);
        }

    }
     public render() {
        const { t } = this.props;
         var rol = this.props.Usuario_id;
        return <div >
            <div style = {{marginTop : "2%"}}>            
                <div >
                    <div className="row" style={{width:'100%'}}>
                     
                        <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div className="row">
                                <div className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
                            <strong style={{color: "#0e7ec3"}}>{t('lbl_contacto')}</strong>
                            <p style={{fontSize: 17, textAlign: "left" }}>{t('msg_contacto')}</p>
								
                                <strong style={{color: "#0e7ec3"}}>{t('lbl_ubicacion')}</strong>
                            <ul className="list-with-icon list-unstyled" style={{ fontSize: 17 }}>
                                <li><span ><span ><i className="fas fa-house-user" style={{ marginRight : 4 }}></i> </span></span>{t('msg_ubicacion')}</li>
                                <li><span ><i className="fas fa-phone" style={{ marginRight: 4 }}></i>{t('msg_ubiccion_1')}</span></li>
                                <li><span ><i className="fas fa-phone" style={{ marginRight: 4 }}></i>{t('msg_ubiccion_2')}</span></li>
                                <li><span ><i className="fas fa-envelope" style={{ marginRight: 4 }}></i>{t('msg_ubiccion_3')}</span></li>
                            </ul>
                                </div>
                                
                            </div>
                           
                        </div>
                        <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div className="row">
                        <div className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.414742824558!2d-100.49586498454485!3d25.690688717805223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x866298f3be209c9b%3A0xce70656839270424!2sGalvaprime!5e0!3m2!1ses-419!2smx!4v1637601338966!5m2!1ses-419!2smx" 
width="600" 
height="450" style={{border:0}}
loading="lazy"></iframe>
							

                        </div>
                       
                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>;
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        ...state.LogInStore,
        ...state.NavbarTopStore
    }
  }
  const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators( {
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators
    }, dispatch)
  }
  const withTranslate = translate('contactos')(Contactos as any);
export default connect(mapStateToProps,mapDispatchToProps)(withTranslate);