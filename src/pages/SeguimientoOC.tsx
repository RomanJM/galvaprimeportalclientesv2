import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import { translate } from 'react-i18next'
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as SeguimientoOCStore from "../store/SeguimientoOCStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import { AnyAction, bindActionCreators } from "redux";
import GraficaGral from "../common/GraficaGeneral";
import DetallesGrafica from "../common/DetallesGrafica";

/**
 * Components
 * */

import Loading from "../common/Loading";
type SeguimientoOCProps = SeguimientoOCStore.SeguimientoOCStoreState &
  typeof SeguimientoOCStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators &
  {t?: any} &
  RouteComponentProps<{}>;
interface SeguimientoOCState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  unidadPeso: string;
  imgExcel: string;
  imgLB: string;
  imgKG: string;
  btnKG: string;
  btnLB: string;
}

class SeguimientoOC extends React.Component<
  SeguimientoOCProps,
  SeguimientoOCState
> {
  constructor(props: any) {
    //if (props.history.location.state) {
    //    console.log(props.history.location.state);
    //}
    super(props);
    this.state = {
      cliente: null,
      vendedor: null,
      nombreCliente: "",
      open: true,
      unidadPeso: "KG",
      imgExcel: "i-excel-2.png",
      imgLB: "i-lb-2.png",
      imgKG: "i-kg-1.png",
      btnKG: "boton-activo",
      btnLB: "boton-inactivo",
    };
  }
  public filtrar = () => {
    var cliente = _.find(this.props.clientesSOC, (x: any) => {
      return x.clave == this.state.cliente ? this.state.cliente.value : "";
    });
    //this.setState({nombreCliente : cliente ? cliente.descripcion : "", open: true});
    //this.props.setClienteVendedor(this.state.cliente ? this.state.cliente.value:'', this.state.vendedor ? this.state.vendedor.value:'');
    //this.props.requestInformacionInicialSOC(this.state.cliente ? this.state.cliente.value:'', this.state.vendedor ? this.state.vendedor.value:'');
  };
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("SeguimientoOC", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.requestInformacionInicialSOC(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
  }
  public render() {
    const { t } = this.props;
    var unidadPeso = this.state.unidadPeso;
    var dataset: any[] = [];
    var registrosDetalle: any = [];
    var total = 0;
    var colores: any = [];
    /*const data = this.props.registrosSOC.map((item: any) => {
      return this.state.unidadPeso == "KG"
        ? item.Valor
        : this.state.unidadPeso == "LB"
        ? item.Valor2
        : item.Valor3;
    });*/
    const data = this.props.registrosSOC.map((item: any) => {
      return this.state.unidadPeso == "KG"
        ? item.Valor
        : this.state.unidadPeso == "LB"
        ? item.Valor2
        : item.Valor3;
    });
    this.props.registrosSOC.map((item: any) => {
      var color = "";
      if (item.Grupo == "Créditos") {
        color = "#F0AC38";
      } else if (item.Grupo === "Atrasos") {
        color = "#E93314";
      } else if (item.Grupo === "En Fecha") {
        color = "#00B95D";
      } else {
        color = "#4A6CC5";
      }
      colores.push(color);
      dataset.push([
        item.Grupo,
        this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3,
        color,
      ]);
    });
    this.props.registrosSOC.map((item: any) => {
      total =
        total +
        (this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3);
      var valor =
        this.state.unidadPeso == "KG"
          ? numeral(item.Valor).format("0,0")
          : this.state.unidadPeso == "LB"
          ? numeral(item.Valor2).format("0,0")
          : numeral(item.Valor3).format("0,0");

      registrosDetalle.push({
        Grupo: t(item.Grupo),
        Valor: numeral(valor).format("0,00"),
      });
    });
    const labels = this.props.registrosSOC.map((item: any) => {
      return t(item.Grupo);
    });
    const props = this.props;
    var cliente = this.state.cliente;
    return this.props.isloadingSeguimientoOC ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="containerpage">
        <div
          style={{ display: "flex", alignItems: "center", margin: 0 }}
          className="row"
        >
         <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-7 col-md-7 col-sm-12 col-12">
            <div className="row">
              {this.props.registrosSOC.length > 0 ? (
                <GraficaGral
                  title={""}
                  label={t('title_seguimiento_oc')}
                  labels={labels}
                  data={data}
                  backgroundColor={colores}
                  onclick={(items: any) => {
                    if (items[0]) {
                      var index = items[0]._index;
                      const vencimient = props.registrosSOC[index];
                      props.history.push({
                        pathname: `/SeguimientoOCDetalle`,
                        state: {
                          vencimiento: vencimient ? vencimient.Grupo : "",
                          vendedor: props.selectedVendedor,
                          cliente: props.selectedCliente,
                          catalogoClientes: props.clientes,
                          unidadPeso: unidadPeso,
                        },
                      });
                    }
                  }}
                />
              ) : null}
            </div>
          </div>
          <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
            <div className="row">
              <div className="col"></div>
              <div className="col">
                <div style={{ display: "flex", lineHeight: "50%" }}>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("1", "2"),
                      });
                    }}
                    onClick={() => {
                      this.props.requestDowloadExcelSOC(
                        this.props.selectedCliente,
                        this.props.selectedVendedor,
                        t
                      );
                    }}
                    className="circulo-iconos boton-inactivo"
                  >
                    <img
                      src={`../../images/icons/${this.state.imgExcel}`}
                      style={{ width: "33px", margin: "auto" }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgKG;
                      this.setState({
                        imgKG: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgKG:
                          this.state.btnKG == "boton-activo"
                            ? "i-kg-1.png"
                            : "i-kg-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "KG",
                        btnKG: "boton-activo",
                        btnLB: "boton-inactivo",
                        imgKG: "i-kg-1.png",
                        imgLB: "i-lb-2.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnKG}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgKG}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgLB;
                      this.setState({
                        imgLB: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgLB:
                          this.state.btnLB == "boton-activo"
                            ? "i-lb-1.png"
                            : "i-lb-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "LB",
                        btnKG: "boton-inactivo",
                        btnLB: "boton-activo",
                        imgKG: "i-kg-2.png",
                        imgLB: "i-lb-1.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnLB}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgLB}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col"></div>
            </div>
            <div
              style={{  }}
              className="row"
            >
              <DetallesGrafica
                registros={registrosDetalle}
                labelTotal={
                  "Total " +
                  this.state.unidadPeso
                }
                total = { numeral(total).format("0,0")}
                onClickTotal={() => {
                  props.history.push({
                    pathname: `/SeguimientoOCDetalle`,
                    state: {
                      vencimiento: "",
                      vendedor: props.selectedVendedor,
                      cliente: props.selectedCliente,
                      catalogoClientes: props.clientes,
                      unidadPeso: this.state.unidadPeso,
                    },
                  });
                }}
                onClickDetalle={(item: any) => {
                  props.history.push({
                    pathname: `/SeguimientoOCDetalle`,
                    state: {
                      vendedor: this.props.selectedVendedor,
                      cliente: this.props.selectedCliente,
                      vencimiento: item,
                      unidadPeso: this.state.unidadPeso,
                      catalogoClientes: this.props.clientes,
                    },
                  });
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.SeguimientoOCStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...SeguimientoOCStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};

const withTranslate = translate('seguimientooc')(SeguimientoOC as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
