import * as React from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { bindActionCreators } from "redux";
import { ApplicationState } from "../store";
import * as FacturasStore from "../store/FacturasStore";
import * as RemisionesStore from "../store/RemisionesStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Tabla from "../common/Tabla";
import Select from "react-select";
import moment from "moment";
import { translate } from 'react-i18next'

/**
 * Components
 * */
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
  DatePicker,
} from "@material-ui/pickers";
import Loading from "../common/Loading";
import {
  FormControl,
  IconButton,
  Button,
  InputLabel,
  MenuItem,
  Zoom,
  TextField,
  Input,
  InputAdornment,
} from "@material-ui/core";

type FacturasProps = FacturasStore.FacturasStoreState &
  typeof FacturasStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators &
  RemisionesStore.RemisionesStoreState &
  typeof RemisionesStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;

interface FacturasState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  fechaInicial: any;
  fechaFinal: any;
  Factura: any;
  Remision: any;
}

class Facturas extends React.Component<FacturasProps, FacturasState> {
  constructor(props: any) {
    super(props);
    this.state = {
      cliente: null,
      vendedor: null,
      nombreCliente: "",
      open: true,
      fechaInicial: new Date(),
      fechaFinal: new Date(),
      Factura: "",
      Remision: "",
    };
  }
  public filtrar = () => {
    var cliente = _.find(this.props.clientesFacturas, (x: any) => {
      return x.clave == this.state.cliente;
    });
    this.setState({
      nombreCliente: cliente ? cliente.descripcion : "",
      open: true,
    });
    //this.props.requestInformacionInicial(this.state.cliente, this.state.vendedor);
  };
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("Facturas", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    var fechaInicio = moment(this.props.fechaInicioFactura).format(
      "YYYY-MM-DD"
    );
    var fechaFin = moment(this.props.fechaFinFactura).format("YYYY-MM-DD");
    this.props.requestInformacionInicialFacturas(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor,
      fechaInicio,
      fechaFin,
      this.props.facturaDoc,
      this.props.remisionDoc,
      this.props.NumAtCard
    );
  }
  public render() {
    const { t }= this.props;
    var columns = [
      {
        Header: t('tbl_fecha'),
        accessor: "DocDate",
        Cell: (row: any) => {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {moment(row.value).format("YYYY-MM-DD")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_factura'),
        accessor: "DocNum",
        Cell: (row: any) => {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {row.value}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_referencia'),
        accessor: "NumAtCard",
        Cell: (row: any) => {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {row.value}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },      
      {
        Header: t('tbl_remision'),
        accessor: "DocNumEnt",
        Cell: (row: any) => {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {row.value}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
    ];
    var colsfinal = null;
    if (
      this.props.selectedCliente == "" ||
      this.props.selectedCliente == "0" ||
      !this.props.selectedCliente
    ) {
      var col = {
        Header: t('tbl_cliente'),
        accessor: "CardName",
        Cell: (row: any) => {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {row.value}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      };
      colsfinal = [...columns, col];
    }

    var colRem = {
      Header: t('tbl_remision'),
      accessor: "",
      Cell: (row: any) => {
        return (
          <div
            style={{ textAlign: "right", paddingRight: "20px", minWidth: 100 }}
          >
            <IconButton
              title={t('msg_descarga_PDF')}
              onClick={() => {
                this.props.requestDowloadArchivoRemision(
                  row.original.DocNumEnt,
                  "PDF"
                );
              }}
              color="default"
              disableRipple
              component="span"
            >
              <img
                src={`../../images/icons/i-pdf.png`}
                style={{ width: "20px", margin: "auto" }}
              />
            </IconButton>
          </div>
        );
      },
      width: 150,
      headerStyle: { fontWeight: "bold" },
    };

    var colFac = {
      Header: t('tbl_factura'),
      accessor: "",
      Cell: (row: any) => {
        return (
          <div
            style={{ textAlign: "right", paddingRight: "20px", minWidth: 100 }}
          >
            <IconButton
              title={t('msg_descarga_XML')}
              onClick={() => {
                this.props.requestDowloadArchivoFacturas(
                  row.original.DocNum,
                  "XML"
                );
              }}
              color="default"
              disableRipple
              component="span"
            >
              <img
                src={`../../images/icons/i-xml.png`}
                style={{ width: "20px", margin: "auto" }}
              />
            </IconButton>
            <IconButton
              title={t('msg_descarga_PDF')}
              onClick={() => {
                this.props.requestDowloadArchivoFacturas(
                  row.original.DocNum,
                  "PDF"
                );
              }}
              color="default"
              disableRipple
              component="span"
            >
              <img
                src={`../../images/icons/i-pdf.png`}
                style={{ width: "20px", margin: "auto" }}
              />
            </IconButton>
          </div>
        );
      },
      width: 150,
      headerStyle: { fontWeight: "bold" },
    };
    var colCer = {
      Header: t('tbl_certificado'),
      accessor: "",
      Cell: (row: any) => {
        return (
          <div style={{ textAlign: "right", paddingRight: "20px" }}>
            <IconButton
              title={t('msg_descarga_PDF')}
              onClick={() => {
                this.props.requestDowloadArchivoCertificadoFactura(
                  row.original.DocNumEnt
                );
              }}
              color="default"
              disableRipple
              component="span"
            >
              <img
                src={`../../images/icons/i-pdf.png`}
                style={{ width: "20px", margin: "auto" }}
              />
            </IconButton>
          </div>
        );
      },
      headerStyle: { fontWeight: "bold" },
    };
    var colRec: any = {
      Header: t('tbl_recibo'),
      accessor: "",
      Cell: (row: any) => {
        if (row.original._Recibo > 0) {
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              <IconButton
                title={t('msg_descarga_PDF')}
                onClick={() => {
                  this.props.requestDowloadArchivoReciboFactura(
                    row.original.DocNumEnt
                  );
                }}
                color="default"
                disableRipple
                component="span"
              >
                <img
                  src={`../../images/icons/i-pdf.png`}
                  style={{ width: "20px", margin: "auto" }}
                />
              </IconButton>
            </div>
          );
        }
      },
      headerStyle: { fontWeight: "bold" },
    };
    colsfinal = [...columns, colFac, colRem, colCer];

    var rol: any = this.props.rolusuario;
    if (rol != "CLI") colsfinal.push(colRec);
    return this.props.isloadingFacturas ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="detallepage">
        <div className="row">
          <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-11 col-md-11 col-sm-12 col-12">
            <div style={{}} className="row">
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePicker
                    label={t('lbl_fecha_inicial')}
                    value={this.props.fechaInicioFactura}
                    onChange={(fecha: any) => {
                      this.props.setFechaInicio(fecha);
                    }}
                    format="MM/dd/yyyy"
                  />
                  {/*<KeyboardDatePicker
            disableToolbar
            format="MM/dd/yyyy"
            margin="normal"
            id="fecha-inicial"
            label="Fecha inicial"
            value={this.state.fechaInicial}
            onChange={(fecha: any) => {
                this.setState({ fechaInicial: fecha })
            }}
            KeyboardButtonProps={{
                'aria-label': 'Select fecha',
            }}
        />*/}
                </MuiPickersUtilsProvider>
              </div>
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePicker
                    label={t('lbl_fecha_final')}
                    value={this.props.fechaFinFactura}
                    onChange={(fecha: any) => {
                      this.props.setFechaFin(fecha);
                    }}
                    format="MM/dd/yyyy"
                  />
                  {/*<KeyboardDatePicker
            disableToolbar
            format="MM/dd/yyyy"
            margin="normal"
            id="fecha-final"
            label="Fecha final"
            value={this.state.fechaFinal}
            onChange={(fecha: any) => {
                this.setState({ fechaFinal: fecha })
            }}
            KeyboardButtonProps={{
                'aria-label': 'Select fecha',
            }}
        />*/}
                </MuiPickersUtilsProvider>
              </div>
              <div className="col-xs-12 col-sm-12 col-lg-1 col-md-1">
                <FormControl className={""}>
                  <InputLabel htmlFor="input-with-icon-adornment">
                    {t('lbl_num_factura')}
                  </InputLabel>
                  <Input
                    id="input-with-icon-adornment"
                    value={this.props.facturaDoc}
                    onChange={(value: any) => {
                      this.props.setFacturaDoc(value.target.value);
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <i className="fas fa-file-invoice" />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>

              <div className="col-xs-12 col-sm-12 col-lg-1 col-md-1">
                <FormControl className={""}>
                  <InputLabel htmlFor="input-with-icon-adornment">
                    {t('lbl_num_remision')}
                  </InputLabel>
                  <Input
                    id="input-with-icon-adornment"
                    value={this.props.remisionDoc}
                    onChange={(value: any) => {
                      this.props.setRemisionDoc(value.target.value);
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <i className="fas fa-file-invoice" />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>              

              <div className="col-xs-12 col-sm-12 col-lg-3 col-md-3">
                <FormControl className={""}>
                  <InputLabel htmlFor="input-with-icon-adornment">
                    {t('lbl_ref_cliente')}
                  </InputLabel>
                  <Input
                    id="input-with-icon-adornment"
                    value={this.props.NumAtCard}
                    onChange={(value: any) => {
                      this.props.setNumAtCard(value.target.value);
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <i className="fas fa-file-invoice" />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>





              <div className="col-xs-12 col-sm-12 col-lg-1 col-md-1">
                <div
                  style={{
                    borderRadius: 1,
                    display: "flex",
                    alignItems: "center",
                    width: "40%",
                    height: "65%",
                    cursor: "pointer",
                    backgroundColor: "#0e7ec3",
                    color: "#FFFFFF",
                  }}
                  onClick={() => {
                    var fechaInicio = moment(
                      this.props.fechaInicioFactura
                    ).format("YYYY-MM-DD");
                    var fechaFin = moment(this.props.fechaFinFactura).format(
                      "YYYY-MM-DD"
                    );
                    this.props.requestInformacionInicialFacturas(
                      this.props.selectedCliente,
                      this.props.selectedVendedor,
                      fechaInicio,
                      fechaFin,
                      this.props.facturaDoc,
                      this.props.remisionDoc,
                      this.props.NumAtCard
                    );
                  }}
                >
                  <img
                    src={`../../images/icons/i-consulta-1.png`}
                    style={{ width: "20px", margin: "auto" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style={{ marginTop: 10 }} className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-12">
            {this.props.registrosFacturas.length > 0 ? (
              <Tabla
                columnas={colsfinal}
                registros={this.props.registrosFacturas}
                filterable={true}
              />
            ) : (
              <div style={{ textAlign: "center" }}>
                {" "}
                <span className="label label-table label-warning">
                  {t('msg_not_result')}
                </span>{" "}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.FacturasStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
    ...state.RemisionesStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...FacturasStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
      ...RemisionesStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('facturas_cert')(Facturas as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
