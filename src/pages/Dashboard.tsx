import * as _ from "lodash";
import numeral from "numeral";
import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store/index";
import * as DashboardStore from "../store/DashboardStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Loading from "../common/Loading";
import { AnyAction, bindActionCreators } from "redux";
import { translate } from 'react-i18next'
type DashboardProps = DashboardStore.DashboardStoreState &
  typeof DashboardStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators &
  {t?: any, i18n?: any} &
  RouteComponentProps<{}>;
interface Dashboardtate {
  cliente: any;
  vendedor: any;
}
declare const $: any;
class Dashboard extends React.Component<DashboardProps, Dashboardtate> {
  constructor(props: any) {
    super(props);
    this.state = {
      cliente: null,
      vendedor: null,
    };
  }

  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.requestInformacionInicialDahboard(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.setVista("Dashboard", true);
  }

  public render() {
    const {t} = this.props;
    var totalInventario = 0;
    var totalOrdenCompra = 0;
    var totalEmbarque = 0;
    var totalEstadoCuenta = 0;

    totalInventario = _.sumBy(
      this.props.registrosDashboard.ResumenAlmacen,
      (x: any) => {
        return x.Valor;
      }
    );
    totalOrdenCompra = _.sumBy(
      this.props.registrosDashboard.ResumenOV,
      (x: any) => {
        return x.Valor;
      }
    );
    totalEmbarque = _.sumBy(
      this.props.registrosDashboard.ResumenEmbarques,
      (x: any) => {
        return x.Valor;
      }
    );
    var primerRegistro = _.find(
      this.props.registrosDashboard.ResumenEdoCta,
      (item: any) => {
        return item;
      }
    );
    var valor = primerRegistro ? primerRegistro.Valor2 : 0;
    var registrosFilter = this.props.registrosDashboard
      ? _.filter(this.props.registrosDashboard.ResumenEdoCta, (item: any) => {
          return (
            item.ValorTxt.substr(item.ValorTxt.length - 3) ===
            (valor == 0 || valor == 2 ? "USD" : "MXN")
          );
        })
      : [];

    totalEstadoCuenta = _.sumBy(registrosFilter, (x: any) => {
      return x.Valor;
    });

    return (
      <div>
        {this.props.isloadingDashboard ? (
          <Loading mensage={t('common:msg_procesando')} />
        ) : (
          <div style={{ marginLeft: 7 }} className="container-fluid dashboard">
            <div className="row">
              <div className="col col-lg-12 col-md-12 col-sm-12 col-12"></div>
            </div>
            <div className="row">
              <div className="col col-lg-12 col-md-12 col-sm-12 col-12">
                {/*  <div style={{fontSize : 13}} className="row">
   
                <div className="col col-lg-12 col-md-12 col-sm-12 col-12">
                 <div style={{display:'flex', alignItems : "center"}} className="row">
                 <div style={{color : "#0D7EC2", fontWeight:"bold", fontSize : "20px",textAlign : "center"}} className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     Resumen de cliente
                   </div>
                   <div style={{fontSize : "18px",textAlign : "center"}} className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <div className="row">
                     <div style={{marginLeft : 0}} className="col col-lg-6 col-md-6 col-sm-12 col-12">
                     Nombre:   <br />
                     ID Cliente:    <br />
                     Empresa:    <br />
                   </div>
                   <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                     {this.props.clienteSeleccionado ? this.props.clienteSeleccionado.descripcion : this.props.NombreUsuario} <br />
                     {this.props.clienteSeleccionado ? this.props.clienteSeleccionado.clave : this.props.Usuario_id} <br />
                     {this.props.vendedorSeleccionado ? this.props.vendedorSeleccionado.descripcion : ""} <br />
                   </div>
                     </div>
                    </div>
             
                 </div>
   
                </div>
                </div> */}
                <div
                  style={{ display: "flex", alignItems: "center" }}
                  className="row"
                >
                  <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div
                      onClick={() => {
                        const menu = _.find(
                          this.props.MenuUsuario,
                          (item: any) => {
                            return item.Enlace === "SeguimientoOC";
                          }
                        );
                        if (menu) {
                          this.props.setMenuActive(
                            menu.Menu_id_padre,
                            menu.Menu_id
                          );
                          this.props.history.push({
                            pathname: `/SeguimientoOC`,
                          });
                        }
                      }}
                      style={{
                        position: "relative",
                        display: "block",
                        alignItems: "center",
                        cursor: "pointer",
                        color: "#fff",
                        backgroundColor: "#125B87",
                        borderRadius: 4,
                        padding: 20,
                        textAlign: "center",
                        height: "14em",
                      }}
                    >
                      <div className="row">
                        {" "}
                        <img
                          src={"images/icons/i-seguimiento-1.png"}
                          style={{ width: "70px", margin: "auto" }}
                        />{" "}
                      </div>
                      <div
                        style={{ textAlign: "center", margin: 0 }}
                        className="row"
                      >
                        {" "}
                        <div>{t('lbl_seguimiento_oc')}</div>{" "}
                      </div>
                      <div
                        style={{
                          fontSize: 18,
                          position: "absolute",
                          left: "1%",
                          bottom: "20%",
                          right: "1%",
                        }}
                        className="row"
                      >
                        {" "}
                        <div>
                          {numeral(
                            totalOrdenCompra ? totalOrdenCompra : 0
                          ).format("0,0")}{" "}
                          KG
                        </div>{" "}
                      </div>
                    </div>
                  </div>
                  <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div
                      onClick={() => {
                        const menu = _.find(
                          this.props.MenuUsuario,
                          (item: any) => {
                            return item.Enlace === "Inventarios";
                          }
                        );
                        if (menu) {
                          this.props.setMenuActive(
                            menu.Menu_id_padre,
                            menu.Menu_id
                          );
                          this.props.history.push({
                            pathname: `/Inventarios`,
                          });
                        }
                      }}
                      style={{
                        position: "relative",
                        display: "block",
                        alignItems: "center",
                        cursor: "pointer",
                        color: "#fff",
                        backgroundColor: "#125B87",
                        borderRadius: 4,
                        padding: 20,
                        textAlign: "center",
                        height: "14em",
                      }}
                    >
                      <div className="row">
                        {" "}
                        <img
                          src={"images/icons/i-inventario-1.png"}
                          style={{ width: "70px", margin: "auto" }}
                        />{" "}
                      </div>
                      <div
                        style={{ textAlign: "center", margin: 0 }}
                        className="row"
                      >
                        {" "}
                        <div>{t('lbl_inventario_pt')}</div>{" "}
                      </div>
                      <div
                        style={{
                          fontSize: 18,
                          position: "absolute",
                          left: "1%",
                          bottom: "20%",
                          right: "1%",
                        }}
                        className="row"
                      >
                        {" "}
                        <div>
                          {numeral(
                            totalInventario ? totalInventario : 0
                          ).format("0,0")}{" "}
                          KG
                        </div>{" "}
                      </div>
                    </div>
                  </div>
				  
                  <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div
                      onClick={() => {
                        const menu = _.find(
                          this.props.MenuUsuario,
                          (item: any) => {
                            return item.Enlace === "Embarques";
                          }
                        );
                        if (menu) {
                          this.props.setMenuActive(
                            menu.Menu_id_padre,
                            menu.Menu_id
                          );
                          this.props.history.push({
                            pathname: `/Embarques`,
                          });
                        }
                      }}
                      style={{
                        position: "relative",
                        display: "block",
                        alignItems: "center",
                        cursor: "pointer",
                        color: "#fff",
                        backgroundColor: "#125B87",
                        borderRadius: 4,
                        padding: 20,
                        textAlign: "center",
                        height: "14em",
                      }}
                    >
                      <div className="row">
                        {" "}
                        <img
                          src={"images/icons/i-embarque-1.png"}
                          style={{ width: "70px", margin: "auto" }}
                        />{" "}
                      </div>
                      <div
                        style={{ textAlign: "center", margin: 0 }}
                        className="row"
                      >
                        {" "}
                        <div>{t('lbl_embarques_hist')}</div>{" "}
                      </div>
                      <div
                        style={{
                          fontSize: 18,
                          position: "absolute",
                          left: "1%",
                          bottom: "20%",
                          right: "1%",
                        }}
                        className="row"
                      >
                        <div>
                          {numeral(totalEmbarque ? totalEmbarque : 0).format(
                            "0,0"
                          )}{" "}
                          KG
                        </div>
                      </div>
                    </div>
                  </div>				  
				  
				  
				  
                  <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div
                      onClick={() => {
                        const menu = _.find(
                          this.props.MenuUsuario,
                          (item: any) => {
                            return item.Enlace === "EstadoCuenta";
                          }
                        );
                        if (menu) {
                          this.props.setMenuActive(
                            menu.Menu_id_padre,
                            menu.Menu_id
                          );
                          this.props.history.push({
                            pathname: `/EstadoCuenta`,
                          });
                        }
                      }}
                      style={{
                        position: "relative",
                        display: "block",
                        alignItems: "center",
                        cursor: "pointer",
                        color: "#fff",
                        backgroundColor: "#125B87",
                        borderRadius: 4,
                        padding: 20,
                        textAlign: "center",
                        height: "14em",
                      }}
                    >
                      <div className="row">
                        {" "}
                        <img
                          src={"images/icons/i-estado-1.png"}
                          style={{ width: "70px", margin: "auto" }}
                        />{" "}
                      </div>
                      <div
                        style={{ textAlign: "center", margin: 0 }}
                        className="row"
                      >
                        <div>{t('lbl_estado_cuenta')}</div>{" "}
                      </div>
                      <div
                        style={{
                          fontSize: 18,
                          position: "absolute",
                          left: "1%",
                          bottom: "20%",
                          right: "1%",
                        }}
                        className="row"
                      >
                        <div>
                          {" "}
                          {numeral(
                            totalEstadoCuenta ? totalEstadoCuenta : 0
                          ).format("$0,0.00")}
                        </div>
                      </div>
                    </div>
                  </div>
				  

				  
				  
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.DashboardStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...DashboardStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('dashboard')(Dashboard as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
