﻿import * as React from 'react';
import {  bindActionCreators } from 'redux';
import { translate } from 'react-i18next'
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as NavbarTopStore from '../store/NavbarTopStore';
type TerminosProps =
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?:  any, lng?: any} &
    RouteComponentProps<{}>;
class Terminos extends React.Component<TerminosProps, {}>{
    componentDidMount(){
        this.props.setVista("Terminos", false);
    }
    public render() {
        const { t } = this.props;
        console.log("props",this.props);
        return <div  >
            <div >
                <div className="acercadepage" style={{ overflowY:'scroll', overflowX:'hidden'}} >
                    <div className="row" style={{width:'100%'}}>
                    <embed src={this.props.lng == "es" ? "pdf/terminosycondiciones/es.pdf" : "pdf/terminosycondiciones/en.pdf"} width="500" height="375" 
 type="application/pdf" />
                    {/*<object width="100%" height="600" data={this.props.lng == "es" ? "pdf/terminosycondiciones/es.pdf" : "pdf/terminosycondiciones/en.pdf"}
                     type="application/pdf">   </object>*/}
                       {/* <div className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div style={{ textAlign: "center" }}>
                                <h4 style={{ fontWeight: 'bold' }} className="section-main-title">{t('lbl_gar_general')}</h4>
                             </div>
                                <br />
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_intro')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                {t('msg_intro_1')} <strong>{t('common:lbl_empresa_may')}</strong>.
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                {t('msg_intro_2')} <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_intro_3')} <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_intro_4')} <strong>{t('common:lbl_empresa_may')}</strong>.
                            </p>
                            <p style={{ fontSize: 17 }}>
                                < h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_garantia')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>{t('common:lbl_empresa_may')}</strong>, {t('msg_garantia_1')} <strong>{t('common:lbl_empresa_may')}</strong>,
                                {t('msg_garantia_2')}
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_politicas')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_politicas_1')} <br/>
                                {t('msg_politicas_2')} <br />
                                {t('msg_politicas_3')}<br />
                                {t('msg_politicas_4')}<br />
                                {t('msg_politicas_5')} <br />
                                {t('msg_politicas_6')} <br />
                                {t('msg_politicas_7')} <br />
                                {t('msg_politicas_8')} <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_politicas_9')}<br />
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_variaciones_peso')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                {t('msg_variaciones_peso_1')} <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_variaciones_peso_2')} <br/>
                                {t('msg_variaciones_peso_3')}
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_golpes')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>{t('common:lbl_empresa_may')}</strong> {t('msg_golpes_1')}
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >{t('lbl_expriracion')}</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                {t('msg_expriracion')}
                            </p>
                        </div>
                       */}
                    </div>

                </div>
            </div>
        </div>;
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        ...state.NavbarTopStore
    }
  }
  const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators( {
      ...NavbarTopStore.actionCreators
    }, dispatch)
  }
  const withTranslate = translate('terminos')(Terminos as any);
  export default connect(mapStateToProps,mapDispatchToProps)(withTranslate);