﻿import * as React from 'react';
import {  bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as NavbarTopStore from '../store/NavbarTopStore';
import { translate } from 'react-i18next';
type AvisoPrivacidadProps =
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;
class AvisoPrivacidad extends React.Component<AvisoPrivacidadProps, {}>{
    componentDidMount(){
        this.props.setVista("AvisoPrivacidad", false);
    }
    public render() {
        const { t } = this.props;
        return <div  >
            <div  >
                <div style={{height : "18em", overflow : "scroll", marginTop : "3%"}} >
                    <div style={{ display: "flex", alignItems: "center" }} className="row">
                        <div style={{ display: "flex", alignItems :"center" }} className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h4 style={{ color: "#0e7ec3",textAlign: "left"}} className="section-main-title">{t('lbl_quienes_somos')}</h4>
							
                        </div>
                        <div  className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">							
                            <p style={{ textAlign: "left", fontSize:17 }}>
                                <strong>{t('common:lbl_empresa_min')}  </strong>{t('msg_quienes_som_1')} <a href="http://www.galvaprime.com" target="blank">{t('common:pagina_internet_g')}</a>, {t('msg_quienes_som_2')}
                            </p>                            
                         </div>
                    </div>
                    <div style={{ display: "flex", alignItems: "center" }} className="row">
                        <div  className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h4 style={{ color: "#0e7ec3",textAlign: "justify" }} className="section-main-title">{t('lbl_datos_personales')}</h4>
                        </div>
                        <div  className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17 }}>
                                {t('msg_datos_personales_1')}
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17}}>
                                <ul className="list-group">
                                    <li ><i className="fas fa-check"></i> {t('msg_datos_personales_2')}</li>
                                    <li><i className="fas fa-check"></i> {t('msg_datos_personales_3')}</li>
                                    <li><i className="fas fa-check"></i>{t('msg_datos_personales_4')} </li>
                                    <li><i className="fas fa-check"></i> {t('msg_datos_personales_5')}</li>
                                </ul>
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17}}>
                                {t('msg_datos_personales_6')}
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17 }}>
                                {t('msg_datos_personales_7')}  <strong> {t('common:correo_ventas')} </strong>
                            </p>
                            <p style={{ textAlign: "justify", fontSize: 17 }}>
                                {t('msg_datos_personales_8')}
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>;
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        ...state.NavbarTopStore
    }
  }
  const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators( {
      ...NavbarTopStore.actionCreators
    }, dispatch)
  }
  const withTranslate = translate('avisoprivacidad')(AvisoPrivacidad as any);
  export default connect(mapStateToProps,mapDispatchToProps)(withTranslate);