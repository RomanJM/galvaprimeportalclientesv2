import * as React from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import { bindActionCreators } from "redux";
import * as CertificadosStore from "../store/CertificadosStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Tabla from "../common/Tabla";
import Select from "react-select";
import moment from "moment";
/**
 * Components
 * */
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
  DatePicker,
} from "@material-ui/pickers";
import Loading from "../common/Loading";
import {
  FormControl,
  IconButton,
  Button,
  InputLabel,
  MenuItem,
  Zoom,
  TextField,
  Input,
  InputAdornment,
} from "@material-ui/core";
import { translate } from "react-i18next";

type CertificadosProps = CertificadosStore.CertificadosStoreState &
  typeof CertificadosStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;

interface CertificadosState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  fechaInicial: any;
  fechaFinal: any;
  Factura: any;
  Lote: any;
}

class Certificados extends React.Component<
  CertificadosProps,
  CertificadosState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      cliente: null,
      vendedor: null,
      nombreCliente: "",
      open: true,
      fechaInicial: new Date(),
      fechaFinal: new Date(),
      Factura: "",
      Lote: "",
    };
  }
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("Certificados", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    var fechaInicio = moment(this.props.fechaInicioFactura).format(
      "YYYY-MM-DD"
    );
    var fechaFin = moment(this.props.fechaFinFactura).format("YYYY-MM-DD");
    this.props.requestInformacionInicialCertificados(
      cliente,
      vendedor,
      fechaInicio,
      fechaFin,
      this.props.certificado,
      this.props.lote,
      this.props.NumAtCardcer
    );
  }
  public render() {
    const { t }= this.props;
    var columns = [
      {
        Header: t('tbl_fecha'),
        accessor: "DocDate",
        Cell: (row: any) => {
          return moment(row.value).format("YYYY-MM-DD");
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_factura'),
        accessor: "DocNumFactura",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_referencia'),
        accessor: "NumAtCard",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      }, 
      {
        Header: t('tbl_remision'),
        accessor: "DocNum",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('lbl_lote'),
        accessor: "Lote",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
    ];
    var colsfinal = null;
    if (
      this.props.selectedCliente == "" ||
      this.props.selectedCliente == "0" ||
      !this.props.selectedCliente
    ) {
      var col = {
        Header: t('tbl_cliente'),
        accessor: "CardName",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      };
      colsfinal = [...columns, col];
    }
    var col1 = {
      Header: t('tbl_certificado'),
      accessor: "",
      Cell: (row: any) => {
        return (
          <div style={{ paddingRight: "20px", minWidth: 100 }}>
            <IconButton
              title={t('msg_descarga_PDF')}
              onClick={() => {
                this.props.requestDowloadArchivoCertificados(
                  row.original.DocNum,
                  "PDF"
                );
              }}
              color="default"
              disableRipple
              component="span"
            >
              <img
                src={`../../images/icons/i-pdf.png`}
                style={{ width: "20px", margin: "auto" }}
              />
            </IconButton>
          </div>
        );
      },
      headerStyle: { fontWeight: "bold" },
      width: 180,
    };
    colsfinal = [...columns, col1];
    var rol = window.localStorage.getItem("rolusuario");
    if (rol != "CLI")
      colsfinal.push({
        Header: t('tbl_recibo'),
        accessor: "",
        Cell: (row: any) => {
          if (row.original._Recibo > 0) {
            return (
              <div style={{ paddingRight: "20px", minWidth: 100 }}>
                <IconButton
                  title={t('msg_descarga_PDF')}
                  onClick={() => {
                    this.props.requestDowloadArchivoReciboCertificados(
                      row.original.DocNum
                    );
                  }}
                  color="default"
                  disableRipple
                  component="span"
                >
                  <img
                    src={`../../images/icons/i-pdf.png`}
                    style={{ width: "20px", margin: "auto" }}
                  />
                </IconButton>
              </div>
            );
          }
        },
        headerStyle: { fontWeight: "bold" },
      });

    return this.props.isloadingCertificados ? (
      <Loading mensage={t('common:msg_procesando')}/>
    ) : (
      <div className="detallepage">
        <div className="row">
          <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-11 col-md-11 col-sm-12 col-12">
            <div style={{}} className="row">
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePicker
                    label={t('lbl_fecha_inicial')}
                    value={this.props.fechaInicioFactura}
                    onChange={(fecha: any) => {
                      this.props.setFechaInicio(fecha);
                    }}
                    format="MM/dd/yyyy"
                    style={{fontSize:'12px'}}
                  />
                  {/*<KeyboardDatePicker
            disableToolbar
            format="MM/dd/yyyy"
            margin="normal"
            id="fecha-inicial"
            label="Fecha inicial"
            value={this.state.fechaInicial}
            onChange={(fecha: any) => {
                this.setState({ fechaInicial: fecha })
            }}
            KeyboardButtonProps={{
                'aria-label': 'Select fecha',
            }}
        />*/}
                </MuiPickersUtilsProvider>
              </div>
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePicker
                    label={t('lbl_fecha_final')}
                    value={this.props.fechaFinFactura}
                    onChange={(fecha: any) => {
                      this.props.setFechaFin(fecha);
                    }}
                    format="MM/dd/yyyy"
                    style={{fontSize:'12px'}}
                  />
                  {/*<KeyboardDatePicker
            disableToolbar
            format="MM/dd/yyyy"
            margin="normal"
            id="fecha-final"
            label="Fecha final"
            value={this.state.fechaFinal}
            onChange={(fecha: any) => {
                this.setState({ fechaFinal: fecha })
            }}
            KeyboardButtonProps={{
                'aria-label': 'Select fecha',
            }}
        />*/}
                </MuiPickersUtilsProvider>
              </div>
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-3">
                <FormControl className={""}>
                  <InputLabel htmlFor="input-with-icon-adornment">
                    {t('lbl_cert_num_doc')}
                  </InputLabel>
                  <Input
                    id="input-with-icon-adornment"
                    value={this.props.certificado}
                    onChange={(value: any) => {
                      this.props.setCertificado(value.target.value);
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <i className="fas fa-file-invoice" />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
              <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                <FormControl className={""}>
                  <InputLabel htmlFor="input-with-icon-adornment">
                    {t('lbl_lote')}
                  </InputLabel>
                  <Input
                    id="input-with-icon-adornment"
                    value={this.props.lote}
                    onChange={(value: any) => {
                      this.props.setLote(value.target.value);
                    }}
                    startAdornment={
                      <InputAdornment position="start">
                        <i className="fas fa-file-invoice" />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
                 <div className="col-xs-12 col-sm-12 col-lg-2 col-md-2">
                              <FormControl className={""}>
                                <InputLabel htmlFor="input-with-icon-adornment">
                                  {t('lbl_ref_cliente')}
                                </InputLabel>
                                <Input
                                  id="input-with-icon-adornment"
                                  value={this.props.NumAtCardcer}
                                  onChange={(value: any) => {
                                    this.props.setNumAtCardCer(value.target.value);
                                  }}
                                  startAdornment={
                                    <InputAdornment position="start">
                                      <i className="fas fa-file-invoice" />
                                    </InputAdornment>
                                  }
                                />
                              </FormControl>
                            </div>
              <div className="col-xs-12 col-sm-12 col-lg-1 col-md-1">
                <div
                  style={{
                    borderRadius: 1,
                    display: "flex",
                    alignItems: "center",
                    width: "45%",
                    height: "60%",
                    cursor: "pointer",
                    backgroundColor: "#0e7ec3",
                    color: "#FFFFFF",
                  }}
                  onClick={() => {
                    var fechaInicio = moment(
                      this.props.fechaInicioFactura
                    ).format("YYYY-MM-DD");
                    var fechaFin = moment(this.props.fechaFinFactura).format(
                      "YYYY-MM-DD"
                    );
                    this.props.requestInformacionInicialCertificados(
                      this.props.selectedCliente,
                      this.props.selectedVendedor,
                      fechaInicio,
                      fechaFin,
                      this.props.certificado,
                      this.props.lote,
                      this.props.NumAtCardcer
                    );
                  }}
                >
                  <img
                    src={`../../images/icons/i-consulta-1.png`}
                    style={{ width: "20px", margin: "auto" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div style={{ marginTop: 10 }} className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-xs-12">
            {this.props.registrosCertificados.length > 0 ? (
              <Tabla
                columnas={colsfinal}
                registros={this.props.registrosCertificados}
                filterable={true}
              />
            ) : (
              <div style={{ textAlign: "center" }}>
                {" "}
                <span className="label label-table label-warning">
                  {t('msg_not_result')}
                </span>{" "}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.CertificadosStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...CertificadosStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('facturas_cert')(Certificados as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
