﻿import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { translate } from 'react-i18next'
import { ApplicationState } from "../store";
import * as EmbarquesStore from "../store/EmbarquesStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import { AnyAction, bindActionCreators } from "redux";
import GraficaGral from "../common/GraficaGeneral";
import DetallesGraficaEmbarques from "../common/DetallesGraficaEmbarques";
/**
 * Components
 * */
import Loading from "../common/Loading";

//declare const $;
type EmbarquesProps = EmbarquesStore.EmbarquesStoreState &
  typeof EmbarquesStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;

interface EmbarquesState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  unidadPeso: string;
  imgExcel: string;
  imgLB: string;
  imgKG: string;
  btnKG: string;
  btnLB: string;
}

class Embarques extends React.Component<EmbarquesProps, EmbarquesState> {
  constructor(props: any) {
    super(props);
    this.state = {
      cliente: "",
      vendedor: "",
      nombreCliente: "",
      open: true,
      unidadPeso: "KG",
      imgExcel: "i-excel-2.png",
      imgLB: "i-lb-2.png",
      imgKG: "i-kg-1.png",
      btnKG: "boton-activo",
      btnLB: "boton-inactivo",
    };
  }
  public meses_txt = (texto: any) => {
    const { t } = this.props;
    const months = [
      t("Ene"),
      t("Feb"),
      t("Mar"),
      t("Abr"),
      t("May"),
      t("Jun"),
      t("Jul"),
      t("Ago"),
      t("Sep"),
      t("Oct"),
      t("Nov"),
      t("Dic"),
    ];
    let arr = texto.split("-");
    return months[Number(arr[1]) - 1] + "-" + arr[0];
  };
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("Embarques", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.requestInformacionInicialEmbarques(cliente, vendedor);
  }

  public render() {
    const { t } = this.props;
    var dataset: any = [];
    var ticks: any = [];
    var registrosDetalle: any = [];
    var total = 0;
    var contador = 1;
    var colores: any = [];
    /* const data = this.props.registrosEmbarques.map((item: any) => {
      var color = "";
      if (contador == 1) color = "#D3422E";
      else if (contador == 2) color = "#E3AE3D";
      else if (contador == 3) color = "#55B755";
      else if (contador == 4) color = "#24D1D1";
      else if (contador == 5) color = "#556BC6";
      else color = "#9756C1";
      total =
        total +
        (this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3);
      var t =
        this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3;
      dataset.push([this.meses_txt(item.ValorTxt), t, color]);
      registrosDetalle.push({Grupo : this.meses_txt(item.ValorTxt),Valor : numeral(t).format("0,000")});
      ticks.push(t);
      contador++;
    });*/
    const data = this.props.registrosEmbarques.map((item: any) => {
	
            let date = new Date();
            let mesactual = date.getFullYear() +"-" +(date.getMonth() + 1).toString().padStart(2, "0");

			console.log(item.ValorTxt+" -- " + mesactual);
		var color = "#5D6770";	
			if (item.ValorTxt == mesactual )
			color = "#386DC5";	
			
      var t =
        this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3;
      registrosDetalle.push({
        Grupo: this.meses_txt(item.ValorTxt),
        Valor: numeral(t).format("0,000"),
      });
      total =
        total +
        (this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3);
      colores.push(color);
      contador++;
      return this.state.unidadPeso == "KG"
        ? item.Valor
        : this.state.unidadPeso == "LB"
        ? item.Valor2
        : item.Valor3;
    });
    const labels = this.props.registrosEmbarques.map((item: any) => {
      return this.meses_txt(item.ValorTxt);
    });
    const props = this.props;
    return this.props.isloadingEmbarques ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="containerpage">
        <div
          style={{ display: "flex", alignItems: "center", margin: 0 }}
          className="row"
        >
            <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-7 col-md-7 col-sm-12 col-12">
            <div className="row">
              {this.props.registrosEmbarques.length > 0 ? (
                <GraficaGral
                  label={t('title_embarques')}
                  labels={labels}
                  data={data}
                  backgroundColor={colores}
                  onclick={(items: any) => {
                    if (items[0]) {
                      var index = items[0]._index;
                      const embar = props.registrosEmbarques[index];
                      props.history.push({
                        pathname: `/EmbarquesDetalle`,
                        state: {
                          grupo: embar ? embar.ValorTxt : "",
                          vendedor: this.props.selectedVendedor,
                          cliente: this.props.selectedCliente,
                          catalogoClientes: this.props.clientes,
                        },
                      });
                    }
                  }}
                />
              ) : null}
            </div>
          </div>
          <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-3 col-md-3 col-sm-12 col-12">
            <div className="row">
              <div className="col"></div>
              <div className="col">
                <div style={{ display: "flex" }}>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("1", "2"),
                      });
                    }}
                    onClick={() => {
                      this.props.requestDowloadExcelEmbarques(
                        this.props.selectedCliente,
                        this.props.selectedVendedor
                      );
                    }}
                    className="circulo-iconos boton-inactivo"
                  >
                    <img
                      src={`../../images/icons/${this.state.imgExcel}`}
                      style={{ width: "33px", margin: "auto" }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgKG;
                      this.setState({
                        imgKG: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgKG:
                          this.state.btnKG == "boton-activo"
                            ? "i-kg-1.png"
                            : "i-kg-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "KG",
                        btnKG: "boton-activo",
                        btnLB: "boton-inactivo",
                        imgKG: "i-kg-1.png",
                        imgLB: "i-lb-2.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnKG}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgKG}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgLB;
                      this.setState({
                        imgLB: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgLB:
                          this.state.btnLB == "boton-activo"
                            ? "i-lb-1.png"
                            : "i-lb-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "LB",
                        btnKG: "boton-inactivo",
                        btnLB: "boton-activo",
                        imgKG: "i-kg-2.png",
                        imgLB: "i-lb-1.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnLB}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgLB}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col"></div>
            </div>
            <div className="row">
              <DetallesGraficaEmbarques
                labelTotal={"Total " +this.state.unidadPeso }
                total = {numeral(total).format("0,0")}
                registros={registrosDetalle}
                onClickTotal={() => {
                  this.props.history.push({
                    pathname: `/EmbarquesDetalle`,
                    state: {
                      vendedor: this.props.selectedVendedor,
                      cliente: this.props.selectedCliente,
                      grupo: "",
                      catalogoClientes: this.props.clientes,
                    },
                  });
                }}
                onClickDetalle={(item: any) => {
                  var grupo = "";
                  _.map(this.props.registrosEmbarques, (x: any) => {
                    if (this.meses_txt(x.ValorTxt) == item) {
                      grupo = x.ValorTxt;
                    }
                  });
                  this.props.history.push({
                    pathname: `/EmbarquesDetalle`,
                    state: {
                      vendedor: this.props.selectedVendedor,
                      cliente: this.props.selectedCliente,
                      catalogoClientes: this.props.clientes,
                      grupo: grupo,
                    },
                  });
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.EmbarquesStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...EmbarquesStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('embarques')(Embarques as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
