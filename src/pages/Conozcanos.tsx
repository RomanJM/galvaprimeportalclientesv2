﻿import * as React from 'react';
import {  bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as NavbarTopStore from '../store/NavbarTopStore';
import { translate } from 'react-i18next';
type ConozcanosProps =
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;
class Conozcanos extends React.Component<ConozcanosProps, {}>{
    componentDidMount(){
        this.props.setVista("Conozcanos", false);
    }
    public render() {
        const { t } = this.props;
        return <div  >
            <div >
                <div className="acercadepage" style = {{overflowY:'scroll', overflowX:'hidden'}} >
                    <div className="row" style={{width:'100%'}}>
                            <h6 className="section-main-title"><strong> {t('lbl_empresa')}</strong></h6>
                            <p style={{ textAlign: "justify", fontSize: 17 }}><strong>{t('common:lbl_empresa_min')}</strong> {t('msg_empresa_1')}</p> <br/>
                            <p style={{ textAlign: "justify", fontSize: 17 }}>{t('msg_empresa_2')} <strong>{t('common:lbl_empresa_min')}</strong> {t('msg_empresa_3')}</p><br />
                            <p style={{ textAlign: "justify", fontSize: 17 }}>{t('msg_empresa_4')} <strong> {t('msg_empresa_5')} </strong> {t('msg_empresa_6')}</p><br />
                    </div>

                    <div style={{ marginTop: 15 }} >
                        <div style={{ marginTop: 10 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>{t('lbl_mision')}</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}>{t('msg_mision_1')}</p> <br />
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>{t('lbl_vision')}</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}></p> {t('msg_vision_1')} <br />
                            </div>
                        </div>
                        <div style={{ marginTop: 10 }} className="row">
                          
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>{t('lbl_valores')}</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}>
                                    {t('msg_valores_1')} <br />
                                    {t('msg_valores_2')}   <br />
                                    {t('msg_valores_3')}   <br />
                                    {t('msg_valores_4')}   <br />
                                    {t('msg_valores_5')}   <br />
                                    {t('msg_valores_6')}  <br />
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>;
    }
}
const mapStateToProps = (state: ApplicationState) => {
    return {
        ...state.NavbarTopStore
    }
  }
  const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators( {
      ...NavbarTopStore.actionCreators
    }, dispatch)
  }
  const withTranslate = translate('conozcanos')(Conozcanos as any);
  export default connect(mapStateToProps,mapDispatchToProps)(withTranslate);