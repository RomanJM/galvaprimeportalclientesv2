﻿import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as InventariosStore from "../store/InventariosStore";
import * as LogInStore from "../store/LogInStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import { AnyAction, bindActionCreators } from "redux";
import GraficaGral from "../common/GraficaGeneral";
import DetallesGrafica from "../common/DetallesGrafica";
import DetallesGrafica2 from "../common/DetallesGrafica2";
import { translate } from 'react-i18next'
//import 'react-select/dist/react-select.css';
/**
 * Components
 * */
import Loading from "../common/Loading";

//declare const $;
type InventariosProps = InventariosStore.InventariosStoreState &
  typeof InventariosStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators &
  LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;
interface InventariosState {
  cliente: any;
  vendedor: any;
  nombreCliente: string;
  open: boolean;
  unidadPeso: string;
  esAlmacen: boolean;
  textoDetalle: string;
  imgExcel: string;
  imgLB: string;
  imgKG: string;
  btnKG: string;
  btnLB: string;
  imgresumenAlmacen: string;
  imgTipoManterial: string;
}

class Inventarios extends React.Component<InventariosProps, InventariosState> {
  constructor(props: any) {
    super(props);
    const { t } = props;
    this.state = {
      cliente: "",
      vendedor: "",
      nombreCliente: "",
      open: true,
      unidadPeso: "KG",
      esAlmacen: true,
      textoDetalle: t('lbl_almacen'),
      imgExcel: "i-excel-2.png",
      imgLB: "i-lb-2.png",
      imgKG: "i-kg-1.png",
      btnKG: "boton-activo",
      btnLB: "boton-inactivo",
      imgresumenAlmacen: "i-material-1.png",
      imgTipoManterial: "i-material-2.png",
    };
  }
  public filtrar = (esAlmacen: boolean) => {
    if (esAlmacen)
      this.props.requestInformacionInicialInventarios(
        this.props.selectedCliente,
        this.props.selectedVendedor
      );
    else
      this.props.requestInformacionInicialMercado(
        this.props.selectedCliente,
        this.props.selectedVendedor
      );
  };
  componentDidMount() {
    var cliente: any =
      this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
    var vendedor: any =
      this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
    this.props.setVista("Inventarios", true);
    this.props.setClienteVendedor(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
    this.props.requestInformacionInicialInventarios(
      this.props.selectedCliente ? this.props.selectedCliente : cliente,
      this.props.selectedVendedor ? this.props.selectedVendedor : vendedor
    );
  }

  public render() {
    const { t } = this.props;
    var dataset: any = [];
    var colores: any = [];
    var detallesRegistros: any = [];
    var total = 0;
    const data = this.props.registrosInventarios.map((item: any) => {
      total =
        total +
        (this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3);
      return this.state.unidadPeso == "KG"
        ? item.Valor
        : this.state.unidadPeso == "LB"
        ? item.Valor2
        : item.Valor3;
    });

   

    const labels = this.props.registrosInventarios.map((item: any) => {
      //return this.state.esAlmacen ? item.ValorTxt : item.Grupo;
      return t(item.ValorTxt);
    });
    this.props.registrosInventarios.map((item: any) => {
      var color = "";
      if (item.Grupo == "PE") color = "#4A6CC5";
      else if (item.Grupo == "PT") color = "#00B95D";
      else if (item.Grupo == "AL") color = "#4A6CC5";
      else if (item.Grupo == "AZ") color = "#00D4D4";
      else if (item.Grupo == "PO") color = "#F0AC38";
      else if (item.Grupo == "CR") color = "#ADADAD";
      else if (item.Grupo == "GZ") color = "#E93314";
      else if (item.Grupo == "GN") color = "#D4B01A";
      else if (item.Grupo == "IX") color = "#666666";
      else color = "#00B95D";
      /*total =
        total +
        (this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3);*/
      dataset.push([
        item.ValorTxt,
        this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3,
        color,
      ]);
      var valor =
        this.state.unidadPeso == "KG"
          ? item.Valor
          : this.state.unidadPeso == "LB"
          ? item.Valor2
          : item.Valor3;
      detallesRegistros.push({
        Grupo: t(item.ValorTxt),
        Valor: numeral(valor).format("0,000"),
      });
      colores.push(color);
    });
    const props = this.props;
    return this.props.isloadingInventarios ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="containerpage">
        <div
          style={{ display: "flex", alignItems: "center", margin: 0 }}
          className="row"
        >
        <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div  className="col col-lg-7 col-md-7 col-sm-12 col-12">
            <div  className="row">
              {this.props.registrosInventarios.length > 0 ? (
                <GraficaGral
                  label={
                    this.state.esAlmacen
                      ? t('title_inventario_pro_ter')
                      : t('title_inventario_mat')
                  }
                  labels={labels}
                  data={data}
                  backgroundColor={colores}
                  onclick={(items: any) => {
                    if (items[0]) {
                      var index = items[0]._index;
                      const merca = props.registrosInventarios[index];
                      var cat = props.clientes;
                      props.history.push({
                        pathname: `/InventariosDetalle`,
                        state: {
                          mercado: merca ? merca.Grupo : "",
                          vendedor: props.selectedVendedor,
                          cliente: props.selectedCliente,
                          catalogoClientes: props.clientes,
                          textoDetalle: this.state.textoDetalle,
                        },
                      });
                    }
                  }}
                />
              ) : null}
            </div>
          </div>
          <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
          <div className="col col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div className="row">
              <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
              <div className="col col-lg-10 col-md-10 col-sm-12 col-12">
                <div style={{ display: "flex", lineHeight: "50%" }}>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      var imagen = this.state.imgExcel;
                      this.setState({
                        imgExcel: String(imagen).replace("1", "2"),
                      });
                    }}
                    onClick={() => {
                      this.props.requestDowloadExcelInventarios(
                        this.props.selectedCliente,
                        this.props.selectedVendedor
                      );
                    }}
                    className="circulo-iconos boton-inactivo"
                  >
                    <img
                      src={`../../images/icons/${this.state.imgExcel}`}
                      style={{ width: "33px", margin: "auto" }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgKG;
                      this.setState({
                        imgKG: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgKG:
                          this.state.btnKG == "boton-activo"
                            ? "i-kg-1.png"
                            : "i-kg-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "KG",
                        btnKG: "boton-activo",
                        btnLB: "boton-inactivo",
                        imgKG: "i-kg-1.png",
                        imgLB: "i-lb-2.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnKG}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgKG}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                  <div
                    onMouseOver={() => {
                      var imagen = this.state.imgLB;
                      this.setState({
                        imgLB: String(imagen).replace("2", "1"),
                      });
                    }}
                    onMouseOut={() => {
                      this.setState({
                        imgLB:
                          this.state.btnLB == "boton-activo"
                            ? "i-lb-1.png"
                            : "i-lb-2.png",
                      });
                    }}
                    onClick={() => {
                      this.setState({
                        unidadPeso: "LB",
                        btnKG: "boton-inactivo",
                        btnLB: "boton-activo",
                        imgKG: "i-kg-2.png",
                        imgLB: "i-lb-1.png",
                      });
                    }}
                    className={`circulo-iconos ${this.state.btnLB}`}
                  >
                    <img
                      src={`../../images/icons/${this.state.imgLB}`}
                      style={{
                        marginLeft: "4%",
                        marginTop: "12%",
                        width: "33px",
                      }}
                    />
                  </div>
                  {this.state.esAlmacen ? (
                    <div
                      onClick={() => {
                        this.props.setAlmacen(false);
                        this.setState({
                          esAlmacen: false,
                          textoDetalle: t('lbl_tipo_marial'),
                        });
                        this.filtrar(false);
                        //this.filtrar(false, this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ? this.state.vendedor.value : "");
                      }}
                      className={`circulo-iconos boton-activo`}
                    >
                      <img
                        src={`../../images/icons/${this.state.imgresumenAlmacen}`}
                        style={{ width: "33px", margin: "auto" }}
                      />
                    </div>
                  ) : null}
                  {!this.state.esAlmacen ? (
                    <div
                      onClick={() => {
                        this.props.setAlmacen(true);
                        this.setState({
                          esAlmacen: true,
                          textoDetalle: t('lbl_almacen'),
                        });
                        this.filtrar(true);
                      }}
                      className={`circulo-iconos boton-activo`}
                    >
                      <img
                        src={`../../images/icons/${this.state.imgTipoManterial}`}
                        style={{ width: "33px", margin: "auto" }}
                      />
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="col col-lg-1 col-md-1 col-sm-12 col-12"></div>
            </div>
            <div className="row">
              {this.state.esAlmacen ? 
               <DetallesGrafica2
               labelTotal={
                 "Total " +
                 this.state.unidadPeso 
               }
               total = {numeral(total).format("0,0")}
               registros={detallesRegistros}
               onClickTotal={() => {
                 this.props.history.push({
                   pathname: `/InventariosDetalle`,
                   state: {
                     mercado: "",
                     vendedor: this.props.selectedVendedor,
                     cliente: this.props.selectedCliente,
                     catalogoClientes: this.props.clientes,
                     textoDetalle: this.state.textoDetalle,
                   },
                 });
               }}
               onClickDetalle={(item: any) => {
                 const merca = _.find(
                   this.props.registrosInventarios,
                   (x: any) => {
                     return x.ValorTxt == item;
                   }
                 );

                 this.props.history.push({
                   pathname: `/InventariosDetalle`,
                   state: {
                     vendedor: this.props.selectedVendedor,
                     cliente: this.props.selectedCliente,
                     mercado: merca ? merca.Grupo : "",
                     unidadPeso: this.state.unidadPeso,
                     catalogoClientes: this.props.clientes,
                     textoDetalle: this.state.textoDetalle,
                   },
                 });
               }}
             /> : 
             <DetallesGrafica
             labelTotal={
               "Total " +
               this.state.unidadPeso 
             }
             total = {numeral(total).format("0,0")}
             registros={detallesRegistros}
             onClickTotal={() => {
               this.props.history.push({
                 pathname: `/InventariosDetalle`,
                 state: {
                   mercado: "",
                   vendedor: this.props.selectedVendedor,
                   cliente: this.props.selectedCliente,
                   catalogoClientes: this.props.clientes,
                   textoDetalle: this.state.textoDetalle,
                 },
               });
             }}
             onClickDetalle={(item: any) => {
               const merca = _.find(
                 this.props.registrosInventarios,
                 (x: any) => {
                   return x.ValorTxt == item;
                 }
               );

               this.props.history.push({
                 pathname: `/InventariosDetalle`,
                 state: {
                   vendedor: this.props.selectedVendedor,
                   cliente: this.props.selectedCliente,
                   mercado: merca ? merca.Grupo : "",
                   unidadPeso: this.state.unidadPeso,
                   catalogoClientes: this.props.clientes,
                   textoDetalle: this.state.textoDetalle,
                 },
               });
             }}
           />
            }
             
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.InventariosStore,
    ...state.LogInStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...InventariosStore.actionCreators,
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('inventarios')(Inventarios as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
