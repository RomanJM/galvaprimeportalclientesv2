import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store/index';
import { Button, Card, Row, Col, Form } from 'react-bootstrap';
import Select from 'react-select'

import * as RegistroEntradaCamionesStore from '../store/RegistroEntradaCamionesStore';
type RegistroEntradaCamionesProps = RegistroEntradaCamionesStore.RegistroEntradaCamionesStoreState & typeof RegistroEntradaCamionesStore.actionCreators & RouteComponentProps<{}>;

 class RegistroEntradaCamiones extends React.Component<RegistroEntradaCamionesProps,any>{
    public render(){
        const options = [
            { value: '1', label: 'Opcion 1' },
            { value: '2', label: 'Opcion 2' },
            { value: '3', label: 'Opcion 3' }
          ]
        return <div>
            <Card >
            <Card.Header>
                Registro entrada camiones
                <div style={{width:'50%', float:'right', textAlign:'right'}}>
                    <Button variant="success" style={{margin:'0 1em'}}>Guardar</Button>
                    <Button variant="danger">Cancelar</Button> 
                </div>
            </Card.Header>
            <Card.Body>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Linea transportsta</Form.Label>
                            <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                            />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Nombre chofer</Form.Label>
                            <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Placas tractor</Form.Label>
                            <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                            />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Placas plataforma</Form.Label>
                            <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Identificación</Form.Label>
                            <Form.Control  />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Trans padrón galvaprime</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Operador equipo protección</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Eq suj y proteccion garga</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Apariencia del camion</Form.Label>
                            <Form.Control />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Piso plataforma, buenas condiciones</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Tiene GPS</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Cortinas buen estado</Form.Label>
                            <Form.Check aria-label="option 1" />
                        </Form.Group>
                    </Col>
                </Row>
                <div style={{margin:'2em 0', borderBottom:'1px solid #555454'}}></div>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Comentarios</Form.Label>
                            <Form.Control  />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Tipo</Form.Label>
                            <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Placas platafforma 2</Form.Label>
                            <Form.Control  />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Numero Celular</Form.Label>
                            <Form.Control  />
                        </Form.Group>
                    </Col>
                </Row>
            </Card.Body>
            </Card>
        </div>
    }
}

export default connect(
    (state: ApplicationState) => state.RegistroEntradaCamionesStore, RegistroEntradaCamionesStore.actionCreators
)(RegistroEntradaCamiones as any);