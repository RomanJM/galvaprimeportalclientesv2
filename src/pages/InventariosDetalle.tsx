﻿import * as React from "react";
import * as _ from "lodash";
import Tabla from "../common/Tabla";
import numeral from "numeral";
import { Button } from "@material-ui/core";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as InventarioDetallesStore from "../store/InventarioDetallesStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Loading from "../common/Loading";
import { translate } from 'react-i18next'


type InventariosDetalleProps =
  InventarioDetallesStore.InventarioDetallesStoreState &
    typeof InventarioDetallesStore.actionCreators &
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;

interface InventariosDetalleState {
  mercado: string;
  cliente: string;
  vendedor: any;
  catalogoClientes: any;
  textoDetalle: any;
  unidadPeso: any;
  imgExcel: string;
  imgLB: string;
  imgKG: string;
  imgPZ: string;
  btnKG: string;
  btnLB: string;
  btnPZ: string;
}
class InventariosDetalle extends React.Component<
  InventariosDetalleProps,
  InventariosDetalleState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      mercado: "",
      cliente: "",
      vendedor: "",
      catalogoClientes: [],
      textoDetalle: "",
      unidadPeso: "KG",
      imgExcel: "i-excel-2.png",
      imgLB: "i-lb-2.png",
      imgKG: "i-kg-1.png",
      imgPZ: "pieza-1.png",
      btnKG: "boton-activo",
      btnLB: "boton-inactivo",
      btnPZ: "boton-inactivo",
    };
  }
  componentDidMount() {
    //var title: any = document.getElementById('title-page');
    //title.innerHTML = "Inventario de producto - Detalles";
    this.props.setVista("InventariosDetalle", false);
    const state = (this.props.location as any).state;
    this.setState({
      mercado: state.mercado,
      cliente: state.cliente,
      vendedor: state.vendedor,
      catalogoClientes: state.catalogoClientes,
      textoDetalle: state.textoDetalle,
    });
    this.props.requestInformacionInicialInventarioDeta(
      state.mercado,
      state.cliente,
      state.vendedor
    );
  }
  componentWillReceiveProps(props: any) {}
  public render() {
    const { t } = this.props;
    const columns = [
      {
        Header: t('tbl_num_cliente'),
        accessor: "Numpartecliente",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },

      {
        Header: t('tbl_codigo'),
        accessor: "ArticuloSAP",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_forma'),
        accessor: "Forma",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_tipo_material'),
        accessor: "TipodeMaterial",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_grado'),
        accessor: "Grado",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_calibre'),
        accessor: "Calibre",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_espesor'),
        accessor: "Espesor",
        Cell: (row: any) => {
          var valor = row.original.Espesor;
          //return numeral(valor).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0.00")}
            </div>
          );		  		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_ancho'),
        accessor: "Ancho",
        Cell: (row: any) => {
          var valor = row.original.Ancho;
          //return numeral(valor).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0.00")}
            </div>
          );		  
		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_largo'),
        accessor: "Largo",
        Cell: (row: any) => {
          var valor = row.original.Largo;
          //return numeral(valor).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_prod_ter'),
        accessor: "ProdTerm",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original.ProdTerm
              : this.state.unidadPeso == "LB"
              ? row.original.ProdTerm * 2.20462
              : row.original._ProdTerm_Pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_por_empaque'),
        accessor: "PorEmpaque",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original.PorEmpaque
              : this.state.unidadPeso == "LB"
              ? row.original.PorEmpaque * 2.20462
              : row.original._PorEmpaque_Pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_total_gral'),
        accessor: "TotalGeneral",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original.TotalGeneral
              : this.state.unidadPeso == "LB"
              ? row.original.TotalGeneral * 2.20462
              : row.original._TotalGeneral_Pz;
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
    ];
    if (!this.state.cliente) {
      columns.unshift({
        Header: t('tbl_cliente'),
        accessor: "Cliente",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      });
    }
    var cliente = _.find(this.state.catalogoClientes, (x: any) => {
      return x.clave == this.state.cliente;
    });
    console.log(this.state);
    return this.props.isloadingInventariosDetalle ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="detallepage">
        <div className="row">
          <div className="col col-lg-8 col-md-8 col-sm-12 col-12">
            <div className="row" style={{ paddingTop: "10px" }}>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div style={{ display: "block", float: "left" }}>
                  {" "}
                  <span
                    style={{
                      padding: 3,
                      borderRadius: 5,
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                    }}
                    className="label label-table label-secondary"
                  >
                    {this.state.textoDetalle}
                  </span>{" "}
                  <span
                    style={{
                      padding: 3,
                      borderRadius: 5,
                      backgroundColor: "#555454",
                      color: "#fff",
                    }}
                    className="label label-table label-secondary"
                  >
                    {this.state.mercado ? this.state.mercado : t('lbl_todos')}
                  </span>
                </div>
              </div>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div style={{ display: "block", float: "right" }}>
                  {" "}
                  <span
                    style={{
                      padding: 3,
                      borderRadius: 5,
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_cliente')}
                  </span>{" "}
                  <span
                    style={{
                      padding: 3,
                      borderRadius: 5,
                      backgroundColor: "#555454",
                      color: "#fff",
                    }}
                    className="label label-table label-secondary"
                  >
                    {cliente
                      ? cliente.clave + " - " + cliente.descripcion
                      : t('lbl_todos')}
                  </span>{" "}
                </div>
              </div>
            </div>
          </div>
          <div className="col col-lg-4 col-md-4 col-sm-12 col-12 ">
            <div style={{ display: "flex", float: "right" }}>
              <div
                onClick={() => {
                  window.history.go(-1);
                }}
                className="circulo-iconos boton-inactivo"
              >
                <img
                  src={`../../images/icons/i-return.png`}
                  style={{ width: "30px", margin: "auto" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgExcel;
                  this.setState({
                    imgExcel: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  var imagen = this.state.imgExcel;
                  this.setState({
                    imgExcel: String(imagen).replace("1", "2"),
                  });
                }}
                onClick={() => {
                  this.props.requestDowloadExcelInventarioDetalle(
                    this.state.mercado,
                    this.state.cliente,
                    this.state.vendedor
                  );
                }}
                className="circulo-iconos boton-inactivo"
              >
                <img
                  src={`../../images/icons/${this.state.imgExcel}`}
                  style={{ width: "33px", margin: "auto" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgLB;
                  this.setState({
                    imgLB: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgLB:
                      this.state.btnLB == "boton-activo"
                        ? "i-lb-1.png"
                        : "i-lb-2.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "LB",
                    btnPZ: "boton-inactivo",
                    btnKG: "boton-inactivo",
                    btnLB: "boton-activo",
                    imgPZ: "pieza-1.png",
                    imgKG: "i-kg-2.png",
                    imgLB: "i-lb-1.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnLB}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgLB}`}
                  style={{ marginLeft: "4%", marginTop: "12%", width: "33px" }}
                />
              </div>

              <div
                onMouseOver={() => {
                  var imagen = this.state.imgKG;
                  this.setState({
                    imgKG: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgKG:
                      this.state.btnKG == "boton-activo"
                        ? "i-kg-1.png"
                        : "i-kg-2.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "KG",
                    btnPZ: "boton-inactivo",
                    btnKG: "boton-activo",
                    btnLB: "boton-inactivo",
                    imgPZ: "pieza-1.png",
                    imgKG: "i-kg-1.png",
                    imgLB: "i-lb-2.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnKG}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgKG}`}
                  style={{ marginLeft: "4%", marginTop: "12%", width: "33px" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgPZ;
                  this.setState({
                    imgPZ: String(imagen).replace("1", "2"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgPZ:
                      this.state.btnPZ == "boton-activo"
                        ? "pieza-2.png"
                        : "pieza-1.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "PZ",
                    btnPZ: "boton-activo",
                    btnKG: "boton-inactivo",
                    btnLB: "boton-inactivo",
                    imgPZ: "pieza-2.png",
                    imgKG: "i-kg-2.png",
                    imgLB: "i-lb-2.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnPZ}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgPZ}`}
                  style={{ width: "33px", margin: "auto" }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-xs-12">
            {
              <Tabla
                registros={this.props.registrosInventariosDetalle}
                columnas={columns}
                filterable={true}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.InventarioDetallesStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...InventarioDetallesStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('inventarios')(InventariosDetalle as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
