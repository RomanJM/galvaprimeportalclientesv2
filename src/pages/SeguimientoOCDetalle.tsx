﻿import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import Tabla from "../common/Tabla";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as SeguimientoOCDetalleStore from "../store/SeguimientoOCDetalleStore";
import * as NavbarTopStore from "../store/NavbarTopStore";
import Loading from "../common/Loading";
import { translate } from 'react-i18next'

type SeguimientoOCDetalleProps =
  SeguimientoOCDetalleStore.SeguimientoOCDetalleStoreState &
    typeof SeguimientoOCDetalleStore.actionCreators &
    NavbarTopStore.NavbarTopState &
    typeof NavbarTopStore.actionCreators & {t?: any} &
    RouteComponentProps<{}>;

interface SeguimientoOCDetalleState {
  vencimiento: string;
  cliente: string;
  vendedor: any;
  catalogoClientes?: any;
  unidadPeso: string;
  imgExcel: string;
}
class SeguimientoOCDetalle extends React.Component<
  SeguimientoOCDetalleProps,
  SeguimientoOCDetalleState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      vencimiento: "",
      cliente: "",
      vendedor: "",
      catalogoClientes: [],
      unidadPeso: "",
      imgExcel: "i-excel-2.png",
    };
  }

  componentDidMount() {
    //var title: any = document.getElementById('title-page');
    //title.innerHTML = "Seguimiento a órdenes de compras abiertas - Detalles";
    this.props.setVista("SeguimientoOCDetalle", false);
    const state = (this.props.location as any).state;
    this.setState({
      vencimiento: state.vencimiento,
      cliente: state.cliente,
      vendedor: state.vendedor,
      catalogoClientes: state.catalogoClientes,
      unidadPeso: state.unidadPeso,
    });
    this.props.requestInformacionInicialSocDetalle(
      state.vencimiento,
      state.cliente,
      state.vendedor
    );
  }

  public render() {
    const { t } = this.props;
    var cliente = _.find(this.state.catalogoClientes, (x: any) => {
      return x.clave == this.state.cliente;
    });
    const columns = [
      {
        Header: t('tbl_estatus'),
        accessor: "Vencimiento",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_oc'),
        accessor: "Reference",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_num_parte'),
        accessor: "CustomerSKU",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_codigo'),
        accessor: "ItemCode",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_tipo_mat'),
        accessor: "MaterialType",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_grado'),
        accessor: "Grado",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_calibre'),
        accessor: "calibre",
        Cell: (row: any) => {
          //return row.value;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_espesor'),
        accessor: "espesor",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );				  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_ancho'),
        accessor: "ancho",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_largo'),
        accessor: "largo",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_um'),
        accessor: "UM",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_solicitado'),
        accessor: "",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.OrderQty
              : row.original.UM == "LB"
              ? row.original.OrderQty * 2.20462
              : row.original.OrderQtyUM;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_embarcado'),
        accessor: "",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.DeliveredQty
              : row.original.UM == "LB"
              ? row.original.DeliveredQty * 2.20462
              : row.original.DeliveredQtyUM;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_saldo'),
        accessor: "",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.OpenQty
              : row.original.UM == "LB"
              ? row.original.OpenQty * 2.20462
              : row.original.OpenQtyUM;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_producto_terminado'),
        accessor: "PTstockAsigned",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.PTstockAsigned
              : row.original.UM == "LB"
              ? row.original.PTstockAsigned * 2.20462
              : row.original._PTstockAsignedUM;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
          //return numeral(row.value).format('0,0');
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_por_empaque'),
        accessor: "PEstockAsigned",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.PEstockAsigned
              : row.original.UM == "LB"
              ? row.original.PEstockAsigned * 2.20462
              : row.original._PEstockAsignedUM;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
          //return numeral(row.value).format('0,0');
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_en_programa'),
        accessor: "Progasig",
        Cell: (row: any) => {
          var valor =
            row.original.UM == "KG"
              ? row.original.Progasig
              : row.original.UM == "LB"
              ? row.original.Progasig * 2.20462
              : row.original._ProgasigUM;
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
          //return numeral(row.value).format('0,0');
        },
        headerStyle: { fontWeight: "bold" },
      },
    ];
    if (!this.state.cliente) {
      columns.unshift({
        Header: t('tbl_cliente'),
        accessor: "alias",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      });
    }
    return this.props.isloadingSocDetalle ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="detallepage">
        <div className="row">
          <div className="col col-lg-10 col-md-10 col-sm-12 col-12">
            <div className="row" style={{ paddingTop: "10px" }}>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div style={{ display: "block", float: "left" }}>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_estatus')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {this.state.vencimiento ? t(this.state.vencimiento) : t('lbl_todos')}
                  </span>
                </div>
              </div>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div style={{ display: "block", float: "right" }}>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_cliente')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {cliente
                      ? cliente.clave + " - " + cliente.descripcion
                      : t('lbl_todos')}
                  </span>{" "}
                </div>
              </div>
            </div>
          </div>
          <div className="col col-lg-2 col-md-2 col-sm-12 col-12 ">
            <div style={{ display: "flex", float: "right" }}>
              <div>
                <div
                  onMouseOver={() => {
                    var imagen = this.state.imgExcel;
                    this.setState({
                      imgExcel: String(imagen).replace("2", "1"),
                    });
                  }}
                  onMouseOut={() => {
                    var imagen = this.state.imgExcel;
                    this.setState({
                      imgExcel: String(imagen).replace("1", "2"),
                    });
                  }}
                  onClick={() => {
                    this.props.requestDowloadExcelSocDetalle(
                      this.state.vencimiento,
                      this.state.cliente,
                      this.state.vendedor,
                      t
                    );
                  }}
                  className="circulo-iconos boton-inactivo"
                >
                  <img
                    src={`../../images/icons/${this.state.imgExcel}`}
                    style={{ width: "33px", margin: "auto" }}
                  />
                </div>
              </div>
              <div>
                <div
                  onClick={() => {
                    window.history.go(-1);
                  }}
                  className="circulo-iconos boton-inactivo"
                >
                  <img
                    src={`../../images/icons/i-return.png`}
                    style={{ width: "30px", margin: "auto" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-12">
            {
              <Tabla
                registros={this.props.registrosSocDetalle}
                columnas={columns}
                filterable={true}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.SeguimientoOCDetalleStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...SeguimientoOCDetalleStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('seguimientooc')(SeguimientoOCDetalle as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
