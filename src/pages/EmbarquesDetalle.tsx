﻿import * as React from "react";
import numeral from "numeral";
import * as _ from "lodash";
import Tabla from "../common/Tabla";
import { bindActionCreators } from "redux";
import { Button } from "@material-ui/core";
import { connect } from "react-redux";
import { translate } from 'react-i18next'
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as EmbarqueDetallesStore from "../store/EmbarqueDetallesStore";
import * as NavbarTopStore from "../store/NavbarTopStore";

import Loading from "../common/Loading";

type EmbarquesDetalleProps = EmbarqueDetallesStore.EmbarqueDetallesStoreState &
  typeof EmbarqueDetallesStore.actionCreators &
  NavbarTopStore.NavbarTopState &
  typeof NavbarTopStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;

interface EmbarquesDetalleState {
  grupo: string;
  cliente: string;
  vendedor: any;
  catalogoClientes: any;
  unidadPeso: string;
  imgExcel: string;
  imgLB: string;
  imgKG: string;
  imgPZ: string;
  btnKG: string;
  btnLB: string;
  btnPZ: string;
}
class EmbarquesDetalle extends React.Component<
  EmbarquesDetalleProps,
  EmbarquesDetalleState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      grupo: "",
      cliente: "",
      vendedor: "",
      catalogoClientes: [],
      unidadPeso: "KG",
      imgExcel: "i-excel-2.png",
      imgLB: "i-lb-2.png",
      imgKG: "i-kg-1.png",
      imgPZ: "pieza-1.png",
      btnKG: "boton-activo",
      btnLB: "boton-inactivo",
      btnPZ: "boton-inactivo",
    };
  }
  componentDidMount() {
    this.props.setVista("EmbarquesDetalle", false);
    const state = (this.props.location as any).state;
    this.setState({
      grupo: state.grupo,
      cliente: state.cliente,
      vendedor: state.vendedor,
      catalogoClientes: state.catalogoClientes,
    });
    this.props.requestInformacionInicialEmbarqueDeta(
      state.grupo,
      state.cliente,
      state.vendedor
    );
  }

  public meses_txt = (texto: any) => {
    const { t } = this.props;
    const months = [
      t("Ene"),
      t("Feb"),
      t("Mar"),
      t("Abr"),
      t("May"),
      t("Jun"),
      t("Jul"),
      t("Ago"),
      t("Sep"),
      t("Oct"),
      t("Nov"),
      t("Dic"),
    ];
    let arr = texto.split("-");
    return months[Number(arr[1]) - 1] + "-" + arr[0];
  };

  public render() {
    //mes actual
    const { t } = this.props;
    const months = [
      t("Ene"),
      t("Feb"),
      t("Mar"),
      t("Abr"),
      t("May"),
      t("Jun"),
      t("Jul"),
      t("Ago"),
      t("Sep"),
      t("Oct"),
      t("Nov"),
      t("Dic"),
    ];
    let date = new Date();
    let mm = date.getMonth();
    let yy = date.getFullYear();
    let fechas_array = [];
    for (let i = 0; i <= 6; i++) {
      let mmx = mm - i >= 0 ? mm - i : 12 + mm - i;
      let aax = mm - i >= 0 ? yy : yy - 1;
      fechas_array[i] = months[mmx] + "-" + aax;
    }

    const columns = [
      {
        Header: t('tbl_num_parte'),
        accessor: "ClienteNoParte",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_codigo'),
        accessor: "CodigoArticuloSAP",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_forma'),
        accessor: "Forma",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_tipo_mat'),
        accessor: "TipodeMaterial",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_grado'),
        accessor: "Grado",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 150,
      },

      {
        Header: t('tbl_calibre'),
        accessor: "Calibre",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_espesor'),
        accessor: "Espesor",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: t('tbl_ancho'),
        accessor: "Ancho",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
        //minWidth : 200
      },
      {
        Header: t('tbl_largo'),
        accessor: "largo",
        Cell: (row: any) => {
          //return numeral(row.value).format("0,0.00");
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(row.value).format("0,0.00")}
            </div>
          );		  
        },
        headerStyle: { fontWeight: "bold" },
        //minWidth: 200
      },
      {
        Header: fechas_array[6],
        accessor: "_Cantidad_6",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_6 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_6 * 1000 * 2.20462
              : row.original._Cantidad_6_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: fechas_array[5],
        accessor: "_Cantidad_5",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_5 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_5 * 1000 * 2.20462
              : row.original._Cantidad_5_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: fechas_array[4],
        accessor: "_Cantidad_4",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_4 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_4 * 1000 * 2.20462
              : row.original._Cantidad_4_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: fechas_array[3],
        accessor: "_Cantidad_3",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_3 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_3 * 1000 * 2.20462
              : row.original._Cantidad_3_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: fechas_array[2],
        accessor: "_Cantidad_2",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_2 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_2 * 1000 * 2.20462
              : row.original._Cantidad_2_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: fechas_array[1],
        accessor: "_Cantidad_1",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_1 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_1 * 1000 * 2.20462
              : row.original._Cantidad_1_pz;
          return (
            <div style={{ textAlign: "right" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
        //    minWidth: 200
      },
      {
        Header: fechas_array[0],
        accessor: "_Cantidad_0",
        Cell: (row: any) => {
          var valor =
            this.state.unidadPeso == "KG"
              ? row.original._Cantidad_0 * 1000
              : this.state.unidadPeso == "LB"
              ? row.original._Cantidad_0 * 1000 * 2.20462
              : row.original._Cantidad_0_pz;
          return (
            <div style={{ textAlign: "right", paddingRight: "20px" }}>
              {numeral(valor).format("0,0")}
            </div>
          );
        },
        headerStyle: { fontWeight: "bold" },
        //    minWidth: 200
      },
    ];
    var cliente = _.find(this.state.catalogoClientes, (x: any) => {
      return x.clave == this.state.cliente;
    });

    if (!this.state.cliente) {
      columns.unshift({
        Header: t('tbl_cliente'),
        accessor: "NombreCortoSN",
        Cell: (row: any) => {
          return row.value;
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 250,
      });
    }
    return this.props.isloadingEmbarquesDetalle ? (
      <Loading mensage={t('common:msg_procesando')} />
    ) : (
      <div className="detallepage">
        <div className="row">
          <div className="col col-lg-10 col-md-10 col-sm-12 col-12">
            <div className="row" style={{ paddingTop: "10px" }}>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_periodo')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {this.state.grupo
                      ? this.meses_txt(this.state.grupo)
                      : t('lbl_todos')}
                  </span>
                </div>
              </div>
              <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div>
                  {" "}
                  <span
                    style={{
                      backgroundColor: "#0e7ec3",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {t('lbl_cliente')}
                  </span>{" "}
                  <span
                    style={{
                      backgroundColor: "#555454",
                      color: "#fff",
                      padding: 3,
                      borderRadius: 5,
                    }}
                    className="label label-table label-secondary"
                  >
                    {cliente
                      ? cliente.clave + " - " + cliente.descripcion
                      : t('lbl_todos')}
                  </span>{" "}
                </div>
              </div>
            </div>
          </div>
          <div className="col col-lg-2 col-md-2 col-sm-12 col-12 ">
            <div style={{ display: "flex", float: "right" }}>
              <div
                onClick={() => {
                  window.history.go(-1);
                }}
                className="circulo-iconos boton-inactivo"
              >
                <img
                  src={`../../images/icons/i-return.png`}
                  style={{ width: "30px", margin: "auto" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgExcel;
                  this.setState({
                    imgExcel: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  var imagen = this.state.imgExcel;
                  this.setState({
                    imgExcel: String(imagen).replace("1", "2"),
                  });
                }}
                onClick={() => {
                  this.props.requestDowloadExcelEmbarqueDeta(
                    this.state.grupo,
                    this.state.cliente,
                    this.state.vendedor
                  );
                }}
                className="circulo-iconos boton-inactivo"
              >
                <img
                  src={`../../images/icons/${this.state.imgExcel}`}
                  style={{ width: "33px", margin: "auto" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgLB;
                  this.setState({
                    imgLB: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgLB:
                      this.state.btnLB == "boton-activo"
                        ? "i-lb-1.png"
                        : "i-lb-2.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "LB",
                    btnPZ: "boton-inactivo",
                    btnKG: "boton-inactivo",
                    btnLB: "boton-activo",
                    imgPZ: "pieza-1.png",
                    imgKG: "i-kg-2.png",
                    imgLB: "i-lb-1.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnLB}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgLB}`}
                  style={{ marginLeft: "4%", marginTop: "12%", width: "33px" }}
                />
              </div>

              <div
                onMouseOver={() => {
                  var imagen = this.state.imgKG;
                  this.setState({
                    imgKG: String(imagen).replace("2", "1"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgKG:
                      this.state.btnKG == "boton-activo"
                        ? "i-kg-1.png"
                        : "i-kg-2.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "KG",
                    btnPZ: "boton-inactivo",
                    btnKG: "boton-activo",
                    btnLB: "boton-inactivo",
                    imgPZ: "pieza-1.png",
                    imgKG: "i-kg-1.png",
                    imgLB: "i-lb-2.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnKG}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgKG}`}
                  style={{ marginLeft: "4%", marginTop: "12%", width: "33px" }}
                />
              </div>
              <div
                onMouseOver={() => {
                  var imagen = this.state.imgPZ;
                  this.setState({
                    imgPZ: String(imagen).replace("1", "2"),
                  });
                }}
                onMouseOut={() => {
                  this.setState({
                    imgPZ:
                      this.state.btnPZ == "boton-activo"
                        ? "pieza-2.png"
                        : "pieza-1.png",
                  });
                }}
                onClick={() => {
                  this.setState({
                    unidadPeso: "PZ",
                    btnPZ: "boton-activo",
                    btnKG: "boton-inactivo",
                    btnLB: "boton-inactivo",
                    imgPZ: "pieza-2.png",
                    imgKG: "i-kg-2.png",
                    imgLB: "i-lb-2.png",
                  });
                }}
                className={`circulo-iconos ${this.state.btnPZ}`}
              >
                <img
                  src={`../../images/icons/${this.state.imgPZ}`}
                  style={{ width: "33px", margin: "auto" }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-lg-12 col-md-12  col-sm-12 col-12">
            {
              <Tabla
                registros={this.props.registrosEmbarquesDetalle}
                columnas={columns}
                filterable={true}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.EmbarqueDetallesStore,
    ...state.NavbarTopStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...EmbarqueDetallesStore.actionCreators,
      ...NavbarTopStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('embarques')(EmbarquesDetalle as any);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslate);
