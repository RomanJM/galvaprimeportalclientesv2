import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import configureStore from './store/configureStore';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './custom.scss';
import resourcesT from './i18n'
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href') as string;
const history = createBrowserHistory({ basename: baseUrl });
// Get the application-wide store instance, prepopulating with state from the server where available.
const store = configureStore(history);

i18next.init({
  lng: window.localStorage.Idioma ? window.localStorage.Idioma : "es",
  interpolation: { escapeValue: false},
  resources:resourcesT
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
        <ConnectedRouter history={history}>
          <I18nextProvider i18n={i18next}>
            <App />
          </I18nextProvider>
        </ConnectedRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
