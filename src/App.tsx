import React from 'react';
import { RouteComponentProps} from 'react-router';
import { Route, Switch, BrowserRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';
import './estilos.css';
import LogIn from './common/LogIn';
import RecuperaPassword from './common/RecuperaPassword';
import NuevoPassword from './common/NuevoPassword';
import Layout from './common/Layout';
import Home from './pages/Home';
import RegistroEntradaCamiones from './pages/RegistroEntradaCamiones';
import DescargaCamiones from './pages/DescargaCamiones';
import SeguimientoOC from './pages/SeguimientoOC';
import Dashboard from './pages/Dashboard';
import SeguimientoOCDetalle from './pages/SeguimientoOCDetalle';
import Inventarios from './pages/Inventarios';
import InventariosDetalle from './pages/InventariosDetalle';
import Embarques from './pages/Embarques';
import EmbarquesDetalle from './pages/EmbarquesDetalle';
import EstadoCuenta from './pages/EstadoCuenta';
import EstadoCuentaDetalle from './pages/EstadoCuentaDetalle';
import Contactos from './pages/Contactos';
import Conozcanos from './pages/Conozcanos';
import AvisoPrivacidad from './pages/AvisoPrivacidad';
import Terminos from './pages/TerminosCondiciones';
import Facturas from './pages/Facturas';
import Certificados from './pages/Certificados';
export default class App extends React.Component<any, any>{
  public render(){
    //Local
    const rutaServidor = "/";
    //Produccion
    //const rutaServidor = "/transportistas";
    return <BrowserRouter >
           <Switch>
              <Route exact path={`/`} component={LogIn} />
              <Route path={`/recuperapassword`} component={RecuperaPassword} />
              <Route path={`/NuevoPassword`} component={NuevoPassword} />
              <Layout>
                  <Route path={`/Inicio`} component={Dashboard} />
                  <Route path={`/Entrada`} component={RegistroEntradaCamiones} />
                  <Route path={`/DescargarCamion`} component={DescargaCamiones} />
                  <Route path={'/SeguimientoOC'} component={SeguimientoOC}/>
                  <Route path={'/SeguimientoOCDetalle'} component={SeguimientoOCDetalle} />
                  <Route path={'/Inventarios'} component={Inventarios} />
                  <Route path={'/InventariosDetalle'} component={InventariosDetalle} />
                  <Route path={'/Embarques'} component={Embarques} />
                  <Route path={'/EmbarquesDetalle'} component={EmbarquesDetalle} />
                  <Route path={'/EstadoCuenta'} component={EstadoCuenta} />
                  <Route path={'/EstadoCuentaDetalle'} component={EstadoCuentaDetalle} />
                  <Route path='/Contactos' component={Contactos} />
                  <Route path='/AvisoPrivacidad' component={AvisoPrivacidad} />
                  <Route path='/Conozcanos' component={Conozcanos} />
                  <Route path='/TerminosCondiciones' component={Terminos} />
                  <Route path='/Facturas' component={Facturas} />
                  <Route path='/Certificados' component={Certificados} />
              </Layout>
           </Switch>
        </BrowserRouter>
  }
}
