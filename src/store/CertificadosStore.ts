import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface CertificadosStoreState {
    isloadingCertificados: boolean,
    registrosCertificados: any[],
    clientesCertificados: any[],
    vendedoresCertificados: any[],
    clienteCertificados: any
}

export interface RequestInformacionInicialCertificados {
    type: 'REQUEST_INFORMACION_INICIAL_CERTIFICADOS'
}

export interface ReceiveInformacionInicialCertificados {
    type: 'RECEIVE_INFORMACION_INICIAL_CERTIFICADOS',
    registros: any[],
    cliente: any
}
export interface ReceiveInformacionClientesCertificados {
    type: 'RECEIVE_CLIENTES_CERTIFICADOS',
    clientes: any[]
}
export interface ReceiveInformacionVendedoresCertificados {
    type: 'RECEIVE_VENDEDORES_CERTIFICADOS',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicialCertificados | ReceiveInformacionInicialCertificados 
| ReceiveInformacionClientesCertificados | ReceiveInformacionVendedoresCertificados;

export const actionCreators = {
    requestInformacionInicialCertificados: (cliente: any, vendedor: any, fechaInicial: any, fechaFinal: any, factura: any,lote:any, numAtCard:any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var fac = parseInt(factura);

        cliente = cliente && cliente != "0" ? cliente : '';

        let _fecha_hasta_ = new Date(fechaFinal);
        let _fecha_desde_ = new Date(fechaInicial);
        let _total_meses_;
        _total_meses_ = (_fecha_hasta_.getFullYear() - _fecha_desde_.getFullYear()) * 12;
        _total_meses_ -= _fecha_desde_.getMonth();
        _total_meses_ += _fecha_hasta_.getMonth();
        _total_meses_++;
        if (_total_meses_ < 0) {
            alert('La fecha inicial debe ser anterior a la fecha final');
            return;
        }

        if (_total_meses_ > 6 && cliente != '') {
            alert('El rango de fechas para consulta con selección de cliente puede ser hasta 6 meses');
            return;
        } else if (_total_meses_ > 3 && cliente == '') {
            alert('El rango de fechas sin seleccionar un cliente puede ser hasta 3 meses');
            return;
        }
        
        var data_str = {
            CardCode: cliente,
            SlpCode: vendedor ? vendedor : 0,
            Fecha_desde: fechaInicial,
            Fecha_hasta: fechaFinal,
            DocNum: fac ? fac : 0,
            Lote: lote,
            NumAtCard: numAtCard
        };
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_CERTIFICADOS' });
        var respnseA = axios.post(url.CertificadoLista, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log("data.OData",data.OData);
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CERTIFICADOS', registros: data.OData ? data.OData : [], cliente: cliente ? cliente : '' });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CERTIFICADOS', registros: [], cliente: cliente ? cliente : '' });
                console.log(error);
            })
    },
    requestDowloadArchivoReciboCertificados: (recibo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: recibo ? parseInt(recibo) : 0
        };
        var respnseA = axios.post(url.ReciboArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', recibo + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestClientesCertificados: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_CERTIFICADOS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_CERTIFICADOS', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientesCertificados: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_CERTIFICADOS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_CERTIFICADOS', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresCertificados: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_CERTIFICADOS' });
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_CERTIFICADOS', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_CERTIFICADOS', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadArchivoCertificados: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo: tipo
        };
        var respnseA = axios.post(url.CertificadoArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: CertificadosStoreState = {
    isloadingCertificados: false,
    registrosCertificados: [],
    clientesCertificados: [],
    vendedoresCertificados: [],
    clienteCertificados: ""
};

export const reducer: Reducer<CertificadosStoreState> = (state: CertificadosStoreState | undefined, incomingAction: Action): CertificadosStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_CERTIFICADOS':
            return {
                ...state,
                isloadingCertificados: true,
                registrosCertificados: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_CERTIFICADOS':
            return {
                ...state,
                isloadingCertificados: false,
                registrosCertificados: action.registros,
                clienteCertificados: action.cliente
            };
        case 'RECEIVE_CLIENTES_CERTIFICADOS':
            return {
                ...state,
                clientesCertificados: action.clientes
            };
        case 'RECEIVE_VENDEDORES_CERTIFICADOS':
            return {
                ...state,
                isloadingCertificados : false,
                vendedoresCertificados: action.vendedores
            };
        default:
            return state;
    }
};
