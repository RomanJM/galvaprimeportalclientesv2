import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface DashboardStoreState {
    isloadingDashboard: boolean,
    registrosDashboard: any,
    clienteSeleccionado : any,
    vendedorSeleccionado : any
}

export interface RequestInformacionInicialDash {
    type: 'REQUEST_INFORMACION_INICIAL_DASHBOARD'
}

export interface ReceiveInformacionInicialDash {
    type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD',
    registros: any,
    cliente: any,
    vendedor : any
}
export interface ResetSeleccionadosDash {
    type: 'RESET_SELECCIONADOS_DASH'
}

type KnownAction = RequestInformacionInicialDash | ReceiveInformacionInicialDash|ResetSeleccionadosDash;

export const actionCreators = {
    requestInformacionInicialDahboard: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_DASHBOARD' });
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        var clienteSeleccionado = _.find(getState().NavbarTopStore.clientes,(x:any)=>{
            return x.clave == data_str.CardCode;
        });
        var vendedorSeleccionado = _.find(getState().NavbarTopStore.vendedores,(x:any)=>{
            return x.clave == data_str.SlpCode;
        });

        axios.post(url.Dashboard, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
  
                //var registros = _.filter(data.OData, { Vencimiento: vencimiento });        
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD', registros: data.OData ,cliente : clienteSeleccionado, vendedor: vendedorSeleccionado});
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD', registros: [], cliente: null,vendedor : null });
                console.log(error);
            })
    },
    ResetSeleccionados: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RESET_SELECCIONADOS_DASH'});
    }


};

const unloadedState: DashboardStoreState = {
    isloadingDashboard: false,
    registrosDashboard: [],
    clienteSeleccionado : null,
    vendedorSeleccionado : null
};

export const reducer: Reducer<DashboardStoreState> = (state: DashboardStoreState | undefined, incomingAction: Action): DashboardStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloadingDashboard: true,
                registrosDashboard: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloadingDashboard: false,
                registrosDashboard: action.registros,
                clienteSeleccionado : action.cliente,
                vendedorSeleccionado : action.vendedor
            };
            case 'RESET_SELECCIONADOS_DASH':
                return {
                    ...state,
                    clienteSeleccionado : null
                };
        default:
            return state;
    }
};

