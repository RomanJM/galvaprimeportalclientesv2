import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface FacturasStoreState {
    isloadingFacturas: boolean,
    registrosFacturas: any[],
    clientesFacturas: any[],
    vendedoresFacturas: any[],
    clienteFactura :  any
}

export interface RequestInformacionInicialFacturas {
    type: 'REQUEST_INFORMACION_INICIAL_FACTURAS'
}

export interface ReceiveInformacionInicialFacturas {
    type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS',
    registros: any[],
    cliente : any
}
export interface ReceiveInformacionClientesFacturas {
    type: 'RECEIVE_CLIENTES_FACTURAS',
    clientes: any[]
}
export interface ReceiveInformacionVendedoresFacturas {
    type: 'RECEIVE_VENDEDORES_FACTURAS',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicialFacturas | ReceiveInformacionInicialFacturas | ReceiveInformacionClientesFacturas | ReceiveInformacionVendedoresFacturas;

export const actionCreators = {
    requestInformacionInicialFacturas: (cliente: any, 
        vendedor: any, 
        fechaInicial : any , 
        fechaFinal : any, 
        factura : any,
        remision:any,
        numAtCard:any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var fac = parseInt(factura);
        var rem = parseInt(remision);
        cliente = cliente && cliente != "0" ? cliente : '';
        var data_str = {
            CardCode: cliente,
            NumAtCard: numAtCard,
            SlpCode: vendedor ? vendedor : 0,
            _Fecha_desde: fechaInicial,
            _Fecha_hasta: fechaFinal,
            DocNum: fac ? fac : 0,
            DocNumEnt: rem ? rem : 0
        };
        console.log(JSON.stringify(data_str));

        let _fecha_hasta_ = new Date(fechaFinal);
        let _fecha_desde_ = new Date(fechaInicial);
        let _total_meses_;
        _total_meses_ = (_fecha_hasta_.getFullYear() - _fecha_desde_.getFullYear()) * 12;
        _total_meses_ -= _fecha_desde_.getMonth();
        _total_meses_ += _fecha_hasta_.getMonth();

        _total_meses_++;

        
        if (_total_meses_ < 0) {
            alert('La fecha inicial debe ser anterior a la fecha final');
            return;
        }

        if (_total_meses_ > 6 && cliente != '') {
            alert('El rango de fechas para consulta con selección de cliente puede ser hasta 6 meses');
            return;
        }else if (_total_meses_ > 3 && cliente =='') {
            alert('El rango de fechas sin seleccionar un cliente puede ser hasta 3 meses');
            return;
        }

        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_FACTURAS' });
        var respnseA = axios.post(url.FacturaLista, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log("data.OData",data.OData);
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS', registros: data.OData, cliente: cliente ? cliente : '' });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS', registros: [], cliente: cliente ? cliente : ''});
                console.log(error);
            })
    },
    requestDowloadExcelFacturas: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        window.location.href = url.SeguimientoExcel;
    },
    requestClientesFacturas: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientesFacturas: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresFacturas: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_FACTURAS', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_FACTURAS', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadArchivoFacturas: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo : tipo
        };
        var respnseA = axios.post(url.FacturaArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoReciboFactura: (recibo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: recibo ? parseInt(recibo) : 0        
        };
        var respnseA = axios.post(url.ReciboArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', recibo + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoCertificadoFactura: (certificado: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: certificado ? parseInt(certificado) : 0
        };
        var respnseA = axios.post(url.CertificadoArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', certificado + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: FacturasStoreState = {
    isloadingFacturas: false,
    registrosFacturas: [],
    clientesFacturas: [],
    vendedoresFacturas: [],
    clienteFactura : ""
};

export const reducer: Reducer<FacturasStoreState> = (state: FacturasStoreState | undefined, incomingAction: Action): FacturasStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_FACTURAS':
            return {
                ...state,
                isloadingFacturas: true,
                registrosFacturas: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_FACTURAS':
            return {
                ...state,
                isloadingFacturas: false,
                registrosFacturas: action.registros,
                clienteFactura : action.cliente
            };
        case 'RECEIVE_CLIENTES_FACTURAS':
            return {
                ...state,
                clientesFacturas: action.clientes
            };
        case 'RECEIVE_VENDEDORES_FACTURAS':
            return {
                ...state,
                vendedoresFacturas: action.vendedores
            };
        default:
            return state;
    }
};
