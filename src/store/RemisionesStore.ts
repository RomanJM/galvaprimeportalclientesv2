import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface RemisionesStoreState {
    isloadingRemisiones: boolean,
    registrosRemisiones: any[],
    clientesRemisiones: any[],
    vendedoresRemisiones: any[],
    clienteRemisiones :  any
}

export interface RequestInformacionInicialRemisiones {
    type: 'REQUEST_INFORMACION_INICIAL_REMISIONES'
}

export interface ReceiveInformacionInicialRemisiones {
    type: 'RECEIVE_INFORMACION_INICIAL_REMISIONES',
    registros: any[],
    cliente : any
}
export interface ReceiveInformacionClientesRemisiones {
    type: 'RECEIVE_CLIENTES_REMISIONES',
    clientes: any[]
}
export interface ReceiveInformacionVendedoresRemisiones {
    type: 'RECEIVE_VENDEDORES_REMISIONES',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicialRemisiones | ReceiveInformacionInicialRemisiones | ReceiveInformacionClientesRemisiones | ReceiveInformacionVendedoresRemisiones;

export const actionCreators = {
    requestInformacionInicialRemisiones: (cliente: any, vendedor: any, fechaInicial : any , fechaFinal : any, factura : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var fac = parseInt(factura);
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor : 0,
            _Fecha_desde: fechaInicial,
            _Fecha_hasta: fechaFinal,
            DocNum: fac ? fac : 0
        };
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_REMISIONES' });
        var respnseA = axios.post(url.RemisionLista, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data.Message2);
                var fecha: any = document.getElementById('fecha_actualizacion');
                fecha.innerHTML = data.Message2;
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_REMISIONES', registros: data.OData ? data.OData : [], cliente: cliente ? cliente : '' });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_REMISIONES', registros: [], cliente: cliente ? cliente : ''});
                console.log(error);
            })
    },
    requestDowloadExcelRemisiones: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        window.location.href = url.SeguimientoExcel;
    },
    requestClientesRemisiones: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_REMISIONES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_REMISIONES', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientesRemisiones: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_REMISIONES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_REMISIONES', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresRemisiones: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_REMISIONES', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_REMISIONES', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadArchivoRemision: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo : tipo
        };
        var respnseA = axios.post(url.RemisionArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                console.log(response);
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoReciboRemision: (recibo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: recibo ? parseInt(recibo) : 0
        };
        var respnseA = axios.post(url.ReciboArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', recibo + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoCertificadoRemision: (certificado: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: certificado ? parseInt(certificado) : 0
        };
        var respnseA = axios.post(url.CertificadoArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', certificado + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: RemisionesStoreState = {
    isloadingRemisiones: false,
    registrosRemisiones: [],
    clientesRemisiones: [],
    vendedoresRemisiones: [],
    clienteRemisiones: ""
};

export const reducer: Reducer<RemisionesStoreState> = (state: RemisionesStoreState | undefined, incomingAction: Action): RemisionesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_REMISIONES':
            return {
                ...state,
                isloadingRemisiones: true,
                registrosRemisiones: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_REMISIONES':
            return {
                ...state,
                isloadingRemisiones: false,
                registrosRemisiones: action.registros,
                clienteRemisiones: action.cliente
            };
        case 'RECEIVE_CLIENTES_REMISIONES':
            return {
                ...state,
                clientesRemisiones: action.clientes
            };
        case 'RECEIVE_VENDEDORES_REMISIONES':
            return {
                ...state,
                vendedoresRemisiones: action.vendedores
            };
        default:
            return state;
    }
};
