import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface LogInStoreState {
    isloadingLogIn: boolean,
    Token: any,
    NombreUsuario: any,
    Usuario_id: any,
    ZEmpleado: any,
    MenuUsuario: any[],
    rolusuario: any,
    FechaActualizacion: any,
    menuActivo: any,
    submenuActivo: any,
    historyp: any,

}

export interface RequestInformationLogIn{
    type:'REQUEST_INFORMATION_LOGIN'
}
export interface ReceiveInformationLogIn{
    type:'RECEIVE_INFORMATION_LOGIN',
    Token: any,
    NombreUsuario: any,
    Usuario_id: any,
    ZEmpleado: any,
    MenuUsuario: any[],
    rolusuario: any,
    historyp: any,
    fechaActualizacion : any
}
export interface RequestInformationFechaActualizacion{
    type:'REQUEST_INFORMATION_FECHA_ACTUALIZACION'
}
export interface ReceiveInformationFechaActualizacion{
    type:'RECEIVE_INFORMATION_FECHA_ACTUALIZACION',
    FechaActualizacion: any
}

export interface ReceiveInformationMenuActivo{
    type:'RECEIVE_INFORMATION_MENU_ACTIVO',
    menu: any,
    submenu: any
}

type KnownAction = RequestInformationLogIn | ReceiveInformationLogIn | RequestInformationFechaActualizacion | ReceiveInformationFechaActualizacion | ReceiveInformationMenuActivo ;

export const actionCreators = {
    LogIn: (user: string, password: string, history: any, t: any): AppThunkAction<KnownAction> => (dispatch, getState) => {

        var respnseA = axios.post(url.LogIn, {
			Usuario_id: user,
			password: password
		})
			.then(response => response.data)
			.then(data => {
                console.log(data);
				if (data.Code != "") {
                    var rol = _.find(data.OData._UsuarioMo._GrupouList, { Es_activo: true });
                    dispatch({
                        type:'RECEIVE_INFORMATION_LOGIN',
                        Token: data.Code,
                        NombreUsuario: data.OData._UsuarioMo.Nombre,
                        Usuario_id: data.OData._UsuarioMo.Usuario_id,
                        ZEmpleado: data.OData._UsuarioMo.ZEmpleado,
                        MenuUsuario:data.OData._UsuarioMo._MenuList,
                        rolusuario: rol ? rol.Grupou_code : "",
                        historyp: history,
                        fechaActualizacion : data.OData.Fecha_actualiza
                    });

					var respnseA = axios.get(url.ParametroFechaActualizacion, { headers: { "Authorization": `Bearer ${data.Code}` } })
						.then(response => response.data)
						.then(data2 => {	
                            var fecha = "";						
							if (data2.Code == "") {
								fecha = data2.OData.Valor;
							}
                            dispatch({type:'RECEIVE_INFORMATION_FECHA_ACTUALIZACION',FechaActualizacion : fecha});
						})
						.catch(error => {
							console.log(error);
						})

					history.replace({ pathname: "/Inicio" });
					
				} else {
					toastr.warning(data.Message, t('common:Title'));
                }
			})
			.catch(error => {
				console.log(error);
			})
    },
    setMenuActive: (idMenu: any, submenu: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type: 'RECEIVE_INFORMATION_MENU_ACTIVO', menu: idMenu, submenu: submenu});
    }

};

const unloadedState: LogInStoreState = {
    isloadingLogIn: false,
    FechaActualizacion: null,
    MenuUsuario: [],
    NombreUsuario: null,
    Token: null,
    Usuario_id: null,
    ZEmpleado: null,
    rolusuario: null,
    menuActivo: null,
    historyp: null,
    submenuActivo: null
};

export const reducer: Reducer<LogInStoreState> = (state: LogInStoreState | undefined, incomingAction: Action): LogInStoreState => {
    state = window.localStorage.getItem("state") ? JSON.parse(String(window.localStorage.getItem("state"))) : state;
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMATION_LOGIN':
            return {
                ...state,
                isloadingLogIn: true,
            };
        case 'RECEIVE_INFORMATION_LOGIN':
            var data = {
                ...state,
                isloadingLogIn: false,
                MenuUsuario: action.MenuUsuario,
                NombreUsuario: action.NombreUsuario,
                Token: action.Token,
                Usuario_id: action.Usuario_id,
                ZEmpleado: action.ZEmpleado,
                rolusuario: action.rolusuario,
                historyp: action.historyp,
                FechaActualizacion :  action.fechaActualizacion
            };
            window.localStorage.setItem("state", JSON.stringify(data));
            return data;

        case 'RECEIVE_INFORMATION_FECHA_ACTUALIZACION':
            return {
                ...state,
                FechaActualizacion: action.FechaActualizacion
            };    
        case 'RECEIVE_INFORMATION_MENU_ACTIVO':
            var dataa = {
                ...state,
                menuActivo: action.menu,
                submenuActivo: action.submenu
            };
            window.localStorage.setItem("state", JSON.stringify(dataa));
            return dataa;
        default:
            return state;
    }
};

