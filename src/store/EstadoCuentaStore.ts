﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface EstadoCuentaStoreState {
    isloadingEdoCuenta: boolean,
    registrosEdoCuenta: any[],
    clientesEdoCuenta: any[],
    vendedoresEdoCuenta: any[],
    clienteEdoCuenta: any,
    vendedorEdoCuenta : any,
    primerRegistro : any,
    valor : any,
    valorTxt : any
}
export interface RequestInformacionInicialEstadoCuentaAction {
    type: 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA'
}

export interface ReceiveInformacionInicialEstadoCuentaAction {
    type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA',
    registros: any[],
    cliente: any,
    vendedor : any,
    primerRegistro : any,
    valor : any,
    valorTxt : any
}

export interface ReceiveInformacionClientesEstadoCuentaAction {
    type: 'RECEIVE_CLIENTES_ESTADOCUENTA',
    clientes: any[]
}

export interface ReceiveInformacionVendedoresEstadoCuentaAction {
    type: 'RECEIVE_VENDEDORES_ESTADOCUENTA',
    vendedores: any[]
}
export interface ChangeValorAction {
    type: 'CHANGE_VALOR',
    valor : any
}
type KnownAction = RequestInformacionInicialEstadoCuentaAction |
    ReceiveInformacionInicialEstadoCuentaAction | ReceiveInformacionClientesEstadoCuentaAction
    | ReceiveInformacionVendedoresEstadoCuentaAction|ChangeValorAction;

const unloadedState: EstadoCuentaStoreState = {
    isloadingEdoCuenta: false,
    registrosEdoCuenta: [],
    clientesEdoCuenta: [],
    vendedoresEdoCuenta: [],
    clienteEdoCuenta: "",
    vendedorEdoCuenta : 0,
    primerRegistro : null,
    valor : 0,
    valorTxt : "USD"
};

export const actionCreators = {
    requestInformacionInicialEdoCuenta: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA' });
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        axios.post(url.EstadoCuentaResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {             
                var registro = _.orderBy(data.OData, (x: any) => { return x.Orden });
                var primerRegistro = _.find(registro, (item: any) => {
                    return item;
                  });
                  var valor = primerRegistro ? primerRegistro.Valor2 : 0;
                  var txt = valor == 1 ? "MXN" : "USD"
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA', registros: registro, cliente : cliente, vendedor : vendedor ,primerRegistro : primerRegistro,valor : valor,valorTxt : txt});
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA', registros: [] , cliente: cliente, vendedor : vendedor,primerRegistro : null, valor : 0, valorTxt : "USD"});
                console.log(error);
            })
    },
    requestClientesEdoCuenta: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientesEdoCuenta: (vendedor : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresEdoCuenta: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_ESTADOCUENTA', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_ESTADOCUENTA', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadExcelEdoCuenta: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            _Vence: '',
            SlpCode: vendedor ? vendedor : 0
        };
        var respnseA = axios.post(url.EstadoCuentaExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Estado de cuenta.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    changeValor: (valor : string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'CHANGE_VALOR', valor : valor });
    }
};

export const reducer: Reducer<EstadoCuentaStoreState> = (state: EstadoCuentaStoreState | undefined, incomingAction: Action): EstadoCuentaStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA':
            return {
                ...state,
                isloadingEdoCuenta: true,
                registrosEdoCuenta: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA':
            return {
                ...state,
                isloadingEdoCuenta: false,
                registrosEdoCuenta: action.registros,
                clienteEdoCuenta: action.cliente,
                vendedorEdoCuenta: action.vendedor,
                primerRegistro : action.primerRegistro,
                valor : action.valor,
                valorTxt : action.valorTxt
            };
        case 'RECEIVE_CLIENTES_ESTADOCUENTA':
            return {
                ...state,
                clientesEdoCuenta: action.clientes
            };
        case 'RECEIVE_VENDEDORES_ESTADOCUENTA':
            return {
                ...state,
                vendedoresEdoCuenta: action.vendedores
            };
            case 'CHANGE_VALOR':
            return {
                ...state,
                valorTxt : action.valor
            };
        default:
            return state;
    }
};