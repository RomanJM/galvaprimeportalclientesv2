
import * as RegistroEntradaCamionesStore from './RegistroEntradaCamionesStore';
import * as DescargaCamionesStore from './DescargaCamionesStore';
import * as SeguimientoOCStore from './SeguimientoOCStore';
import * as DashboardStore from './DashboardStore';
import * as LogInStore from './LogInStore';
import * as NavbarTopStore from './NavbarTopStore';
import * as SeguimientoOCDetalleStore from './SeguimientoOCDetalleStore';
import * as InventariosStore from './InventariosStore';
import * as InventarioDetallesStore from './InventarioDetallesStore';
import * as EmbarquesStore from './EmbarquesStore';
import * as EmbarqueDetallesStore from './EmbarqueDetallesStore';
import * as EstadoCuentaStore from './EstadoCuentaStore';
import * as EstadoCuentaDetalleStore from './EstadoCuentaDetalleStore';
import * as ContactosStore from './ContactosStore';
import * as FacturasStore from './FacturasStore';
import * as RemisionesStore from './RemisionesStore';
import * as CertificadosStore from './CertificadosStore';

// The top-level state object
export interface ApplicationState {
    RegistroEntradaCamionesStore: RegistroEntradaCamionesStore.RegistroEntradaCamionesStoreState,
    DescargaCamionesStore : DescargaCamionesStore.DescargaCamionesStoreState,
    SeguimientoOCStore: SeguimientoOCStore.SeguimientoOCStoreState,
    DashboardStore: DashboardStore.DashboardStoreState,
    LogInStore: LogInStore.LogInStoreState,
    NavbarTopStore: NavbarTopStore.NavbarTopState,
    SeguimientoOCDetalleStore: SeguimientoOCDetalleStore.SeguimientoOCDetalleStoreState,
    InventariosStore: InventariosStore.InventariosStoreState,
    InventarioDetallesStore: InventarioDetallesStore.InventarioDetallesStoreState
    EmbarquesStore: EmbarquesStore.EmbarquesStoreState,
    EmbarqueDetallesStore: EmbarqueDetallesStore.EmbarqueDetallesStoreState
    EstadoCuentaStore: EstadoCuentaStore.EstadoCuentaStoreState,
    EstadoCuentaDetalleStore: EstadoCuentaDetalleStore.EstadoCuentaDetalleStoreState,
    ContactosStore: ContactosStore.ContactosStoreState,
    FacturasStore : FacturasStore.FacturasStoreState,
    RemisionesStore : RemisionesStore.RemisionesStoreState,
    CertificadosStore : CertificadosStore.CertificadosStoreState
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    RegistroEntradaCamionesStore: RegistroEntradaCamionesStore.reducer,
    DescargaCamionesStore : DescargaCamionesStore.reducer,
    SeguimientoOCStore: SeguimientoOCStore.reducer,
    DashboardStore: DashboardStore.reducer,
    LogInStore: LogInStore.reducer,
    NavbarTopStore: NavbarTopStore.reducer,
    SeguimientoOCDetalleStore: SeguimientoOCDetalleStore.reducer,
    InventariosStore: InventariosStore.reducer,
    InventarioDetallesStore: InventarioDetallesStore.reducer,
    EmbarquesStore: EmbarquesStore.reducer,
    EmbarqueDetallesStore: EmbarqueDetallesStore.reducer,
    EstadoCuentaStore: EstadoCuentaStore.reducer,
    EstadoCuentaDetalleStore: EstadoCuentaDetalleStore.reducer,
    ContactosStore: ContactosStore.reducer,
    FacturasStore : FacturasStore.reducer,
    RemisionesStore : RemisionesStore.reducer,
    CertificadosStore :  CertificadosStore.reducer
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
