import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface NavbarTopState {
    isloadingNavbarTop: boolean,
    clientes: any[],
    vendedores: any[],
    vista: any,
    selectedCliente : any,
    selectedVendedor : any,
    mostrarSelects : boolean,
    fechaInicioFactura : any,
    fechaFinFactura : any,
    facturaDoc : any,
    NumAtCard:any,
    remisionDoc : any,
    lote : any,
    certificado : any,
    NumAtCardcer: any
}


export interface ReceiveInformacionClientes {
    type: 'RECEIVE_CLIENTES',
    clientes: any[]
}
export interface ReceiveInformacionVendedores {
    type: 'RECEIVE_VENDEDORES',
    vendedores: any[]
}
export interface ReceiveInformacionVista {
    type: 'RECEIVE_VISTA',
    vista: any,
    mostrarSelects : boolean
}
export interface SetClienteVendedor {
    type: 'SET_CLIENTE_VENDEDOR',
    cliente: any,
    vendedor : any
}
export interface SetFechaInicioFactura {
    type: 'SET_FECHA_INICIO_FACTURA',
    fecha: any
}
export interface SetFechaFinFactura {
    type: 'SET_FECHA_FIN_FACTURA',
    fecha: any
}
export interface SetFacturaDoc {
    type: 'SET_FACTURA_DOC',
    numDoc: any
}
export interface SetRemisionDoc {
    type: 'SET_REMISION_DOC',
    numDoc: any
}

export interface SetNumAtCard {
    type: 'SET_REFERENCIA_DOC',
    NumAtCard: any
}
export interface SetNumAtCardCer {
    type: 'SET_REFERENCIA_DOC_CER',
    NumAtCard: any
}
export interface SetLote {
    type: 'SET_LOTE',
    lote: any
}
export interface SetCertificado{
    type: 'SET_CERTIFICADO',
    certificado: any
}
type KnownAction = ReceiveInformacionClientes | ReceiveInformacionVendedores | 
ReceiveInformacionVista|SetClienteVendedor|SetFechaInicioFactura|SetFechaFinFactura
|SetFacturaDoc |SetRemisionDoc|SetLote|SetCertificado|SetNumAtCard|SetNumAtCardCer
;
                    
export const actionCreators = {
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: [] });
                console.log(error);
            })
    },
    setVista: ( vista: any, mostrarSelects : boolean): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'RECEIVE_VISTA', vista: vista,mostrarSelects : mostrarSelects});
    },
    setClienteVendedor: ( cliente: any, vendedor : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
         console.log(cliente, vendedor);
        dispatch({type:'SET_CLIENTE_VENDEDOR', cliente: cliente ? cliente : "", vendedor : vendedor ? vendedor : ""});
    },
    setFechaInicio: ( fechaInicio: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_FECHA_INICIO_FACTURA', fecha: fechaInicio });
    },
    setFechaFin: ( fechaFin: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_FECHA_FIN_FACTURA', fecha: fechaFin });
    },
    setFacturaDoc: ( numDoc: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_FACTURA_DOC', numDoc: numDoc });
    },
    setRemisionDoc: ( numDoc: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_REMISION_DOC', numDoc: numDoc });
    },
    setNumAtCard: ( NumAtCard: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_REFERENCIA_DOC', NumAtCard: NumAtCard });
    },  
    setNumAtCardCer: ( NumAtCard: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_REFERENCIA_DOC_CER', NumAtCard: NumAtCard });
    },
    setLote: ( lote: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_LOTE', lote: lote });
    },
    setCertificado: ( cert: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({type:'SET_CERTIFICADO', certificado: cert });
    }
};

const unloadedState: NavbarTopState = {
    isloadingNavbarTop: false,
    clientes: [],
    vendedores: [],
    vista: null,
    selectedCliente : null,
    selectedVendedor : null,
    mostrarSelects : false,
    fechaInicioFactura : new Date(),
    fechaFinFactura : new Date(),
    facturaDoc : null,
    remisionDoc : null,
    lote : null,
    NumAtCard:null,
    certificado : null,
    NumAtCardcer: null
};

export const reducer: Reducer<NavbarTopState> = (state: NavbarTopState | undefined, incomingAction: Action): NavbarTopState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) { 
        case 'RECEIVE_VISTA':
            return {
                ...state,
                vista: action.vista,
                mostrarSelects : action.mostrarSelects
            }
        case 'RECEIVE_CLIENTES':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES':
            return {
                ...state,
                vendedores: action.vendedores
            };
            case 'SET_CLIENTE_VENDEDOR':
                return {
                    ...state,
                    selectedCliente: action.cliente,
                    selectedVendedor : action.vendedor
                }
            case 'SET_FECHA_INICIO_FACTURA':
                    return {
                        ...state,
                        fechaInicioFactura: action.fecha
                    }
            case 'SET_FECHA_FIN_FACTURA':
                    return {
                     ...state,
                     fechaFinFactura: action.fecha
                    }
            case 'SET_FACTURA_DOC':
               return {
                ...state,
                  facturaDoc: action.numDoc
                  }      
            case 'SET_REMISION_DOC':
                return {
                ...state,
                remisionDoc: action.numDoc
                }
                case 'SET_LOTE':
                    return {
                    ...state,
                    lote: action.lote
            }   
            case 'SET_CERTIFICADO':
                return {
                ...state,
                certificado: action.certificado
                } 

                case 'SET_REFERENCIA_DOC':
                    return {
                    ...state,

                    NumAtCard: action.NumAtCard
                    }   
                    case 'SET_REFERENCIA_DOC_CER':
                        return {
                        ...state,
    
                        NumAtCardcer: action.NumAtCard
                        }                
        default:
            return state;
    }
};

