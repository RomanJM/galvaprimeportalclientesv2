import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

export interface RegistroEntradaCamionesStoreState {
    isloading: boolean,
    registros: any
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL_DASHBOARD'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD',
    registros: any
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial;

export const actionCreators = {
    requestInformacionInicial: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

    }
    
};

const unloadedState: RegistroEntradaCamionesStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<RegistroEntradaCamionesStoreState> = (state: RegistroEntradaCamionesStoreState | undefined, incomingAction: Action): RegistroEntradaCamionesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };

        default:
            return state;
    }
};

