﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface InventariosStoreState {
    isloadingInventarios: boolean,
    registrosInventarios: any[],
    clientesInventarios: any[],
    vendedoresInventarios: any[],
    esAlmacen : boolean
}
export interface RequestInformacionInicialInventariosAction {
    type: 'REQUEST_INFORMACION_INICIAL_INVENTARIOS'
}

export interface ReceiveInformacionInicialInventariosAction {
    type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS',
    registros: any[]
}

export interface ReceiveInformacionClientesInventariosAction {
    type: 'RECEIVE_CLIENTES_INVENTARIOS',
    clientes: any[]
}

export interface ReceiveInformacionVendedoresInventariosAction {
    type: 'RECEIVE_VENDEDORES_INVENTARIOS',
    vendedores: any[]
}
export interface SetAlmacen {
    type: 'SET_ALMACEN',
    esalmacen: boolean
}
type KnownAction = RequestInformacionInicialInventariosAction |
    ReceiveInformacionInicialInventariosAction  | ReceiveInformacionClientesInventariosAction
     | ReceiveInformacionVendedoresInventariosAction |SetAlmacen;

const unloadedState: InventariosStoreState = {
    isloadingInventarios: false,
    registrosInventarios: [],
    clientesInventarios: [],
    vendedoresInventarios : [],
    esAlmacen : true
};

export const actionCreators = {
    requestInformacionInicialInventarios: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_INVENTARIOS' });
        var data_str = {
            CardCode: cliente ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(data_str);
        axios.post(url.InventarioResumenAlmacen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
           })
            .then(response => response.data)
            .then(data => {              
                var registro = _.orderBy(data.OData, (x: any) => { return x.Orden });
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS', registros: registro });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS', registros: [] });
                console.log(error);
            })
    },
    requestInformacionInicialMercado: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_INVENTARIOS' });
        var data_str = {
            CardCode: cliente ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        axios.post(url.InventarioResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
              
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS', registros: [] });
                console.log(error);
            })
    },
    requestClientesInventarios: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_INVENTARIOS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_INVENTARIOS', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientesInventarios: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        console.log(vendedor);
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_INVENTARIOS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_INVENTARIOS', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresInventarios: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_INVENTARIOS', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_INVENTARIOS', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadExcelInventarios: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente: '',
            CodAlmacen: '',
            SlpCode: vendedor ? vendedor: 0
        };
        var respnseA = axios.post(url.InventarioExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Inventarios.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    setAlmacen: ( esAlmacen : boolean): AppThunkAction<KnownAction> => (dispatch, getState) => {

       dispatch({type:'SET_ALMACEN', esalmacen : esAlmacen});
   }
};

export const reducer: Reducer<InventariosStoreState> = (state: InventariosStoreState | undefined, incomingAction: Action): InventariosStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_INVENTARIOS':
            return {
                ...state,
                isloadingInventarios: true,
                registrosInventarios: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_INVENTARIOS':
            return {
                ...state,
                isloadingInventarios: false,
                registrosInventarios: action.registros
            };
        case 'RECEIVE_CLIENTES_INVENTARIOS':
            return {
                ...state,
                clientesInventarios: action.clientes
            };
        case 'RECEIVE_VENDEDORES_INVENTARIOS':
            return {
                ...state,
                vendedoresInventarios: action.vendedores
            };
            case 'SET_ALMACEN':
                return {
                    ...state,
                    esAlmacen: action.esalmacen
                };
        default:
            return state;
    }
};