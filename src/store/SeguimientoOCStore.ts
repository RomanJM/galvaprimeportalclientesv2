import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface SeguimientoOCStoreState {
    isloadingSeguimientoOC: boolean,
    registrosSOC: any[],
    clientesSOC: any[],
    vendedoresSOC : any [], 
    otrocampo: any
}

export interface RequestInformacionInicialSOC {
    type: 'REQUEST_INFORMACION_INICIAL_SEGUIMIENTO_OC'
}

export interface ReceiveInformacionInicialSOC {
    type: 'RECEIVE_INFORMACION_INICIAL_SEGUIMIENTO_OC',
    registros: any[]
}
export interface ReceiveInformacionClientesSOC {
    type: 'RECEIVE_CLIENTES_SEGUIMIENTO_OC',
    clientes: any[]
}
export interface ReceiveInformacionVendedoresSOC {
    type: 'RECEIVE_VENDEDORES_SEGUIMIENTO_OC',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicialSOC | ReceiveInformacionInicialSOC | ReceiveInformacionClientesSOC | ReceiveInformacionVendedoresSOC;

export const actionCreators = {
    requestInformacionInicialSOC: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
       console.log(cliente);
       console.log(vendedor);
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor: 0
        };
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_SEGUIMIENTO_OC' });
        var respnseA = axios.post(url.SeguimientoResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
               
                var registro = _.orderBy(data.OData, (x: any) => {return x.Orden});
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_SEGUIMIENTO_OC', registros: registro });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_SEGUIMIENTO_OC', registros: [] });
                console.log(error);
            })
    },
    requestDowloadExcelSOC: (cliente: string, vendedor: string, t: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            Vencimiento: '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.SeguimientoExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //console.log(JSON.stringify(response.headers));
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', (cliente ? (cliente + "_") : '') + t('xls_title_ar') +".xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })

        //window.location.href = url.SeguimientoExcel;
    },
    requestClientesSOC: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor: 0 }, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                     clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                     });
                    clientes = _.orderBy(clientes, ['descripcion']);
                } 
                dispatch({ type: 'RECEIVE_CLIENTES_SEGUIMIENTO_OC', clientes: data.Type == "success" ? clientes : [] });
              })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_SEGUIMIENTO_OC', clientes: [] });
                console.log(error);
            })
      
    },
    requestFiltraClientesSOC: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_SEGUIMIENTO_OC', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_SEGUIMIENTO_OC', clientes: [] });
                console.log(error);
            })

    },
    requestVendedoresSOC: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {                      
                        return { "clave": x.SlpCode, "descripcion": x.SlpName}
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_SEGUIMIENTO_OC', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_SEGUIMIENTO_OC', vendedores: [] });
                console.log(error);
            })
    }
};

const unloadedState: SeguimientoOCStoreState = {
    isloadingSeguimientoOC: false,
    registrosSOC: [],
    clientesSOC: [],
    vendedoresSOC :[], 
    otrocampo: null
};

export const reducer: Reducer<SeguimientoOCStoreState> = (state: SeguimientoOCStoreState | undefined, incomingAction: Action): SeguimientoOCStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_SEGUIMIENTO_OC':
            return {
                ...state,
                isloadingSeguimientoOC: true,
                registrosSOC: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_SEGUIMIENTO_OC':
            return {
                ...state,
                isloadingSeguimientoOC: false,
                registrosSOC: action.registros
            };
        case 'RECEIVE_CLIENTES_SEGUIMIENTO_OC':
            return {
                ...state,
                clientesSOC: action.clientes
            };
        case 'RECEIVE_VENDEDORES_SEGUIMIENTO_OC':
            return {
                ...state,
                vendedoresSOC: action.vendedores
            };
        default:
            return state;
    }
};
