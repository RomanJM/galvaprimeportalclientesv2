﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface EmbarquesStoreState {
    isloadingEmbarques: boolean,
    registrosEmbarques: any[],
    clientesEmbarques: any[],
    vendedoresEmbarques: any[]
}
export interface RequestInformacionInicialEmbarquesAction {
    type: 'REQUEST_INFORMACION_INICIAL_Embarques'
}

export interface ReceiveInformacionInicialEmbarquesAction {
    type: 'RECEIVE_INFORMACION_INICIAL_Embarques',
    registrosEmbarques: any[]
}

export interface ReceiveInformacionclientesEmbarquesEmbarquesAction {
    type: 'RECEIVE_clientesEmbarques_Embarques',
    clientesEmbarques: any[]
}

export interface ReceiveInformacionvendedoresEmbarquesEmbarquesAction {
    type: 'RECEIVE_vendedoresEmbarques_Embarques',
    vendedoresEmbarques: any[]
}
type KnownAction = RequestInformacionInicialEmbarquesAction |
    ReceiveInformacionInicialEmbarquesAction | ReceiveInformacionclientesEmbarquesEmbarquesAction
    | ReceiveInformacionvendedoresEmbarquesEmbarquesAction;

const unloadedState: EmbarquesStoreState = {
    isloadingEmbarques: false,
    registrosEmbarques: [],
    clientesEmbarques: [],
    vendedoresEmbarques: []
};

export const actionCreators = {
    requestInformacionInicialEmbarques: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_Embarques' });
        var data_str = {
            CardCode: cliente && cliente != "0"? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        axios.post(url.EmbarqueResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {              
                var registro = _.orderBy(data.OData, (x: any) => { return x.Orden });
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarques', registrosEmbarques: registro });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarques', registrosEmbarques: [] });
                console.log(error);
            })
    },
    requestclientesEmbarques: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientesEmbarques: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientesEmbarques = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientesEmbarques = _.orderBy(clientesEmbarques, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_clientesEmbarques_Embarques', clientesEmbarques: data.Type == "success" ? clientesEmbarques : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_clientesEmbarques_Embarques', clientesEmbarques: [] });
                console.log(error);
            })

    },
    requestFiltraclientesEmbarques: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientesEmbarques: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientesEmbarques = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientesEmbarques = _.orderBy(clientesEmbarques, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_clientesEmbarques_Embarques', clientesEmbarques: data.Type == "success" ? clientesEmbarques : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_clientesEmbarques_Embarques', clientesEmbarques: [] });
                console.log(error);
            })

    },
    requestvendedoresEmbarques: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedoresEmbarques: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedoresEmbarques = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedoresEmbarques = _.orderBy(vendedoresEmbarques, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_vendedoresEmbarques_Embarques', vendedoresEmbarques: data.Type == "success" ? vendedoresEmbarques : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_vendedoresEmbarques_Embarques', vendedoresEmbarques: [] });
                console.log(error);
            })
    },
    requestDowloadExcelEmbarques: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente: '',
            _Vence: '',
            SlpCode: vendedor ? vendedor : 0
        };
        var respnseA = axios.post(url.EmbarqueExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + getState().LogInStore.Token,
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Embarques.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

export const reducer: Reducer<EmbarquesStoreState> = (state: EmbarquesStoreState | undefined, incomingAction: Action): EmbarquesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_Embarques':
            return {
                ...state,
                isloadingEmbarques: true,
                registrosEmbarques: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_Embarques':
            return {
                ...state,
                isloadingEmbarques: false,
                registrosEmbarques: action.registrosEmbarques
            };
        case 'RECEIVE_clientesEmbarques_Embarques':
            return {
                ...state,
                clientesEmbarques: action.clientesEmbarques
            };
        case 'RECEIVE_vendedoresEmbarques_Embarques':
            return {
                ...state,
                vendedoresEmbarques: action.vendedoresEmbarques
            };
        default:
            return state;
    }
};