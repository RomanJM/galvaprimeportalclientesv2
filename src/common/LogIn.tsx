import React from "react";
import * as _ from "lodash";
import { Button, Card, Row, Col, Form, Carousel } from "react-bootstrap";
import axios from "axios";
import * as toastr from "./Toast";
import * as url from "../common";

import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store/index";
import { AnyAction, bindActionCreators } from "redux";
import * as LogInStore from "../store/LogInStore";
import { translate } from 'react-i18next';
import Idioma from './Idioma'

type LogInProps = LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any, i18n?: any} &
  RouteComponentProps<{}> ;

interface LogInState {
  Usuario: string;
  Contrasena: string;
}
class LogIn extends React.Component<LogInProps, LogInState> {
  constructor(props: any) {
    super(props);
    this.state = {
      Usuario: "",
      Contrasena: "",
    };
  }
  public LogIn = () => {
    const {t} = this.props;
    if (!this.state.Usuario) {
      toastr.warning(t('msg_valida_usuario'), t('common:Title'));
      return false;
    }
    if (!this.state.Contrasena) {
      toastr.warning(t('msg_valida_password'), t('common:Title'));
      return false;
    }
   // this.props.history.replace({ pathname: "/Inicio" });
    this.props.LogIn(
      this.state.Usuario,
      this.state.Contrasena,
      this.props.history,
      t
    );
  };
  public render() {
    console.log("props",this.props);
    const {t, i18n} = this.props;
    
    return (
      <div>
        
        <Row className="rowlogin">
          <Col
            xs={12}
            sm={12}
            md={5}
            lg={5}
            xl={5}
            className="text-center"
            style={{ position: "relative" }}
          >
            
            <img src="images/logo.png" className="imagelogin"  />
          </Col>
          <Col
            xs={12}
            sm={12}
            md={7}
            lg={7}
            xl={7}
            style={{ paddingTop: "1em" }}
          >
            <nav className="navbar navbar-expand-lg navlogin">
              <div className="container-fluid">
                <button
                  className="navbar-toggler"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#navbarTogglerDemo01"
                  aria-controls="navbarTogglerDemo01"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div
                  className="collapse navbar-collapse"
                  id="navbarTogglerDemo01"
                >
                  <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                      {" "}
                      <a
					  target="_blank"
                        className="nav-link opcionmenu"
                        aria-current="page"
                        href="http://galvaprime.com/conozcanos/"
                      >
                        {t('lbl_nosotros')}
                      </a>
                    </li>
                    <li className="nav-item">
                      {" "}
                      <a
					  target="_blank"
                        className="nav-link opcionmenu"
                        aria-current="page"
                        href="http://galvaprime.com/productos/"
                      >
                        {t('lbl_productos')}
                      </a>
                    </li>
                    <li className="nav-item">
                      {" "}
                      <a
					  target="_blank"
                        className="nav-link opcionmenu"
                        aria-current="page"
                        href="http://galvaprime.com/servicios/"
                      >
                        {t('lbl_servicios')}
                      </a>
                    </li>
                    <li className="nav-item">
                      {" "}
                      <a
                        className="nav-link opcionmenu"
                        aria-current="page"
                        onClick={() => {
                          window.location.replace("/recuperapassword");
                        }}
                      >
                        {t('lbl_olvide_contrasena')}
                      </a>
                    </li>
                    <li className="nav-item">
                      {" "}
                      <a
					  target="_blank"
                        className="nav-link opcionmenu"
                        aria-current="page"
                        href="https://api.whatsapp.com/send/?phone=%2B5218124698582&text=Hola+Galvaprime&app_absent=0"
                      >
                        {t('lbl_contacto')}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          </Col>
        </Row>
        <Row style={{ width: "100%" }}>
          <Col
            xs={12}
            sm={12}
            md={5}
            lg={5}
            xl={5}
            style={{ textAlign: "center" }}
          >
            <Card
              className="cardlogin"
              style={{
                margin: "0 auto",
                background:
                  "linear-gradient(to right, rgba(13,126,194,1) 0%, rgba(13,165,194,0.94) 100%)",
              }}
            >
              <Card.Body>
              <Row>
                <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Idioma />
                </Col>
              </Row>
                <Row>
                  <Col
                    xs={12}
                    sm={12}
                    md={12}
                    lg={12}
                    xl={12}
                    style={{
                      color: "#FFF",
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    {t('title_login')}
                  </Col>
                </Row>
                <br />
                <br />
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Form.Group>
                      <Form.Control
                        onChange={(evt: any) => {
                          this.setState({ Usuario: evt.target.value });
                        }}
                        placeholder={t('input_user')}
                        value={this.state.Usuario}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Form.Group>
                      <Form.Control
                        type="password"
                        onChange={(evt: any) => {
                          this.setState({ Contrasena: evt.target.value });
                        }}
                        placeholder={t('input_password')}
                        onKeyUp={(evt: any) => {
                          if (evt.keyCode == 13) this.LogIn();
                        }}
                        value={this.state.Contrasena}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Button
                      variant="primary"
                      size="sm"
                      onClick={this.LogIn}
                      style={{
                        backgroundColor: "#FFF",
                        color: "#0D7EC2",
                        float: "left",
                        fontWeight: "bold",
                        padding: " 5px 20px",
                        borderRadius: "50px",
                      }}
                    >
                      {t('btn_ingresar')}
                    </Button>
                  </Col>
                </Row>
                <br />
                {/*<Row>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group >
                                    <Form.Label style={{color: '#fff', fontStyle:'italic'}}>Olvidé la contraseña</Form.Label>
                                </Form.Group>
                            </Col>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group  >
                                    <Form.Label style={{color: '#fff', fontStyle:'italic'}}>Registrarme</Form.Label>
                                </Form.Group>
                            </Col>
                        </Row>*/}
              </Card.Body>
            </Card>
          </Col>
          <Col
            xs={12}
            sm={12}
            md={1}
            lg={1}
            xl={1}
            style={{ textAlign: "center" }}
          >		
		  </Col>		  
          <Col
            xs={12}
            sm={12}
            md={6}
            lg={6}
            xl={6}
            style={{ textAlign: "center" }}
          >
            <div style={{ color: "#FFF" }} className="carousellogin">
              <Carousel
                prevLabel=""
                nextLabel=""
                prevIcon=""
                nextIcon=""
                fade={true}
                interval={3000}
              >
                <Carousel.Item>
                  <div style={{ textAlign: "left" }}>
                    <h5 style={{ margin: 0 }}>
                      <strong>{t('lbl_mision')}</strong>{" "}
                    </h5>
                    <p style={{ fontSize: "14px" }}>
                      {t('msg_mision')}
                    </p>
                  </div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={{ textAlign: "left" }}>
                    <h5 style={{ margin: 0 }}>
                      <strong>{t('lbl_vision')}</strong>{" "}
                    </h5>
                    <p style={{ fontSize: "14px" }}>
                      {" "}
                      <ul>
                        {" "}
                        <li>
                          {t('msg_vision_1')}
                        </li>
                        <li>{t('msg_vision_2')}</li>
                        <li>{t('msg_vision_3')}</li>
                      </ul>{" "}
                    </p>
                  </div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={{ textAlign: "left" }}>
                    <h5 style={{ margin: 0 }}>
                      <strong>{t('lbl_valores')}</strong>{" "}
                    </h5>
                    <p style={{ fontSize: "14px" }}>
                      {t('msg_valores')}
                    </p>
                  </div>
                </Carousel.Item>
              </Carousel>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.LogInStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...LogInStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('login')(LogIn as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
