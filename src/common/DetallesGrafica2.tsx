import * as React from "react";

interface DetallesGrafica2Props {
  labelTotal? : any;
  total: any;
  registros: any[];
  onClickTotal: any;
  onClickDetalle: any;
}
export default class DetallesGrafica2 extends React.Component<
  DetallesGrafica2Props,
  any
> {
  public render() {
    return (
      <div style={{overflow : "hidden",display : "flex", alignItems : "center", justifyContent : "center",paddingBottom : 0, paddingLeft : 5, paddingRight : 5}} className="detalles-grafica">
          <div>  <div
          onClick={() => {
            this.props.onClickTotal();
          }}
          style={{ cursor: "pointer" }}
        >
          <div className={`row ${this.props.registros.length <= 5 ? " total-min" : " total-max"}`}>
            <div
              style={{ fontWeight: "bold"}}
              className={`col col-lg-12 col-md-12 col-sm-12 col-xs-12 ${this.props.registros.length <= 7 ? "fontdetalletotal-min" : "fontdetalletotal-max"}`}
            >
              <div className="row">
                <div className="col col-lg-5 col-md-5">
                  <div style={{textAlign : "left", marginLeft : "18%"}}>{this.props.labelTotal}</div>
                </div>
                <div className="col col-lg-5 col-md-5">
                <div style={{textAlign : "right"}}>{this.props.total}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.props.registros.map((x: any) => {
          return (
            <div>
              <div
                style={{ cursor: "pointer", padding: "5px" , marginLeft : "0.5%"}}
                className={`row ${this.props.registros.length >= 6 ? "row-max" : "row-min"}`}
              >
                <div   className={`col-lg-12 col-md-12 col-sm-12 col-xs-12 ${this.props.registros.length <= 8 ? "fontdetallegrafica-min" : "fontdetallegrafica-max"}`}>
                  <div  className="row">
                    <div className="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div style={{textAlign : "justify", marginLeft : "2%"}} className={`texto ${x.Grupo.length > 12 ? "grupo" : "grupo-2"}`}>{x.Grupo}</div>                  
                    </div>
                    <div  style={{marginLeft : "-8%"}}  className="col col-lg-5 col-md-5 col-sm-12 col-12">                      
                        <div className="texto" style={{ fontWeight: "bold", textAlign : "right" }}>{x.Valor}</div>                     
                    </div>
                    <div
                    style={{  textAlign : "center", marginLeft: "-6%", paddingRight : 0,display : "flex", float : "right"}}                  
                      onClick={() => {
                        this.props.onClickDetalle(x.Grupo);
                      }}
                      className="col col-lg-1 col-md-1 col-sm-12 col-12"
                    >
                      <img
                        src={"../../images/icons/go.png"}
                        style={{ width: "20px", margin: "auto" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })} </div> 
      
      </div> 
    );
  }
}
