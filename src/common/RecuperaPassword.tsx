
import React from 'react';
import * as _ from 'lodash';
import { Button, Card, Row, Col, Form, Carousel} from 'react-bootstrap'; 
import axios from 'axios';
import * as toastr from './Toast';
import * as url from '../common';


export default class RecuperaPassword extends React.Component<any,any>{
    constructor(props: any){
        super(props);
        this.state = {
            Usuario: "",
            Contrasena: ""
        };

    }
    public Enviar = () =>{
        if (!this.state.Usuario) {
			toastr.warning("Ingresar usuario para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.Contrasena) {			
			toastr.warning("Ingresar contraseña para continuar", "Portal Clientes");
			return false;
		}	

        
		this.setState({ loading: true });
		var respnseA = axios.post(url.RecuperarPassword, {
			Usuario_id: this.state.Usuario,
			Email: this.state.Contrasena
		})
			.then(response => response.data)
			.then(data => {
				this.setState({ loading: false });
				if (data.Type == "success") {
					toastr.success(data.Message, "Portal Clientes");
				} else {
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			})
 
    }
    public LogIn = () => {
		this.props.history.replace({ pathname: "/" });
	}
    public render(){
        return <div>
            <Row style={{paddingTop:'5em'}}>
                        <Col xs={12} sm={12} md={5} lg={5} xl={5} className="text-center" style={{ position : "relative"}}>
                        <img 
                            src="images/logo.png"
                            className=""
                            width="260"
                            height="70"
                        /> 
                        </Col>
                        <Col xs={12} sm={12} md={7} lg={7} xl={7} style={{paddingTop:'1em'}}>
                             <nav className="navbar navbar-expand-lg" style={{fontSize:'14px'}} >
                                <div className="container-fluid">
                                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">                
                                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                                        
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page" href="#">NOSOTROS</a></li>
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page" href="#">PRODUCTOS</a></li>
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page" href="#">SERVICIOS</a></li>
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page"  onClick={()=>{
                                                window.location.replace("/recuperapassword");
                                            }}>OLVIDE LA CONTRASEÑA</a></li>
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page" href="#">REGISTRARME</a></li>
                                            <li className="nav-item"> <a className="nav-link opcionmenu" aria-current="page" href="#">CONTACTO</a></li>
                                        
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </Col>
                    </Row> 
                <Row style={{width:'100%'}}>
                    <Col xs={12} sm={12} md={5} lg={5} xl={5} style={{textAlign:'center'}}>                     
                    <Card style={{ width: '25rem', margin:'0 auto', marginTop:'10em', background: 'linear-gradient(to right, rgba(13,126,194,1) 0%, rgba(13,165,194,0.94) 100%)', padding:'15px 30px' }} >
                    <Card.Body>              
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{color: '#FFF',fontWeight:"bold", textAlign:'center'}}>
                            Recuperar contraseña
                            </Col>
                        </Row>
                        <br />
                        <br />
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Form.Group  >
                                    <Form.Control 
                                         onChange={(evt: any)=>{
                                            this.setState({Usuario: evt.target.value});
                                        }}  
                                        placeholder="Nombre de usuario"
                                        value = {this.state.Usuario}
                                        />
                                </Form.Group>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Form.Group  >
                                    <Form.Control type="text"
                                        onChange={(evt: any)=>{
                                            this.setState({Contrasena: evt.target.value});
                                        }}
                                        placeholder="E-mail"
                                        value = {this.state.Contrasena}
                                         />
                                </Form.Group>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <Button variant="primary" size="sm" onClick={this.Enviar} 
                                     style={{backgroundColor:"#FFF", color:'#0D7EC2',fontWeight:"bold",padding:" 5px 20px", borderRadius:'50px'}}
                                    >Enviar</Button>
                            </Col>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <Button variant="primary" size="sm" onClick={this.LogIn} 
                                     style={{backgroundColor:"#FFF", color:'#0D7EC2',fontWeight:"bold",padding:" 5px 20px", borderRadius:'50px'}}
                                    >Iniciar sesión</Button>
                            </Col>
                            
                        </Row>
                        <br />
                    </Card.Body>
                    </Card>        
            
                    </Col>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7} style={{textAlign:'center'}}>
                        <div style={{ color:"#FFF", marginTop:'20em'}}>
                        <Carousel prevLabel="" nextLabel="" prevIcon="" nextIcon="" fade={true} interval={3000} >
                            <Carousel.Item>
                                <h4 style={{margin : 0}}><strong>Somos tan fuertes como el acero</strong> </h4>
                            </Carousel.Item>
                            <Carousel.Item>
                                <h4 style={{margin : 0}}><strong>Ofrecemos soluciones en acero y metales planos,<br/> con valor y confianza</strong></h4>
                            </Carousel.Item>
                        </Carousel>
                        </div>                                     
                    </Col>
                 </Row>
             </div>
    }
  
}