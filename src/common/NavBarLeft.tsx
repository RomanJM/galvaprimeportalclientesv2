import React from "react";

import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store/index";
import * as LogInStore from "../store/LogInStore";
import * as url from "../common";
import { Link } from 'react-router-dom';
import { NavLink } from 'reactstrap';
import { translate } from "react-i18next";
import { AnyAction, bindActionCreators } from "redux";

type NavbarLeftProps = LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators &
  {t?: any} &
  RouteComponentProps<{}>;

class NavbarLeft extends React.Component<NavbarLeftProps, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      menuActivo: "-1",
      mostrarWatsap: false,
    };
  }
  public render() {
    const { t } = this.props;
    var menuheders = _.filter(this.props.MenuUsuario, (x: any) => {
      return x.Menu_id_padre == 0 && x.Auxx == 1;
    });
    return (
      <ul
              style={{  textDecoration: "none", listStyle: "none" }} className="Navbarleft"
            >
              <li>
              <NavLink tag={Link}   className={`circulo ${this.state.menuActivo == "-1" ? "circuloactivo" : ""}`}
                                      to={'/'.concat('Inicio')}
                                      onClick={() => {
                                        this.props.setMenuActive(null, null);
                                        this.setState({ menuActivo: "-1", mostrarWatsap: false });
                                      }}
                                  >
                                      <img src={"images/icons/i-inicio-1.png"} style={{width:'100%'}}/>
                                      <p>{t('menu_inicio')}</p>
                                </NavLink>
              </li>
              {menuheders.map((menu: any) => {
                var submenus = _.filter(this.props.MenuUsuario, {
                  Menu_id_padre: menu.Menu_id,
                  Auxx: 1,
                });
                return (
                  <li  key={menu.Menu_id}>
                     <NavLink tag={Link}   className={`circulo ${this.state.menuActivo == menu.Menu_id? "circuloactivo": ""}`}
                                      to={submenus.length ? submenus[0].Enlace : "#"}
                                      onClick={() => {
                                        if (menu.Enlace === "#") {
                                          
                                          this.props.setMenuActive(
                                            menu.Menu_id,
                                            submenus[0].Menu_id
                                          );
                                          this.setState({
                                            menuActivo: menu.Menu_id,
                                            mostrarWatsap: menu.Menu_id == 15 ? true : false,
                                          })
                                        } else {
                                          window.open(menu.Enlace, "_blank");
                                        }
                                      }}
                                  >
                                      <img src={`images/icons/${menu.Descripcion}`} style={{width:'100%'}}/>
                                      <p>{t(menu.I18n)}</p>
                                </NavLink>
                  </li>
                );
              })}
              {this.state.mostrarWatsap ? (
                <li style={{ marginTop: 4 }}>
                  <div
                    className="circulo whatsapp"
                    onClick={() => {
                      //mostrar modal para enviar mensaje de watssap
                      window.open(url.RutaFinalWhatssapp, "_blank");
                    }}
                  >
                    <img
                      src={"images/icons/whatsapp.png"}
                      style={{width:'100%'}}
                    />
                    <p>{t('menu_whatssap')}</p>
                  </div>{" "}
                </li>
              ) : null}
            </ul>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.LogInStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...LogInStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('common')(NavbarLeft as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
