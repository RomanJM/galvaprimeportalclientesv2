
import React from 'react';
import * as _ from "lodash";
import { Button, Card, Row, Col, Form, Carousel, Alert } from "react-bootstrap";
import axios from "axios";
import * as toastr from "./Toast";
import * as url from "../common";

export default class NuevoPassword extends React.Component<any,any>{
    constructor(props: any){
        super(props);
        this.state = {
            password1: "",
            password2: "",
            token: "",
            mostrarcambiopassword: false
          };
    }
    componentWillMount() {
        const { search } = this.props.location;
        var token: any =  null;
        if(search){
            token = search.replace("?token=","");
            this.setState({token: token});
        }
        this.validaToken(token);
    }

    public validaToken = (token: any) => {

        var respnseA = axios.post(url.ValidaToken, {Token : token}, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if(data.Type == "success"){
                    this.setState({mostrarcambiopassword: true});
                }else{
                    this.setState({mostrarcambiopassword: false, message: data.Message});
                }
            })
            .catch(error => {
                console.log(error);
            })
    }
    public Recuperar = () =>{
        if (!this.state.password1) {
			toastr.warning("Ingresar usuario para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.password2) {			
			toastr.warning("Ingresar contraseña para continuar", "Portal Clientes");
			return false;
		}

        if ( this.state.password1 != this.state.password2) {			
			toastr.warning("Contraseña incorrecta - diferentes", "Portal Clientes");
			return false;
		}

        var respnseA = axios.put(url.ActualizaPassword, {
            Usuario_id: this.state.token,
            Password: this.state.password2
        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                toastr.error(data.Message, "Portal Clientes");
                if(data.Code != "-1"){
                    this.LogIn();
                }
                
            })
            .catch(error => {
                console.log(error);
            })

    }
    public LogIn = () => {
		this.props.history.replace({ pathname: "/" });
	}
    public render() {
        return this.state.mostrarcambiopassword ? 
          <div>
              <Row className="rowlogin">
              <Col
                xs={12}
                sm={12}
                md={5}
                lg={5}
                xl={5}
                className="text-center"
                style={{ position: "relative" }}
              >
                <img src="images/logo.png" className="imagelogin" />
              </Col>
              <Col
                xs={12}
                sm={12}
                md={7}
                lg={7}
                xl={7}
                style={{ paddingTop: "1em" }}
              >
                <nav className="navbar navbar-expand-lg navlogin">
                  <div className="container-fluid">
                    <button
                      className="navbar-toggler"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#navbarTogglerDemo01"
                      aria-controls="navbarTogglerDemo01"
                      aria-expanded="false"
                      aria-label="Toggle navigation"
                    >
                      <span className="navbar-toggler-icon"></span>
                    </button>
                    <div
                      className="collapse navbar-collapse"
                      id="navbarTogglerDemo01"
                    >
                      <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                          {" "}
                          <a
                          target="_blank"
                            className="nav-link opcionmenu"
                            aria-current="page"
                            href="http://galvaprime.com/conozcanos/"
                          >
                            NOSOTROS
                          </a>
                        </li>
                        <li className="nav-item">
                          {" "}
                          <a
                          target="_blank"
                            className="nav-link opcionmenu"
                            aria-current="page"
                            href="http://galvaprime.com/productos/"
                          >
                            PRODUCTOS
                          </a>
                        </li>
                        <li className="nav-item">
                          {" "}
                          <a
                          target="_blank"
                            className="nav-link opcionmenu"
                            aria-current="page"
                            href="http://galvaprime.com/servicios/"
                          >
                            SERVICIOS
                          </a>
                        </li>
                        <li className="nav-item">
                          {" "}
                          <a
                            className="nav-link opcionmenu"
                            aria-current="page"
                            onClick={() => {
                              window.location.replace("/recuperapassword");
                            }}
                          >
                            OLVIDE LA CONTRASEÑA
                          </a>
                        </li>
                        <li className="nav-item">
                          {" "}
                          <a
                          target="_blank"
                            className="nav-link opcionmenu"
                            aria-current="page"
                            href="https://api.whatsapp.com/send/?phone=%2B5218124698582&text=Hola+Galvaprime&app_absent=0"
                          >
                            CONTACTO
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
              </Col>
            </Row>
            <Row style={{ width: "100%" }}>
              <Col
                xs={12}
                sm={12}
                md={5}
                lg={5}
                xl={5}
                style={{ textAlign: "center" }}
              >
                <Card
                  className="cardlogin"
                  style={{
                    margin: "0 auto",
                    background:
                      "linear-gradient(to right, rgba(13,126,194,1) 0%, rgba(13,165,194,0.94) 100%)",
                  }}
                >
                  <Card.Body>
                    <Row>
                      <Col
                        xs={12}
                        sm={12}
                        md={12}
                        lg={12}
                        xl={12}
                        style={{
                          color: "#FFF",
                          fontWeight: "bold",
                          textAlign: "center",
                        }}
                      >
                        Nueva contraseña
                      </Col>
                    </Row>
                    <br />
                    <br />
                    <Row>
                      <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group>
                          <Form.Control
                           type="password"
                            onChange={(evt: any) => {
                              this.setState({ password1: evt.target.value });
                            }}
                            placeholder="Contraseña"
                            value={this.state.password1}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group>
                          <Form.Control
                            type="password"
                            onChange={(evt: any) => {
                              this.setState({ password2: evt.target.value });
                            }}
                            placeholder="Repetir contraseña"
                            onKeyUp={(evt: any) => {
                              if (evt.keyCode == 13) this.Recuperar();
                            }}
                            value={this.state.password2}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <br />
                    <Row>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <Button variant="primary" size="sm" onClick={this.Recuperar} 
                                     style={{backgroundColor:"#FFF", color:'#0D7EC2',fontWeight:"bold",padding:" 5px 20px", borderRadius:'50px'}}
                                    >Enviar</Button>
                            </Col>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <Button variant="primary" size="sm" onClick={this.LogIn} 
                                     style={{backgroundColor:"#FFF", color:'#0D7EC2',fontWeight:"bold",padding:" 5px 20px", borderRadius:'50px'}}
                                    >Iniciar sesión</Button>
                            </Col>
                            
                        </Row>
                    <br />
                    {/*<Row>
                                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                    <Form.Group >
                                        <Form.Label style={{color: '#fff', fontStyle:'italic'}}>Olvidé la contraseña</Form.Label>
                                    </Form.Group>
                                </Col>
                                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                    <Form.Group  >
                                        <Form.Label style={{color: '#fff', fontStyle:'italic'}}>Registrarme</Form.Label>
                                    </Form.Group>
                                </Col>
                            </Row>*/}
                  </Card.Body>
                </Card>
              </Col>
              <Col
                xs={12}
                sm={12}
                md={1}
                lg={1}
                xl={1}
                style={{ textAlign: "center" }}
              >		
              </Col>		  
              <Col
                xs={12}
                sm={12}
                md={6}
                lg={6}
                xl={6}
                style={{ textAlign: "center" }}
              >
                <div style={{ color: "#FFF" }} className="carousellogin">
                  <Carousel
                    prevLabel=""
                    nextLabel=""
                    prevIcon=""
                    nextIcon=""
                    fade={true}
                    interval={3000}
                  >
                    <Carousel.Item>
                      <div style={{ textAlign: "left" }}>
                        <h5 style={{ margin: 0 }}>
                          <strong>Misión</strong>{" "}
                        </h5>
                        <p style={{ fontSize: "14px" }}>
                          Ofrecer soluciones en acero y metales planos, con valor y
                          confianza.
                        </p>
                      </div>
                    </Carousel.Item>
                    <Carousel.Item>
                      <div style={{ textAlign: "left" }}>
                        <h5 style={{ margin: 0 }}>
                          <strong>Visión</strong>{" "}
                        </h5>
                        <p style={{ fontSize: "14px" }}>
                          {" "}
                          <ul>
                            {" "}
                            <li>
                              Ser Líder de productos y servicios especializados de
                              acero y metales.
                            </li>
                            <li>Consolidar el crecimiento rentable del negocio.</li>
                            <li>Impulsar el desarrollo del capital humano.</li>
                          </ul>{" "}
                        </p>
                      </div>
                    </Carousel.Item>
                    <Carousel.Item>
                      <div style={{ textAlign: "left" }}>
                        <h5 style={{ margin: 0 }}>
                          <strong>Valores</strong>{" "}
                        </h5>
                        <p style={{ fontSize: "14px" }}>
                          Calidad, Integridad, Trabajo en equipo, Rapidez y Agilidad
                          Responsabilidad, Pro actividad
                        </p>
                      </div>
                    </Carousel.Item>
                  </Carousel>
                </div>
              </Col>
            </Row>
          </div>
          :
          <div className='text-center'>
              <Alert key={"warning"} variant={"warning"}>
              <strong>{this.state.message}</strong>
            </Alert>
              
          </div>
        
      }
}