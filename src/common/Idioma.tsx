import React from "react";
import { Row, Col} from "react-bootstrap";
import { translate } from 'react-i18next';


class Idioma extends React.Component<{t?: any, i18n?: any}, {}> {
  constructor(props: any) {
    super(props);
  }
  public render() {
    const {t, i18n} = this.props;
    
    return (
        <Row >
        <Col >
        <img
                        src={"images/flags/spain.png"}
                        style={{ cursor:'pointer',float: 'right' }}
                        onClick={()=>{
                          i18n.changeLanguage("es");
                          localStorage.setItem("Idioma", "es");
                        }}
                      />
                      <img
                        src={"images/flags/united-kingdom.png"}
                        style={{ cursor:'pointer', float: 'right', marginRight: 10 }}
                        onClick={()=>{
                          i18n.changeLanguage("en");
                          localStorage.setItem("Idioma", "en");
                        }}
                      />
        </Col>            
      </Row>
    );
  }
}

const withTranslate = translate('common')(Idioma as any);
export default withTranslate;