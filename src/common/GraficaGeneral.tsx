import * as React from "react";
import numeral from "numeral";
import { Chart } from "react-google-charts";
import * as _ from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { Bar } from 'react-chartjs-2';

interface GraficaGeneralProps {
  title?: any;
  data: any[];
  onclick: any;
  ticks?: any[];
  label: any,
  labels: any[],
  backgroundColor: any,
}
export default class GraficaGeneral extends React.Component<
  GraficaGeneralProps,
  any
> {
  
  
  public render() {
    console.log("window.screen.height ",window.screen.height);
    var height: any = window.screen.height;
    if(window.screen.height <= 1080 && window.screen.height >= 1051){
      height = window.screen.height - 730;
    }else if(window.screen.height <= 1050 && window.screen.height >= 901){
      if(window.screen.height == 1050)
        height = window.screen.height - 680; 
      if(window.screen.height == 1024)
        height = window.screen.height - 680; 
      if(window.screen.height == 960)
        height = window.screen.height - 600; 
    }else if (window.screen.height <= 900 && window.screen.height >= 801){
      height = window.screen.height - 560;  
    }else{
      height = window.screen.height - 460; 
    }
      
      
   /* var importes: any[] = [];
    this.props.data.map((item: any) => {
      importes.push(item[1]);
    });
    var mayor = _.max(importes);
    var x = -1;
  */
    /*return this.props.data.map((item: any) => {
      x++;
      var numero: any = (parseFloat(item[1]) * 100) / mayor;
      numero = parseFloat(numero);
      */
      return (
        <div className="container" style={ this.props.data.length <=2 ? { marginLeft: "7%" , marginRight: "2%", paddingTop: "2%"} : { marginLeft: "2%" , marginRight: "2%",paddingTop: "2%"}}>
          {
            /*<div className="row" style={{ margin: "10px" }}>
            <div
              className="col col-lg-2 col-md-2 col-sm-12 col-12"
              style={{ fontSize: "13px", fontWeight: "bold" }}
            >
              {item[0]}
            </div>
            <div
              className="col col-lg-8 col-md-8 col-sm-12 col-12"
              style={{ fontSize: "14px" }}
            >
              <OverlayTrigger
                key={"right"}
                placement={"right"}
                overlay={
                  <Tooltip id={`tooltip-${"right"}`}>
                    <div>{numeral(item[1]).format("0,0")}</div>
                  </Tooltip>
                }
              >
                <div
                  id={`barar-${x}`}
                  onClick={() => {
                    this.props.onclick(item[0], item);
                  }}
                  style={{
                    borderRadius: "50px",
                    padding:
                      _.isNaN(numero) || numero == 0
                        ? "0"
                        : this.props.data.length > 6
                        ? "14px"
                        : "18px",
                    backgroundColor: item[2],
                    width: _.isNaN(numero) || numero <= 0 ? 0 : `${numero}%`,
                    cursor: "pointer",
                    position: "relative",
                  }}
                  className="barragrafica"
                ></div>
              </OverlayTrigger>
            </div>
          </div>*/
              }
               <Bar
                height={height}               
                data={{
                    labels: this.props.labels,
                    datasets: [{
                        label: this.props.label,
                        data: this.props.data,
                        /*backgroundColor: '#0e7ec3',*/
                        backgroundColor: this.props.backgroundColor,
                        pointStyle: 'circle',
                        hoverRadius: 10,
                        /*barThickness: 50,*/
                        barThickness: 70,
                        radius: [20, 20, 20, 20, 20],
                        minBarLength: 5
                    }]
                }}
                options={
                    {                 
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            yAxes: [{
                                /*gridLines: { color: '#555454', offsetGridLines: true },*/
                                gridLines: { display: false },
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value : any) {
                                        return numeral(value).format('0,0')
                                    },
                                    
                                    fontSize: 14
                                }

                            }],
                            xAxes: [{
                                gridLines: { display: false },
                                ticks: {
                                 
                                    fontSize: 14
                                }
                            }],
                            
                        },
                        legend: {
                            display: false,
                            labels: {
                                
                                fontSize: 14
                            }
                        },
                        onClick: (e : any, items: any) => {
                            this.props.onclick(items);
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem: any, data: any) {
                                    let label = data.datasets[tooltipItem.datasetIndex].label || '';
                                    if (label) {
                                        label += ':* ';
                                    }
                                    label += numeral(tooltipItem.yLabel).format('0,0');
                                    return label;
                                }
                            },
                            backgroundColor: '#555454'  

                        },
                        animation: {
                            duration: 1000,
                            easing: 'linear'
                        }
                    }
                }
                redraw={true}
            />
        </div>
      );
    /*<Chart
                          width={'600px'}
                          height={'300px'}
                          chartType="BarChart"
                          loader={<div>Cargando Grafica</div>}     
                          data={this.props.data}
                          
                          chartEvents={[
                            {
                              eventName: 'select',
                              callback: ({ chartWrapper }) => {
                                const chart = chartWrapper.getChart()
                                const selection = chart.getSelection()
                                console.log(selection);
                                if (selection.length === 1) {
                                  const [selectedItem] = selection
                                  //const dataTable : any= chartWrapper.getDataTable()
                                  const { row, column } = selectedItem
                                  this.props.onclick( row, column);
                                }
                              },
                            },
                          ]}
                          options={{
                            
                            bars: 'horizontal',
                            animation: {
                              startup: true,
                              easing: 'linear',
                              duration: 500,
                            },
                            bar: { groupWidth: '70%',borderWidth : 15, border : 10,cornerRadius:10,fontSize : 10 },
                            legend: { position: 'none' },
                            title: this.props.title,
                            chartArea: { width: '50%' },
                            hAxis: { textPosition: 'none',gridlines: { count: 0 ,color: 'transparent' },ticks: this.props.ticks},
                            vAxis: { 
                              baseline : 0,
                              baselineColor: "#fff",
                              gridlines: { count: 0 ,color: 'transparent' },viewWindow: { min: 0, max: 8 },ticks: this.props.ticks},
                            //colors : colores,
                            xAxis: {
                              gridlines: {
                                color: 'none',
                                display : false,
                                interval: 0
                            }
                            },
                            yAxis: {
                              gridlines: {
                                color: 'none',
                                display : false,
                                interval: 0,
                                baseline : 0,
                              baselineColor: "#fff",
                            }
                            
                            }
                            
                          }}
                        />*/
  }
}
