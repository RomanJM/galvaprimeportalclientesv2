import * as React from 'react';
interface LayoutGraficaProps {
    mensage?: string
}
export default class Loading extends React.Component<LayoutGraficaProps, any>{
    public render() {
        return <div className=" container-fluid layout-grafica containerpage">
                      {this.props.children}
                 </div>;
    }
}