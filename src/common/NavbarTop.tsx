import React from 'react';
import {  Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import Select from 'react-select';
import moment from 'moment';
import { connect } from 'react-redux';
import { translate } from 'react-i18next'
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store/index';
import { AnyAction, bindActionCreators } from 'redux';
import * as LogInStore from '../store/LogInStore';
import * as NavbarTopStore from '../store/NavbarTopStore';
import * as DashboardStore from '../store/DashboardStore';
import * as SeguimientoOCStore from '../store/SeguimientoOCStore';
import * as InventariosStore from '../store/InventariosStore';
import * as EstadoCuentaStore from '../store/EstadoCuentaStore';
import * as EmbarquesStore from '../store/EmbarquesStore';
import * as FacturasStore from '../store/FacturasStore';
import * as CertificadosStore from '../store/CertificadosStore';
import { Block } from '@material-ui/icons';
import Idioma from './Idioma';

type NavbarTopProps = NavbarTopStore.NavbarTopState & typeof NavbarTopStore.actionCreators & 
                      LogInStore.LogInStoreState & typeof LogInStore.actionCreators & 
                      DashboardStore.DashboardStoreState & typeof DashboardStore.actionCreators & 
                      SeguimientoOCStore.SeguimientoOCStoreState & typeof SeguimientoOCStore.actionCreators &
                      InventariosStore.InventariosStoreState & typeof InventariosStore.actionCreators &
                      EstadoCuentaStore.EstadoCuentaStoreState & typeof EstadoCuentaStore.actionCreators &
                      EmbarquesStore.EmbarquesStoreState & typeof EmbarquesStore.actionCreators &
                      FacturasStore.FacturasStoreState & typeof FacturasStore.actionCreators &
                      CertificadosStore.CertificadosStoreState & typeof CertificadosStore.actionCreators &
                      {t?: any, i18n?: any} &
                      RouteComponentProps<{}>;
interface NavbarTopstate {
    cliente: any,
    vendedor: any
}
 class NavbarTop extends React.Component<NavbarTopProps, NavbarTopstate>{
    constructor(props: any){
        super(props);
        this.state = {
            cliente : 0,
            vendedor : 0
        };

    }
    componentDidMount(){
        var cliente: any = this.props.rolusuario == "CLI" ? this.props.Usuario_id : "";
        var vendedor: any = this.props.rolusuario == "VEN" ? this.props.ZEmpleado : "";
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();
    }
    public Buscar = () =>{
        this.props.setClienteVendedor(this.state.cliente.value, this.state.vendedor.value);
        if(this.props.vista === "Dashboard"){
            this.props.requestInformacionInicialDahboard(this.state.cliente.value, this.state.vendedor.value);
        }
        if(this.props.vista === "OC"){
            this.props.requestInformacionInicialDahboard(this.state.cliente.value, this.state.vendedor.value);
        }
        if(this.props.vista === "Inventarios"){
           this.props.requestInformacionInicialInventarios(this.state.cliente.value, this.state.vendedor.value);
        }
    }
    public Filtrar = (cliente : any, vendedor : any) =>{
        this.props.setClienteVendedor(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
        if(this.props.vista === "Dashboard"){
            this.props.requestInformacionInicialDahboard(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
         }else if(this.props.vista === "SeguimientoOC"){
           this.props.requestInformacionInicialSOC(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
         }   else  if(this.props.vista === "Inventarios"){
            if(this.props.esAlmacen == true){
                this.props.requestInformacionInicialInventarios(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
            }else{
                
                this.props.requestInformacionInicialMercado(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
            }
            
         }  else if(this.props.vista === "EstadoCuenta"){
            this.props.requestInformacionInicialEdoCuenta(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
          } 
          else if(this.props.vista === "Embarques"){
            this.props.requestInformacionInicialEmbarques(cliente ? cliente.value : '', vendedor ? vendedor.value : '');
          } 
          else if(this.props.vista === "Facturas"){
            var fechaInicio = moment(this.props.fechaInicioFactura).format('YYYY-MM-DD');
            var fechaFin = moment(this.props.fechaFinFactura).format('YYYY-MM-DD');  
            this.props.requestInformacionInicialFacturas(cliente ? cliente.value : '', vendedor ? vendedor.value : '' , fechaInicio, fechaFin, this.props.facturaDoc,this.props.remisionDoc,this.props.NumAtCard);
          } 
          else if(this.props.vista === "Certificados"){
            var fechaInicio = moment(this.props.fechaInicioFactura).format('YYYY-MM-DD');
            var fechaFin = moment(this.props.fechaFinFactura).format('YYYY-MM-DD');  
            this.props.requestInformacionInicialCertificados(cliente ? cliente.value : '', vendedor ? vendedor.value : '' , fechaInicio, fechaFin, this.props.certificado,this.props.lote,  this.props.NumAtCardcer);
          }
    }
    public render(){
        const {t} = this.props;
        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "", label: t('option_todo')});
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: "", label: t('option_todo') });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });
        var estilos = {
            control: (styles: any) => ({ ...styles, backgroundColor: 'transparent', color:'white', border:'1px solid #ffffff',zIndex: 2 }),
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    color : '#357DBD',
                    textAlign:'left',
                    zIndex: 2,
                    fontWeight: 'bold'
                };
            },
            input: (styles: any) => ({ ...styles, color:'white' }),
            placeholder: (styles: any) => ({ ...styles, color:'white' }),
            menuList: (styles: any) => ({
                ...styles,
                zIndex: 2,
                backgroundColor: 'rgba(255, 255, 255, 0.3)'
            })
        };
        var rol = this.props.rolusuario;
        return  (
            <div id="menu_top" className="menutop">
                
                <Navbar variant="light" className="navmenutop" >
                    <Navbar.Brand href="#home">
                        <img src="../images/logo.png" width ="200px" height ="12%" style={{filter:'brightness(0) invert(1)'}} /> 
                    </Navbar.Brand>
                    <Nav className="me-auto">
                        {/* <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#features">Features</Nav.Link>
                        <Nav.Link href="#pricing">Pricing</Nav.Link> */}
                        
                    </Nav>
                    <Nav>
                    <Idioma />
                    </Nav>
                    <Nav className="justify-content-end" activeKey="1">
                    
                                <NavDropdown title={<div>{this.props.NombreUsuario} <img src="../images/avatar.jpg" width="40px" height="40px" style={{borderRadius:'50%'}} alt="" /></div>} id="nav-dropdown">
                                    <NavDropdown.Item  eventKey="1.1" onClick={()=>{
                                        window.localStorage.clear();
                                        window.location.replace("/");
                                    }}>{t('btn_salir')}</NavDropdown.Item>
                                </NavDropdown>
                               
                        </Nav>
                </Navbar> 
                <div className="row">
                    <div className = "col col-lg-1 col-md-1 col-12 col-12"></div>
                    <div className = "col col-lg-11 col-md-11 col-12 col-12 text-center" >
                   
                        <h3 id="bienvenida">{t('msg_bienvenida')}</h3> 
                       
                    </div>
                </div>
                <div className="row" >
                    <div className = "col col-lg-1 col-md-1 col-12 col-12" ></div>
                    <div className = "col col-lg-11 col-md-11 col-12 col-12" style={{paddingRight:'4.5em'}}>
                        {this.props.mostrarSelects == true ? 
                            <div className="row rowcenter">
                            {(rol == "ADM" || rol == "JF1") ?  <div style={{ paddingRight : 0 }}  className="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                  <div style={{display : "block", float : 'right'}}  className = "select">
                                                  <Select
                                                         id="select-vendedor"
                                                         className="react-select"
                                                         value={this.state.vendedor}
                                                         options={vendedores}
                                                         onChange={(valor: any) => {
                                                             this.setState({ vendedor: valor, cliente: null });
                                                             //this.props.ResetSeleccionados();
                                                             this.props.requestFiltraClientes(valor.value);                                                       
                                                             this.setState({ cliente: 0 });
                                                             this.Filtrar("",valor);
                                                         }}
                                                         placeholder={t('select_vendedor')}
                                                         defaultValue={{ value: "", label: '- TODO -' }}
                                                         styles={estilos}
                                                     />
                                                  </div>
                                                   
                                                   </div>:null}
                         {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?  <div style={{ paddingRight : 0 }} className="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                  <div style={{display : "block", float : 'left'}}  className="select">
                                                  <Select
                                                         id="select-cliente"
                                                         value={this.state.cliente}
                                                         options={clientes}
                                                         onChange={(valor: any) => {
                                                             this.setState({ cliente: valor });
                                                             this.Filtrar(valor, this.state.vendedor);
                                                         }}
                                                         placeholder={t('select_cliente')}
                                                         defaultValue={{ value: "", label: '- TODO -' }}
                                                         styles={estilos}
                                                     />
                                                  </div>
                                                 
                                                   </div>:null}
                                            </div>     
                        : null}     
                    </div>
                </div>
            </div>
            
        );
    }
}

const mapStateToProps = (state: ApplicationState) => {
    return {
        ...state.LogInStore,
        ...state.NavbarTopStore,
        ...state.DashboardStore,
        ...state.SeguimientoOCStore,
        ...state.InventariosStore,
        ...state.EstadoCuentaStore,
        ...state.EmbarquesStore,
        ...state.FacturasStore,
        ...state.CertificadosStore
    }
  }
  const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators( {
      ...LogInStore.actionCreators,
      ...NavbarTopStore.actionCreators,
      ...DashboardStore.actionCreators,
      ...SeguimientoOCStore.actionCreators,
      ...InventariosStore.actionCreators,
      ...EstadoCuentaStore.actionCreators,
      ...EmbarquesStore.actionCreators,
      ...FacturasStore.actionCreators,
      ...CertificadosStore.actionCreators
    }, dispatch)
  }
const withTranslate = translate('common')(NavbarTop as any);
export default connect(mapStateToProps,mapDispatchToProps)(withTranslate);