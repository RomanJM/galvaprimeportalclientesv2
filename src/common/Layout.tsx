import React from 'react';
import NavbarTop from './NavbarTop';
import NavbarLeft from './NavBarLeft';
import OpcionesTop from './OpcionesTop';
import 'react-table/react-table.css';
import { translate } from 'react-i18next'
class Layout extends React.Component<{t?: any},any>{
    componentDidMount(){
        var body: any = document.getElementById("body");
        body.classList.remove("image-fondo");
    }
    public render(){
        const { t } = this.props;
        return <div >
                <NavbarTop />              
                <div className="row" style={{height:'100vh'}}>
                    <div className = "col col-lg-1 col-md-1 col-12 col-12"  >
                    <NavbarLeft/>
                    </div>
                    <div className = "col col-lg-11 col-md-11 col-12 col-12" style={{paddingRight:'4em'}}>
                        <div className="row opcionestop">
                            <OpcionesTop/>
                        </div>
                        {this.props.children}
                        
                    </div>
                    <div  style={{ display : "block", color : "#234ff", textAlign : "center", position: 'absolute',bottom:10}}> 
                            <div style={{textAlign : "center", display : "flex", justifyContent : "center", alignItems : "center"}}> 
                                <div >{t('lbl_ultima_actualizacion')} </div> 
                                <div id="fecha-actualizacion"></div> 
                            </div> 
                        </div>  
                </div>   
                              
            </div>
    }
}

const withTranslate = translate('common')(Layout as any);
export default withTranslate;