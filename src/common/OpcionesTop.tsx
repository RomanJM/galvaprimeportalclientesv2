import React from "react";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import { NavLink } from "reactstrap";
import * as _ from "lodash";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store/index";
import * as LogInStore from "../store/LogInStore";
import moment from "moment";
import { AnyAction, bindActionCreators } from "redux";
import { translate } from "react-i18next";

type OpcionesTopProps = LogInStore.LogInStoreState &
  typeof LogInStore.actionCreators & {t?: any} &
  RouteComponentProps<{}>;

class OpcionesTop extends React.Component<OpcionesTopProps, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      menuActivo: null,
    };
  }
  componentWillReceiveProps(props: any) {
    this.setState({ menuActivo: props.submenuActivo });
  }
  componentDidMount(){
    var fecha: any = document.getElementById("fecha-actualizacion");
    var date =new Date(this.props.FechaActualizacion);
    var h = "am";
    if(date){
      var horas = date.getHours();
      if(horas > 12){
        h = "pm";
      }
    }
   
    fecha.innerHTML =  moment(this.props.FechaActualizacion).format("DD-MM-YYYY hh:mm") + h;
  }
  public render() {
    const { t } = this.props;
    var submenus = _.filter(this.props.MenuUsuario, (x: any) => {
      return x.Menu_id_padre == this.props.menuActivo && x.Auxx == 1;
    });
    return (
      <div style={{marginTop : '1%'}} className="container-fluid">
        <div className="row rowcenter">
          {submenus.map((submenu: any) => {
            var campo = this.state.menuActivo == submenu.Menu_id ? "1" : "2";
            var imagen = String(submenu.Descripcion).replace(
              this.state.menuActivo == submenu.Menu_id ? "2" : "1",
              this.state.menuActivo == submenu.Menu_id ? "1" : "2"
            );
            return (
              <NavLink
                tag={Link}
                className={`opciones_menu menu_no_activo ${
                  this.state.menuActivo == submenu.Menu_id ? "menu_activo" : ""
                }`}
                style={{ fontWeight: "bold", color: "#0D7EC2" }}
                to={submenu.Enlace}
                onClick={() => {
                  // this.setState({menuActivo: submenu.Menu_id});
                  this.props.setMenuActive(
                    submenu.Menu_id_padre,
                    submenu.Menu_id
                  );
                }}
              >
                <div
                  className="row"
                  onMouseOver={() => {
                    var element: any = document.getElementById(submenu.Enlace);
                    var valor = element.getAttribute("src");
                    element.setAttribute(
                      "src",
                      String(valor).replace("2", "1")
                    );
                  }}
                  onMouseOut={() => {
                    var element: any = document.getElementById(submenu.Enlace);
                    var valor = element.getAttribute("src");
                    if (this.props.submenuActivo != submenu.Menu_id) {
                      element.setAttribute(
                        "src",
                        String(valor).replace("1", "2")
                      );
                    }
                  }}
                >
                  <div
                    className="col col-lg-3 col-md-3 col-sm-12 col-12"
                    style={{ paddingRight: "0" }}
                  >
                    <img
                      src={`images/icons/${imagen}`}
                      style={{ width: "30px", margin: "auto" }}
                      id={submenu.Enlace}
                    />
                  </div>
                  <div
                    className="col col-lg-9 col-md-9 col-sm-12 col-12"
                    style={{ paddingLeft: "0" }}
                  >
                    <div style={{ textAlign: "center" }}>
                      {" "}
                      <p style={{ margin: "8px auto" }}>{t(submenu.I18n)}</p>{" "}
                    </div>
                  </div>
                </div>
              </NavLink>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => {
  return {
    ...state.LogInStore,
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      ...LogInStore.actionCreators,
    },
    dispatch
  );
};
const withTranslate = translate('common')(OpcionesTop as any);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslate);
